 

 

 

INSTITUT INTERNATIONAL
DE BIBLIOGRAPHIE PUBLI-

CATION

Bet pe L'IKsrirur : Perfectionner et unifier les méthodes bibliographiques et
documentaires. Organiser la coopération scientifique internationale dans N' 76
les travaux de bibliographie et de documentation, Préparer un Répertoire
Bibliographique Universel et en délivrer des extraits et duplicata,

 

 

 

 

 

 

 

 

 

 

 

 

Bibliothèque Collective

des Associations et Institutions

Scientifiques et Corporatives

BRUXELLES. — SALLE GOTHIQUE
27%, MONTAGNE DE LA COUR

OUVERTE TOUS LES JOURS NON FÉRIÉS
DE 9 À 12 ET DE 2 À 6 HEURES

Indice bibliographique :

[027 (4932) ] BRUXELLES

 
Institut International de Bibliographie. [o27 (493.2)]
1906. — Bibliothèque collective des Associations et Institutions
scientifiques et corporatives. — [Programme. — Règlement. —
Services d’information et de documentation. — Local.]
Bruxelles, au siège de la Bibliothèque collective. 27a, Montagne de la
Cour. In-8°, 12 p.

M
Ly

NB, Notice bibliographique de la présente publication, destinée à servir de texte pour la prepa-
ration des répertoires bibliographiques et des catalogues de bibliothèque.

 

 

 

 

 

 

 

 

 

EE

 

 

 

 

 

 

 

 

 

 

AU A!

 

LA BIBLIOTHÈQUE COLLECTIVE DES SOCIÉTÉS SAVANTES

 

La Bibliothèque Collective des associa-
tions et des institutions scientifiques et
corporatives a été fondée, en Avril 1906,
par l'Institut International de Bibliogra-
phie et diverses Sociétés gavantes de
Bruxelles.

 

PROGRAMME

DE LA BIBLIOTHÈQUE COLLECTIVE

. Grouper Îes bibliothèques éparses des institutions et associations
scientifiques et corporatives, ainsi que celles des directions de pério-
diques.

Assumer, d'une manière à déterminer dans chaque cas, la gestion
administrative de ces bibliothèques.

Assurer à leur service des locaux appropriés, chauffés, éclairés et
accessibles pendant la plus grande partie de la journée.

Placer les collections de chaque institution adhérente sous la garde
d'une administration responsable, chargée de les conserver, de les
cataloguer, de les classer, de les communiquer sur place et de les
prêter au dehors, tout en laissant la pleine propriété et la libre
disposition des ouvrages déposés à l'institution adhérente.

Constituer ainsi, par la réunion des diverses bibliothèques spé-
ciales, une bibliothèque collective embrassant progressivement les
diverses branches du savoir encyclopédique, et qui soit l’auxiliaire
des bibliothèques publiques existantes, dont le caractère est général.

Mettre par suite à la disposition de l’Institut International de
Bibliographie, en échange des soins de sa gestion, des collections
étendues, utiles à ses travaux de documentation.

En même temps permettre aux associations et institutions scien-
tifiques d'assurer à leurs membres l'usage des services d’information
et de documentation de l’Institut.

 
RÉGLEMENT

Ï, — AFFILIATION.

ARTICLE PREMIER. — Admission, — Sont admis à participer aux
services de la Bibliothèque Collective, tous les groupements et insti-
tutions ayant un caractère scientifique ou corporatif, et dans la
mesure où l'Institut de Bibliographie disposera lui-même des locaux
nécessaires. Les publications périodiques indépendantes sont éga-
lement admises à participer à ces services,

ART. 2. — Effis de l'affiliation. — Les bibliothèques affiliées con-
servent la pleine propriété et la disposition de leurs collections.
L'objet de l'affliation est limité à l'administration des collections et
à leur usage en commun, dans une mesure à déterminer dans chaque
cas en particulier, mais impliquant toujours la faculté pour l’Institut
international de Bibliographie de les consulter pour ses propres
besoins,

ART. 3. — Conditions de l'affiliation. — L'Institut de Bibliographie
reçoit en dépôt les collections inventoriées qui lui sont confées. Il
assume l'organisation des divers services énumérés ci-après.

Les institutions adhérentes interviennent dans les frais d'admi-
nistration pour l'occupation des rayons et pour l'usage des services
auxquels elles déclarent vouloir participer. Ces services sont facul-
tatifs, Ils donnent lieu à des cotisations proportionnelles à l'usage
que devra en faire chaque bibliothèque. Les cotisations sont déter-
minées au tableau ci-après (page 8).

Les bibliothèques affiliées devront participer aux frais d'une assu-
rance collective, chacune au prorata du nombre et de la valeur de
ses ouvrages. |

ART. 4. — Durée. — L'affliation a une durée minimum d'un an,
Elle est prorogée par tacite reconduction. Toute résiliation est
notifiée trois mois avant l'échéance du terme.

II. — COMITÉ ADMINISTRATIF.

ART. 5. — Composition. — Les institutions affiliées sont représentées
au Comité central de la Bibliothèque Collective par un délégué.

ART. 6. — Afiributions, — Ce Comité a dans ses attributions lé
contrôle de la gestion ét, d'une manière générale, l'examen de toutes

 

sn

‘ les questions se rattachant au développement de la Bibliothèque

ainsi que les mesures.et démarches nécessaires en vue d’accroitre Le
nombre des institutions affiliées et d'obtenir les subsides des pouvoirs
publics en faveur de la Bibliothèque Collective,

IÏI. — INVENTAIRE ET ACCROISSEMENT.

ART. 7. — Inventaire, — Les collections déposées sont inventoriées
sur fiches, Un numéro d'inventaire est attribué à chaque ouvrage.
Il est porté sur l'ex-libris propre à chaque institution affiliée et qui est
appliqué sur chacun des ouvrages qu'eile dépose.

Les fiches de l'inventaire sont paraphées par le bibliothécaire de
l'Institut et par le délégué de l'institution affiliée. Elles portent la :
date du dépôt,

Le Bibliothécaire de l'Institut remet mensuellement à l'institution
adhérente un état constatant le nombre total d'ouvrages portés à
l'inventaire.

ART. 8. — Accroissements, — Les accroissements sont portés à
l'inventaire à mesure de leur réception. Ces inscriptions sont contré-
lées et paraphées périodiquement par le délégué de l'institution
intéressée et par le bibliothécaire.

Sur demande et moyennant cotisation spéciale, une liste bibliogra-
phique des accroissements pourra être fournie à l'institution affiliée

‘ et lui servir de manuscrit en vue de la publication dans son Bulletin

ou autrement, du catalogue de ses accroissements.

Les institutions adhérentes sont invitées à donner à leur biblio-
thèque tout le développement dont celles-ci sont susceptibles, I] est
de l'intérêt de chaque affilié de compléter ses collections dans le
domaine de ses travaux, comme il cst de l'intérêt de ia Bibliothèque
collective d’être composée de bibliothèques spéciales très riches afin
de former ainsi un ensemble encyclopédique aussi complet que
possible. Les institutions adhérentes sont aussi invitées à faire relier
leurs collections pour les mettre à l'abri des pertes et détériora-
tions. |

Les publications périodiques des institutions affiliées (bulletins,
actes, mémoires, rapports) constituent des éléments précieux .qui
permettent un accroissement des collections presque sans frais, soit
par voie d'échange avec les publications similaires, soit par la récep-
tion d'ouvrages nouveaux pour comptes rendus et analyses, Le
service central de la Bibliothèque coilective se charge, pour compte
des institutions qui le désirent, du service des échanges et des
demandes d'ouvrages nouveaux.

 
 

_ 6 —
TV. — SERVICES DIVERS ORGANISÉS,

ART. G. — Services divers, — Les services organisés comportent la
garde des ouvrages, leur classement, leur cataloguage, le dépouille-
ment des périodiques, l'organisation des menus imprimés en réper-
toire documentaire, l'accroissement des collections, notamment par
l'échange des périodiques, la communication des ouvrages sur place
et au dehors. | |

Les institutions adhérentes déclarent, lors de leur affiliation ou par
la suite, quels sont ceux des services organisés auxquels elles
désirent recourir et paient les cotisations y afférentes. :

ART. 10. — Conservafion des ouvrages. — La simple garde et la con-
servation des ouvrages sont gratuites, à la condition que le mobilier
soit fourni par l'institution affiliée et que l'usage des ouvrages déposés
soit déclaré commun aux membres et aux délégués de toutes les
institutions affiliées.

Le matériel nécessaire à la conservation des ouvrages tels que
rayons, cartons à brochures, etc., peut être procuré par l'Institut de
Bibliographie à titre locatif ou autre.

Art, 11. — Cafalogue, — Il est établi sur fiches, et conformément
aux méthodes de l’Institut de Bibliographie, un catalogue en double
exemplaire, l'un classé par ordre alphabétique des noms d'auteurs,
l'autre par ordre méthodique des matières. Ce catalogue est collectif
et comprend, en une seule série, l'ensembie des ouvrages déposés par
toutes les institutions adhérentes.

Chaque fiche du catalogue porte la mention de l'institution pro-
priétaire de l'ouvrage caialogué.

Les institutions affiliées peuvent obtenir pour leur usage privé, et

moyennant rétribution, un duplicata du éatalogue des ouvrages

qu'elles ont déposés.

ART, 12, — Consultation, lecture, prét, — La Bibliothèque collective
comporte une salle de lecture qui est accessible de g à 12 et de 2 à
6 heures, tous les jours non fériés. L'entrée de la salle de lecture est
située Montagne de la Cour, 274.

Chaque institution adhérente remet au service central une copie
tenue à jour de la liste de ses membres ou délégués afin de faciliter
le service etle contrôle. Notification est donnée des changements,
radiations, inscriptions nouvelles, etc.

Sont seuls admis à fréquenter la salle de lecture les membres et les
délégués des diverses institutions adhérentes, sur production de leur
carte d'identité, et les personnes auxquelles l'Institut accordera cette
faculté sous sa propre responsabilité, |

— 7 —

Les membres et les délégués peuvent recevoir en communication
sur place tous les ouvrages déposés à la Bibliothèque collective, quel
que soit le fonds auquel ces ouvrages appartiennent.

Les prêts des ouvrages au dehors n'est fait que dans les limites
prescrites par le réglement de chacune des institutions dépo-

.Santes.

ART, 13. — Service des échanges. — Les échanges comportent l'envoi
de a publication échangée ainsi que la réception et l'inventaire de la
publication avec laquelle l'échange est établi,

Le service central se charge de remettre gratuitement au Bureau
belge du service international des échanges les envois destinés à
l'étranger en franchise de port et de retirer les publications arrivant
de l'étranger à ce bureau en destination de l'institution affiliée.

ART. 14. — Demandes pour compte rendu. — Les demandes sont
faites d'après les indications données par les institutions intéres-
sées. ‘

Pour l'indication des ouvrages nouveaux à demander, les réper-
toires de l'Institut de Bibliographie sont mis à la disposition des
affiliés.

ART, 15.— Sernice des envois, — Pour l'usage du service central, il
est établi, $ous forme d'étiquettes imprimées et gommées, des séries
d'adresses de tous les membres des institutions adhérentes. Ces séries
d'adresses peuvent être cédées aux institutions adhérentes pour
leur propre usage.

V, — COTISATIONS.

ART. 16. — L'affiliation est gratuite mais l’utilisation des divers
services organisés, autres que la simple garde des ouvrages, implique
le paiement des cotisations indiquées au tableau ci-après.

Des subsides spéciaux seront sollicités des pouvoirs publics à
l'effet de voir développer les services communs aux bibliothèques
afilées. Ces subsides viendront annuellement en déduction des coti-
sations dues par les bibliothèques affiliées, et ce au prorata du mon-
tant des cotisations qu'elles se seront engagées à payer.

Il est éventuellement fait un forfait de gestion avec les institu-
tions qui désirent faire usage de l’ensemble des services organisés.
Ce forfait prend pour base le nombre des membres de l'institu-
tion adhérente et celui des volumes de la bibliothèque qu'elle
dépose. ‘

Le règlement des cotisations se fait semestriellement.

 
—8—

TABLEAU DES COTISATIONS.

1, Confection de l'inventaire, par ouvrage. . . . , ,fr.
2. Catalogue et duplicata du catalogue, par fiche , , .
3. Prêt au dehors (les frais éventuels d'envoi sont facturés à |
part), par volume . . . , , , . , 0.03
4. Demandes d'ouvrages pour compte rendu (rédaction et
pott), par ouvrage demandé, , , . . _ 0.13
5. Echanges, comprenant l'envoi et la réception des pério- |
diques échangés, par périodique, annuellémént, ports
non compris. . , our . + + 0,20
6. Séries d'adresses des membres : : cent adresses différentes
imprimées à cent exemplaires chacune , , , , , , 15.00
7. Dépouiliement des périodiques (conditions à convenir).
8. Fourniture ou location de mobilier et accessoires (conditions à
convenir).
9. Assurance : 1 franc par 1,000 francs de valeur déclarée,

SERVICES
D'INFORMATION ET DE DOCUMENTATION

Par leur union avec l'Institut International de Bibliographie, les
associations et institutions scientifiques peuvent utiliser, au béné-
fice de leurs membres, un service complet d'information et de docu-
mentation pour les matières qui font l’objet de leurs études :

» Cabinet des ouvrages périodiques spéciaux belges et étrangers {1) ;
2° Bibliothèque d'ouvrages spéciaux (livres et brochures) ;

30 Répertoire Bibliographique Universel renseignant les travaux pu-
bliés dans toutes les branches du savoir encyclopédique, {notices
classées par noms d'auteurs ét par matières; comprenant à ce
jour sept millions de notices (en formation):

Catalogue Collectif des Bibliothèques de Belgique, renseignant les
Bibliothèques du pays, où les ouvrages désirés peuvent être obte-
nus en consultation (en formation);

Répertoire Iconographique Universel, comportant les documents
photographiques qui représentent les objets, les idées et les opéra-
tions de toute nature (en formation],

{1} Le Cabinet des périodiques de la Bibliothèque Royale a principalement pour objet la collec
tion des grands périodiques d'intérêt général.

 

LOCAL

DE LA BIBLIOTHÈQUE COLLECTIVE

La Chapelle Saint-Georges, en laquelle sont installées les collections
de la « Bibliothèque Collective », fait partie intégrante des batiments du
Musée et des Arckives, lille est située, 274, Montagne de la Cour, en

- plein centre de Bruxelles.

Elle constitue vraiment un bijou d'architecture, Malgré sa destination
nouvelle, la construction conserve tout son caractère, et l'on se croirait
dans la library de quelque vieux collège d'Oxford ou de Cambridge.

Son aménagement en bibliothèque, répond aux exigences les plus
modernes: casiers‘à livres, luminaire électrique, système de classement
des livres et revues, catalogue, etc,

C'est à 1516 que remonte la reconstruction actuelle de la Chapelle
Saint-Georges. Elle apparaissait déjà remarquable à Albert Dürer, qui la
visita et y goûta fort un tableau d'Hugo van der Goes. En style ogi-
val Aamboyant, avec ses trois longues et minces colonnes cylindriques
sans chapitaux, sa voûte surbaissée à fines nervures croisées, sa tribune
ornée d'une élégante balustrade, elle constitue tout ce qui reste aujour-
d'hui, hors une aile de la cour du Musée, de la somptueuse demeure où le
Taciturne menait grand train de maison.

La Révolution détruisit l'autel de La Chapelle, puis un brasseur y ins.
talla ses tonneaux. En 1838, la ville la fit restaurer et on y déposa Îles
sculptures achetées à Kessels, Plus tard, le Musée d'Histoire naturelle
l'utilisa pour ses empaillages, et Depauw s'y illustra en y montant les
iguanodons.

En ces dernières années, on en avait fait un refuge à matériaux divers.
La voici retournée à sa plus intellectuelle destination. Le Mont-des-Arts
exigera son enveloppement extérieur par des bâtiments nouveaux, mais
l'intérieur demeurera intact et pourra être éclairé par la cour du Musée.

 
BIBLIOGRAPHIA UNIVERSALIS

Recueils périodiques de bibliographie, édités ou patronnés par l’Institut
International de Bibliographie et formant contributions imprimées au
Répertoire Bibliographique Universel.

Edition À = en volume ordinaire. “
»  B — en volume imprimé recto, blanc au verso. Nombre
» C= sur fiches imprimées. approximatif

= de notices
» D =sur fiches collées. parues

au 1* janvier
1906

Contribution n° 2. Bibliographia Zoologica Universalis ; depuis 1896.
Edition À, par fascicules hebdomadaires, fr. 18.75 par an. }
» B, » » 25.00 » 117,242
»  C, sur fiches (fr. 10.00 les 1,000 fiches). |
Expédition par séries de 5o fiches environ,
Contribution n° 3. Bibliographie Philosophica Universalis ; dep. 1895. 18,502
Edition B, par fascicules trimestriels, fr. 5.00 par an. »79
Contribution n° 4. Bibliographia Physiologica Universalis ; dep. 1893.
Edition B, 3 ou 4 fascicules par an, fr. 0.50 par fascicule. 9,007
» C, sur fiches, prix variable. 1

Contribution n° 6. Bibliographia Anatomica Universalis ; dep. 1897.
Edition À, 24 fascicules par an, fr. 10.00.
» B, 24 » » D 14.50.
» C, sur fiches, prix variable.
Contribution n°8. Bibliographiemensuelledes Chemins de fer; dep.1897.
Edition B, 12 fascicules par an, fr. 10.00.
Contribution n° 15. Bibliographie de Belgique; depuis 1897.
Edition A, fascicules bi-mensuels.
Contribution n° 16. Bibliographia Geologica Universalis ; dep. 1896.
Edition À, par volumes annuels, prix variable.
Contribution n° 30. Bibliographie Medica Universalis ; depuis 1900.
Edition À, environ 36,000 notices par an, fr. 120.00.
Contributionne 31.Bibliogvaphia BibliographicaUniversalis;dep.1808.
Edition B, en brochure annuelle, fr. 4.00 par an.
» C, en fiches imprimées, fr. 12.00 par an.
» D, » collées, fr. 7.00 par an.

|
2,960

Contribution n° 39. Bibliographia Economica Universalis ; dep. 1902.

Edition B, en volume annuel, fr. 6.00. Î _—

Contribution n°40. Bibliographia Agronomica Universalis ; dep. 1903.
Edition B, par fascicules trimestriels, 12 francs par an.

4932

Contribution n° 41. Bibliographia Technica Universalis; depuis 1903.
Edition B, par fascicules mensuels, par an : Belgique, } 43,564
fr. 10.00; étranger, fr. 13.25.

Contribution n° 40. Bibliographie du Droit belge. 3,275

Total des notices, environ. . . 512,056

 

 

 

LA BIBLIOTHÈQUE

COLLECTIVE DES SOCIÉTÉS SAVANTES

CORTE TNT

 
 

 

NAN D 772

ADD NTETINITEITTT
MAT TIITITITNMITTTTITIT SE.
MAT TITTITTMITITTITIT

VUÉ PARTIELLE DES-RÉPERTOIRES DE L'INSTITUT. INTERNATIONAL DE BIBLIOGRAPHIE

 

 

 

 

AUE VEYDOT.

OSCAR LAMBERTY» 70»

TT IMP.

BRUX:

 
