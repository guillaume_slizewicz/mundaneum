Epreuves _ FASG. 50

1910-05-10

CONGRÉS MONDIAL

DES

ASSOCIATIONS INTERNATIONALES

BRUXELLES, o-11 MAI io.

Association catholique internationale des œuvres
de protection de la jeune fille,

Historique. — L'idée première de l’Association fut inspirée,
en 1896, à M. Léon Genoud, de Fribourg (Suisse), par la constata-
tion douloureuse des dangers que courent les jeunes filles placées en
Hongrie et en Russie. M®° Louise de Reynold prit l'initiative de la
créalion de l'OEuvre qui, au début, fut envisagée comme devant
protéger particulièrement les Suissesses catholiques émigrant vers
l'Europe orientale.

Depuis vingt ans déjà fonctionnait une œuvre protestante, celle
des Amies de la jeune fille, ayant un programme analogue ; aussi
M de Reynold songea-t-elle d’abord simplement à greffler sur ce
tronc, déjà vigoureux, une branche catholique.

Mais les Amies de la jeune fille déclarérent que leur constitution
essentiellement « évangélique » ne permettait pas Funion proposée.
Se rendant à ces considérations, M° de Reynold fonda à Fribourg,
le 22 septembre 1896, l'OEuvre catholique suisse de protection de
la jeune fille.

 
 

—3—

Un an plus tard, l’Association s'était étendue dans la plus grande
partie de la Suisse. Le 19 août 4897, elle tenait sa première réunion
générale. |

Comprenant la nécessité d’une entente internationale pour la pro-
tection de l'émigrante, les initiateurs du Congrès avaient convié à
cette mémorable séance des représentants des principaux pays de
l'Europe, atu que l'OEuvre püt être proclamée internationale.

Depuis lors, cette Association à organisé plusieurs congrès inter-
nationaux qui ont eu lieu dans l'ordre suivant :

4897 ... . . . . . Fribourg
4900. . . . . . . Paris.
4902 . . . . . . . Munich.
1906. . . . . . . Paris.
1909. . . . . . . Strasbourg.

L'Association est régie par les statuts suivants :

Statuts.
Dispositions générales.

ÂRTICLE PREMIER. — É'Associalion catholique internationale des
OEuvres de protection de la jeune fille a pour but :

4° De réunir étroitement, pour un programme d’action commune,
les œuvres et institutions qui, dans les diverses pays, s'occupent de
la protection de la jeune fille, tout en facilitant entre elles un
échange de services réciproques ;

2 De faciliter et de poursuivre la fondation de ces sociétés et
institutions dans les pays qui en sont dépourvus;

8° De gagner à l'Association, comme membres isolés, les personnes
qui s'occupent d'œuvres entrant dans son programme.

AeT. 2. — L'Association, fondée avec l'approbation et la bénédic-
tion de Sa Saintelé le Pape Léon XII, se place sous la protection
spéciale de Notre-Dame du Bon Conseil; elle soumet son action à
l'autorité écclésiastique et entretient avec elle d'étroites relations.

Arr. 3. — L'Association a son siège à Fribourg, en Suisse.
Les couleurs de l'Association sont le jaune et le blanc.

 

    
 

Membres de l'Association.

Arr, 4, — Peuvent être admis comme membres de Association
par le Comité international :

4° Les Associations de protection de la jeune fille ; dans les pays
où un Comité national est organisé, elles se font agréer par l’entre-
mise de ce dernier;

2 Toutes les sociétés el instilutions catholiques qui, sous
n'importe quel nom, ont pour but la protection ou le patronage de la
jeune title;

8° Tous les catholiques qui, soit par leurs services personnels, soit
par des dons volontaires, veulent soutenir l'Association.

AnT. 5. — Les sociétés de protection de la jeune fille payent à
l'Association internationale, comme contribution aux frais généraux,
le 2° de leurs recettes ordinaires ou quelque autre tribut annuel
fixé d'entente avec le Comité international {{).

Les institutions payent une cotisation annuelle dont le montant est
à fixer par leur bon vouloir.

Les membres isolés versent à la Caisse internationale 2 francs
par an ou, une fois pour toutes, 50 francs. En retour, ils reçoivent
gratuitement le catalogue général des membres de l'Association.

AnT. G. — Les sociétés et institutions qui adhèrent à l'Association
conservent une complète autonomie, à laquelle on ne peut porter
atteinte sous aucun prétexte.

De même, le mode d'organisation de l’OEuvre dans chaque pays est
absolument libre. Toutefois, il est recommandé à toutes les sociétés
de proteclion de la jeune file de former entre elles de grandes fédé-
rations circonserites par les frontières adminisiratives, civiles ou
religieuses, et d'en donner la direction à un Comité national, pro-
vincial ou diocésain.

 

@} Chaque natiun organisée versera annuellement au Bureau exécutif de Fribourg
la somme de 200 francs pour couvrir les frais de l'organisation et de la propagande
internalionale, {LVme Congrès international, Paris, octobre 406.)

  
   

Direction.

AT. 7. — L’Associalion est placée sous la direction du Comité
international assisté par le Conseil international,

Ant. 8 — Le Comité international a son stège à Fribourg, en
Suisse: il se compose de la Présidente générale, de la Vice-Prési-
dente, de la Secrétaire générale, de la Trésorière et de dix
Conseillers ou Conseillères nommés par l'assemblée générale : pour
celte élection, chaque pays représenté à l'assemblée a une voix.

Le Comité international a le droit de s’adjoindre encore quelques
membres par voie de cooptation, mais sans jamais pouvoir, dans
son ensemble, en compter plus de trente. Les membres du Comité
sont élus pour trois ans, soit d'une assemblée générale à l'autre, et
sont rééligibles.

AT. 9. — Le Comité international dirige l'Association. Ses prin-
cipales attributions sont :

4° La propagande en faveur de l’Associalion ;

2 L'entretien de rapports constants avec les Comités de chaque
pays, ceux-ci pouvant, du reste, avoir entre eux des relalions
directes;

3 La rédaction et la publication du Bulletin mensuel;

4 Ea publication annuelle du catalogue des membres de l'Asso-
ciation ;

ë° La préparation de l'assemblée générale et l'élaboration de son
programme.

Le Comité prend ses décistons à la majorité des membres présents
ou régulièrement représentés.

AnT. 10. — La Présidente générale ou la Vice-Présidente repré-
sente l'Association vis-à-vis des tiers. La Présidente on la Vice-
Présidente a, conjointement avec la Secrétaire générale, la signature
sociale,

ART. 11. —— La Secrétaire générale tient le protocole des séances
du Comité international et du Conseil international, ainsi que de
l'assemblée générale: elle dirige la correspondance, assure la publi-
cation du Bulletin mensuel, ainsi que de lous les autres communiqués
et imprimés de l'Assucialion, Elle présente à l’assemblée générale

    
 

8 —

un rapport sur la marche de l'Association et sur le travail du Secré-
larial.

ART. 12, — la Trésorière générale assure la conservation des
biens sociaux ; elle perçoit les cotisations des membres ; elle présente
annuellement au Comite un compte rendu de sa gestion et à l'assem-
blée générale, un rapport sur la situation financière de l'Association.

ART. 13. - Le Conseil international se compose :
- 4° Des membres du Comilé international;

2 Des représentants de chaque pays nommés par leurs Comités
nationaux el présentés tous les trois ans à l'assemblée générale, qui
lixera leur nombre. Lo

Les membres du Conseil international ont pour mission la défense
des intérêts généraux de l’Association et le suin de sa propagande
extensive. La Secrétaire générale leur fait parvenir l’ordre du jour et
le procès-verbal des séances du Comité international.

Le Conseil international se réunit avant chaque assemblée géné-

rale. [l'est permis d'y voter par délégation manusérite ou par corres-
pondance.

Assemblée générale.

Axr, 44, -— Une assemblée générale des membres de l'Association
a lieu au moins tous les trois ans.

Le Comité international désigne la date de l'assemblée générale,
dont ii établit l'ordre du jour.

Aucune proposilion ne peut être soumise aux délibérations sans
avoir été présentée au Comité international trois mois au moins
avant l'assemblée générale.

L'assemblée générale est présidée par la Présidente générale. I
jui appartient :

4° De nommer la Présidente générale, la Vice-Présidente, la Secré-
taire générale, la Trésorière générale et dix membres du Comité
intérnalional ;

2 D'approuver les décisions du Comité international et sa gestion
administrative et financière.

3 De fixer le lieu de la prochaine assemblée;

4 De reviser les statuts et de prononcer la dissolution de l’Asso-
cialion.

 

 
Chaque pays a le droit de nommer à l’assemblée générale un délé-
gué de son choix, qui dispose d’une seule voix dans les votalions. Ce
délégué doit produire une attestation de ses pouvoirs.

Revision des statuts et dissolution de l'Association.

ART. 15. — Toute proposition ayant pour but la revision des sta-
tuts ou la dissolution de l’Association doit étre portée devant le
Comité international six mois au moins avant la réunion de l'assem-
blée générale.

Elle doit être appuyée par les représentants de trois pays au
moins.

La dissolution de l’\ssociation ne peut être prononcée que par les
deux tiers des membres des représentants des différents pays.

ART. 16. — Dans le cas de dissolution de l'Association, l'assem-
blée générale détermine l'usage de l'actif de l'Association en faveur
d'une œuvre ou institution ayant un but analogue au sien.

Un règlement spécial détermine la composition et le fonctionne-
ment du Conseil international,

Règlement intérieur du Conseil international.

ARTICLE PREMIER. — Le Conseil international de l'Association se
compose :

a) Des membres du Bureau exécutif et du Comité international
résidant à Fribourg ;

b) Des délégués des nations organisées, c'est-à-dire possédant un
comité national ou un organe central équivalent. Dans ce Conseil, les
nations suivantes possèdent d'après le nombre de leurs œuvres locales
et de leurs adhérents :

Allemagne, 14 représentants ;

Autriche, 5 représentants;

Belgique, 10 représentants;

Espagne, 7 représentants ;

France, 18 représentants;

Grande-Bretagne, 15 représentants, dont 3 pour l'Irlande.
Hongrie, 2 représentants ;

Ltalie, 10 représentants;

Luxembourg, 3 représentants ;

Pays-Bas, T représentants ;

Suisse, 12 représentants;

Republique Argentine, 5 représentants.

AnT. 2. — Le Conseil international se réunit tous les trois ans
avant le Congrès international. Il peut, toutefois, être convoqué dans
l'intervalle, si trois nations organisées le demandent ou si le Bureau
de Fribourg le juge nécessaire.

ART. 5. — Le Conseil examine les propositions qui doivent être
soumises au Congrès, délibère sur les intérêts généraux de l’Associa-
tion, propose la revision des statuts et, en cas de conflit entre deux
ou plusieurs comités nationaux, tranche le différend. [1 prend toutes
décisions et toutes mesures jugées utiles à l’Association internatio-
nale.

ART. 4. — Le Bureau exécutif et le Comité international perma-
nent, siégeant à Fribourg, relèvent du Conseil international seul et
n'ont de comptes à rendre qu'à lui.

ART. à. — Le Conseil admet éventuellement dans son sein les
représentants des nouvelles nations qui se dotent de l’organisation
centrale nécessaire pour avoir droit à une délégation au Conseil
international.

ART. 6 — Le Conseil international est présidé par la Présidente
internationale, à son défaut par une personne désignée par le Bureau
exécutif.

ART. 7. — Tous les membres ont voix délibérative et consultative,
mais dans les scrutins et volalions chaque pays n'en a qu’une seule.
Les membres absents peuvent voter par délégation manuscrite signée
ou par correspondance.

ART. 8. — La Présidente du Conseil ne vote pas, sauf en cas
d'égalité dans les suffrages exprimés, elle tranche alors dans un sens
ou dans l'autre.

 
Arr. 9. — Le procès-verbal des assemblées du Conseil est tenu
par le Secrétariat permanent de Fribourg.

ART. 40. — Pour les séances du Conseil, les membres doivent
porter en évidence l’insigne de l'Association.

Ant. 11. — [Les séances du Conseil sont tenues dans l’ordre
suivant :

a) Prière (Pater, Ave et une invocation à Notre-Dame du Bon-
Conseil) ;

b)} Lecture du procès-verbal ;

c) Appel nominal des membres du Conseil :

d) Discussion des propositions du Bureau et du Comité de
Fribourg ;

e) Discussion des propositions des comités nationaux ;

f) Discussions diverses.

ART. 12. — Aucune proposition ne peut être discutée au sein du
Conseil international, si elle n'a pas été indiquée d'avance, parmi les
tractanda de la séance, dans la circulaire de convocation.

ART. 15. — Cette circulaire de convocation doit être toujours
adressée un mois avant la date de la séance du Conseil international,
afin de permettre anx comités nationaux de se réunir et de donner à
leurs représentants un mandat motivé.

ART. 14. — Le Bureau exécutif et le Comité international perma-
nent, siégeant à Fribourg, convoquent les réunions du Conseil

international, en dressent le procès-verbal et exécutent ses décisions
eventuelles.

ART. 15. — Les membres du Conseil international sont élus tous
les trois ans par les comités nationaux dont ils sont les représen-
tants. Ces nominations sont soumises à l'assemblée du Congrès, qui
fixe le nombre des représentants auquel chaque pays a droit. Les élus
sont rééligibles.

Les membres du Comité international et du Bureau exécutif,
siégeant à Fribourg, sont réélus tous les trois ans par l'assemblée
générale du Congrès. Ils sont rééligibles.

ART. 16. — Le siège social du Conseil international est à Fri-
bourg (Suisse). C’est là que sont conservés :

a) Ses archives;

b) Sa caisse :

c) Son avoir éventuel.

ART. 47. — Les membres du Conseil international qui sont
empéchés de prendre part à une séance où ils ont été régulièrement
convoqués doivent excuser leur absence par une lettre motivée.

AnT. 48. — Toutes les communications, lettres, propositions,
réclamations intéressant le (Conseil international devront être
adressées au Secrétariat permanent du Bureau exécutif, 16, rue
Saint-lierre, Fribourg (Suisse).

ART. 19. — Le Bureau de Fribourg peut convoquer exception-
nellement au Conseil international les représentants d’un comité,
d'une œuvre ou d’une institution locale affiliée à l'Association, qui
auraient à y défendre une proposition faile, un vœu soumis par eux
au Conseil. Il peut également y convoquer les personnalités mar-
quantes (évêques, ministres, savants, présidents de grandes associa-
tions catholiques, etc.), soit pour leur rendre hommage, soit pour
les entendre sur un sujet discuté par le Conseil.

Ces invitations sont faites pour une seule séance et renouvelables
suivant les circonstances. Elles donnent seulement à ceux qui en
sont l’objet voix consultative. Les comités nationaux peuvent
demander, en les motivant, ces invitations extraordinaires au Bureau
de Fribourg.

Finances.

À. — Compte de caisse de l'Association eatholique internationale des OEuvres
de protection de la jeune fille, du 4° janvier au 84 décembre 4909.

RECETTES.
Er 786 25

' » 1,240  »
Abonnements et annonces du Bulletin 2,285 10
\ » 1,578 16
Vente de broches et d’insignes. -. . » 225 50
Adhésions au Congrès de Strasbourg . » 41,615 »
Quête faite au Congrès de Strasbourg. » 409 »
Intérêt du fonds social » 43 85

Total. Fr. 8,171 86

 

 
—#—

DÉPENSES.
Frais de correspondance et divers.

si us

 

Impression du Bulletin. Fr. 4,037 50
Rédaction du Bulletin . » 75
Port du Bulletin. . . » 221 05

 

Loyer, chauffage et service des bureaux .
Frais de propagande, voyages, expositions.
Frais du Congrès de Strasbourg, imprimés,

location de la salle, divers É

Subsides pour la Mission de la gare de

 

Versement pour l'impression du compte
rendu du Congrès de Strasbourg
Note des ateliers du Technicum pour deux
»
»

Total. . Fr.

Total des dépenses . . . . . . . Fr.
Total des recettes »

Déficit. . tr.

B. — Compte du fonds social.

Fonds social au 1° janvier 4909 . . . Fr.
j »

Au 4* janvier 1910 . Fr.

Uélail des cotisations reçues en 1900.

Comité national allemand . . . . . Fr.

A l »

À L »
»
»

Comité de Montevideo
Russie
RS nd mai e à « 0" %
OEuvres et membres divers »

Total: + fr.

 

100
800

220
5

8,978
8,978

8,171

806

1,169 29

806 84

902 45

147
125
21H)
200
200

90

ro

25
21

10
222

1,240

se

Siège. - Le siège du Secrétariat international est à Fribourg,
en Suisse, 16, rue Saint-Pierre. Cette ville a été choisie comme
étant le berceau de l'Association et aussi a cause de sa situation
centrale dans un pays neutre, aux confins des langues française, alle-
mande et italienne.

L'Association internationale n'a pas la personnification civile à
cause de son internationalilé ; il n’a pas été jusqu’à ce jour possible
de l'obtenir, elle ne reçoit pas de subside de gouvernement en qua-
lité d’Association internationale.

Le nombre des membres est de 10,000 environ, répartis prineipa-
lement en Europe. L'Association a des comités nationaux en Alle-
magne, en Grande-Bretagne, en France, en Belgique, dans les
Pays-Bas, en Italie, au | uxembourg, en Suisse, en Espagne, dans la
République Argentine. Elle a des correspondants et œuvres affiliées
en Autriche-Hongrie, en Grèce, dans la principauté de Monaco, eu
Norvège, en Russie et Pologne, en Suède, en Turquie d'Europe et
d'Asie, aux États-Unis, au Canada, en Amérique centrale, dans les
divers États de l'Amérique du Sud, en Afrique et en Australie.

L'Association poursuit le bien matériel, moral et religieux de la
jeune fille, en s'occupant de son placement, de sa protection dans les
voyages, de son patronage sous des formes diverses, de l’organisa-
tion de cours en vue de sa formation professionnelle, de la création,
en sa faveur, de caisses d'épargne et de retraite, d’associalions
d’ouvrières de fabrique, de domestiques, etc., de la lutte contre la
traite des blanches, enlin, pour les malheureuses qui ont failli, de
tentatives de relèvement moral.

Publications. Le Secrétariat international fait parvenir
chaque année, à lous ses comités nationaux, à ses œuvres alliliées et
correspondants, un Questionnaire qui lui donne des renseignements
statistiques et le tient au courant de l’activité de chacune des
sections.

L'Association édite, outre les comptes rendus des congrès, un
Bulletin mensuel en langues française et allemande qui relate la
chronique de l'OEuvre et sert de lien entre les comités et associés de
l'Office central. (Prix d'abonnement : fr. 1.75 pour la Suisse,
fr. 2.10 pour les autres pays de l'Union postale.) Tous les comités

 
= #9 —

peuvent y insérer des communications d'intérêt général. Elle pablie
un Annuaire contenant la liste et les adresses des comités, secré-
tariats, missions des gares, correspondants el œuvres afliliées. Les
membres de l'Association qui versent une cotisation d'au moins
2 franes à la Caisse internationale + ont droit gratuitement. (Le prix
de l'Annuaire pour les autres membres est de 1 franc.) L'Association
édite aussi un Guide à l'usage des jeunes filles, leur donnant, outre
des avis moraux et des renseignements pratiques, de nombreuses
adresses de maisons d'accueil, bureaux de placement, patronages et
missions des gares dans les principales villes. (Prix du Guide :
50 centimes.)
Enfin, les tracts destinés à faire connaître l'Association.

Bibliographie.

Annuaire de l'Association catholique internationale des UEuvres pour 1a
protection de la jeune fille, 1909. — Prix : fr. 4.20.
Guide de la jeune fille, 1908. — Prix tr. 0.50.

Bulletin mensuel de l'Association en langues française et allemande. —
Prix d'abonnement : fr. 1.75 pour la Suisse, fr. 2.10 pour les pays
de l’Union postale.

Bericht uber den dritten Kongress des internationalen Verbands der
katholischen Mädchenschutzvereine abgehalten zu Munchen am 17,18
und 19 Juni 1902. — Prix : fr. 1.00

Compte rendu du IVe Congrès international de l'Association. Paris, 19,
20, 21 octobre 1906. — Prix : fr. 1.50.

Compte rendu du Ve Congrès international de l'Association. Strasbourg,
17, 48 et 19 juin 1909. — Prix : fr, 2.50.

Bericht über die VI. Generalversammlung des Verbandes der Schweiz.
kath. Mädchenschulzvereine abgehalten zu St. Gallen am 29. und
30. September 1902. — Prix : fr. 1.00.

Compte rendu de la VII: Assemblée générale de l'Association des œuvres
catholiques suisses de protection de la jeune Jille. Fribourg, 7 et
8 octobre 1903. — Pris : fr. 0.80.

Bericht über die VIII, Generalversammlung des Verbands der Schweiz
abgehalten in Zurich am 27. und 28. September 1901.

= =

Compte rendu de la X° Assemblée générale de l'Association des œuvres
catholiques suisses de protection de la jeune fille à Fribourg, les 21 et
22 septembre 1906. — Prix : fr. 0.70.

Bericht über die XI. Generalversammlung des Verbandes der Schweiz.
kath. Madchenschutzvereine, abgehalten in Basel am 21. und
22. Oktober 1907. — Prix : fr. 0.80.

Compte rendu de la XI1° Assemblée générale nationale suisse à Lau-
sanne, les 23 et 24 septembre 1908. — Prix : fr. 1.35.

Bericht über die XIII. Generalversammlung des schweiz. Verbands,
abgehallen in Zug am 20. und 21. August 1909. — Prix : fr. 1.35.

Statuts de l'Association catholique internationale des OEuvres de pro-
tection de la jeune fille.

Notice explicalive sur lAssociation catholique internationale des
OEuvres de protection de la jeune fille.

Notice historique de l'OEuvre catholique suisse de protection de la jeune
fille. — Prix : fr. 0.30

Baronne ne Monrtenacn, L’Assoriation catholique internationale des
OEuvres de protection de la jeune fille : Histoire, but, programme,
services. Extrait de l'enquête sociale de la Croix, T et 8 septem-
bre 1906.

Baronne pe MontTeNaca, L'Association catholique internationale des
OEuvres de protection de la jeune fille. Rapport présenté à la réu-
nion des Comités suisses, le 8 octobre 1903.

Baronne pe MonTenaca, L'OEuvre de protection de la jeune fille : Orga-
nisation, propagande, action sociale. Rapport présenté à la Réunion
générale des catholiques à Sion, 1904.

Les nouvelles formes de notre protection sociale. Discours de Me la
baronne pe MoNTENACH, vice-présidente de l'Association, prononcé
au Congrès international de Strasbourg, le 49 juin 1909.

Homes et Bureaux de placement. Rapport présenté par Me HExry
Décux au IVe Congrès international de l'Association à Paris, le
20 octobre 1906.

Le logement des domestiques, son influence sanitaire, morale et sociale.
Rapport présenté au Congrès international de Strasbourg, juin
1909, par M. Geonces DE MoxTENAGu.

Fürsorge für Kellnerinnen am eidgenossischen Srhütsenfest im St.
Gallen (16-28 Juli 1904). Vortrag gehalten von Frau WiNTER-
HALtERr-EucsTer an die Jahresversammlung der schweizerischen
katholischen Mädchenschutzvereine in Zurich, September 1904.

 
 

Compte rendu analytique du V° Congrès national français de l'Associa-
tion, tenu à Paris, les 11 et 12 juin 1908. Dijon, imprimerie
Jobard. On peut se procurer ces publicalions au secrétariat inter-
national, 46, rue Saint-Pierre, Fribourg (Suisse).

Compte rendu du VIe Congrès national français, tanu à Nancy, les 43,
44 et 15 juin 1909. Dijon, imprimerie Jobard.

L'action populaire : Mie H, D. et J. R,, Protection catholique intérna-
tionale de la jeune fille. Rédaction et administration : Reims, 5, rue
des Trois Raisinets. — Prix : fr. 0.25.

 
