JOCUMENTS. DE CRIMINOLOGIE.
0 ET DE MÉDECINE LÉGALE

  

 

UN CODE TÉLÉGRAPHIQUE
DU PORTRAIT
PARLÉ

TAR

R.-A REISS

Professeur de Police scientifique
à l'Université de Lausanne,

 

PARIS
À. MALOINE, LIBRAIRE-ÉDITEUR

259%

7. rue de l'École de Médecine, 25-93

1907
UN CODE TÉLÉGRAPHIQUE :

DU PORTRAIT PARLÉ

M. Paul Otlet, secrétaire général de l'Institut international de
Bibliographie à Bruxelles, a publié dans le fascicule 1-3 de
l’année 1906 du Bulletin dudit institut une ébauche de classe-
ment de fiches anthropométriques (syst, Bertillon) par la méthode
de classement décimal. M. Otlet donne dans ce même travail la
caractéristique suivante de sa méthode :

« La méthode consiste à attribuer à chaque individu anthropo-
métré (délinquants, conscrits, etc.) un numéro classificateur basé
sur les éléments recognitifs les plus caractéristiques de leur per-
sonne physique et inscrit en quelque sorte dans leurs organes.
On pourra toujours lrouver ensuite, sous Le même nombre clas-
sificateur, dont les éléments de formation sont invariables, tous
les documents (photographies, pièces, rapports, ete.) ayant trait
à un même individu anthropométré à divers âges. » |

M. Otlet, comme nous venons de le dire plus haut, utilise done
pour ce classement décimal les données anthropométriques. Ge
classement est certainement possible, mais il est franchement
inférieur au classement anthropométrique de M. Bertillon.

Nous avons eu la bonne fortune de nous entretenir avec
M. Otlet sur son classement décimal anthropométrique, et nous
lui avons fait part de nos doutes sur la commodité et la précision
de son système, M. Otlet nous a alors objecté que peut-être, dans
le portrait parlé, le classement décimal pourrait être pratiqué
avec succès. Nous avons cherché à mettre en pratique l'idée de
M. Otlet, et, dans ce qui suit, nous soumettons aux intéressés un
code télégraphique et international du signalement par le por-
trait parlé basé sur la classification décimale qui nous parait être
d'une certaine importance pour les relations entre les dilfé-
rentes directions de police du même pays et des pays étrangers.

En effet, ce code télégraphique que nous allons exposer réunit,

à côté d'une grande simplicité, les qualités inappréciables en
matière de police, d'être international, précis et bon marché dans
la pratique.

- Nous supposons que le lecteur est au courant de ia elassifica-
tion décimale appliquée en bibliographique. Employons ce même
principe de classement au portrait parlé et nous aurons alors à
répertorier d'abord les éléments principaux du portrait parlé
dans les nombres 0,1 — 0,9. La elassification qui nous paraît être
la plus pratique est la suivante :

0,1 front,

O2 nez.

0,3 orcille.

0,4 bouche, lèvres, menton.

0,5 contour général du profil et contour naso-buccal,

0,6 contour général de la face, profil du crâne proprement dit.

0,7 sourcils, paupières, globes et orbiles,

0,$ cou, les rides, carrure, ceinture, attitude, allure générale,
VOIX. |

0,9 veux, cheveux, barbe, pigmentation de la peau.

Si done nous voyons sur un signalement télégraphique 0,1 nous
savons tout de suite qu'il s'agit du front; 0,3 nous indique que :
l'oreille est visée par le signalement. On sait, d'autre part, que
le front est examiné au point de vue : du degré de saillie des
arcades sourcilières ; de l’inclinaison de sa ligne de profil; de sa
“hauteur; de sa largeur; de quelques particularités. Classons ces
caractères par la méthode décimale en partant du o,1 pour l'élé-
ment : le front, et nous aurons alors les chiffres suivants :

0,11 degré de saillie des arcades soureilières.
5e

0,12 inctinaison de la ligne de profil du front,
0,15 hauteur du front. °

0,14 largeur du front.

0,15 particularités du front.

Les arcades sourcilières peuvent aller du très petit au très

5

grand, exprimées par la méthode décimale :
; EXP P

6,111 arcades trés petiles.

0,112
0,113
0,114
0,415
0,116
0,117

pelites.
(petites).
intermédiaires.
(grandes).
grandes.

très grandes.

L'inclinaison du front va du très fuyant au proéminent
exprimée par la méthode décimale :

0,481 inclinaison très fuyante,

0,122
0,128
0,124
0,125
0,126
0,127

fuyante.
{fuyante).
intermédiaire.
(verticale),
verlicale.
proéminente,

La hauteur du front va du très petit au très grand :

0,131 hauteur du front très petite.

0,132
0,133
0,134
0,135
0,136
0,187

— petite.

—  (petite},

— intermédiaire.
—  {grande).

— grande.

— très grande.

Pour la largeur du front :

0,141 largeur du front très pelite.

0,142
0,143
"0,144

— petite.
— (petite).
— intermédiaire,
6 —

0,145 largeur du front {grande).
0,146  — — grande.
OT — — très grande.

Pour les particularités du front :

0,151 sinus frontaux.
0,152 bosses frontales.
0,153 profil courbe.
0,154 fossetle frontale.

Les sinus frontaux peuvent être petits, intermédiaires, grands,
Nous signalerons cette particularité en ajoutant au 0,151 les
chiffres 1,2, 3, ce qui nous donné alors dans le répertoire déci-
mal les valeurs suivantes :

0,15f1 sinus frontaux petits.
O5 — — intermédiaires.
04513 — — grands.

Ainsi que nous avons pu répertorier par la méthode décimale le
front avec tous ses caractères, nous pouvons procéder de la même
3 M
façon pour tous les autres éléments de la figure et du portrait
5 D
parlé en général. En prenant comme base la classification des
éléments principaux mentionnés antérieurement, nous aurons
alors le répertoire décimal, formant le code télégraphique suivant :
 

 

 

 

0,1

0

0
Front. °
0

1
À

3 hauteur du front.

,14 largeur du front.
.145 particularités du front.

11 degré de saillie des arcades sourcilières.
2 l'inchinaison de ia ligne de profil du front.

 

 

 

0,111 arcades petites. 0,181 inelinaison fuyante.

OAI2 — petiles, 6,122 — fuyante.

0,113 — {petites}. 0,188 — (fuyante).
DAt4k — intermédiaires. 0,124 — intermédiaire,
0,415 — (grandes). 0,125 —- (verticale).
0,146 — grandes. 0,126 _— verticale.

DIT — grandes. 0,127 _— proéminente.
0,431 hauteur du front pelite. Ô,L4! largeur du front petite.
0,132 — — petite. 0443  — — potile.
0,133 — —  {pelite}, 0,413  — — {petite}.
0,134 — —  intermédre,| 0,14  — —  intermédre
04385. — — (grande). O,145  — — grande}
0,436 — — grande, 0,146  — — grande.
0,137 — — grande. OLIT  — — grande.
0,154 sinus frontaux. 0,1512 sinus frontaux petits.
6,152 bosses frontales. 01514 — — intermédres,
0,153 profil courbe, O,1516 —  — grands.
0,154 fossette frontale. .

 

 

0,2 Nez.

{  D,24 racine du nez.
0.22 dos du nez.
0,23 base du nez.
0,24 hauteur du nez.
0,25 saillie.

0.26 largeur.
0,27 particularités.

=

 

0,211
0,212
‘0,213
0.214
0,215
0,216
0,217

racine du nez petite.

— —  pelile. 0,822 —
— —  (pelite). 0,823 —
— — intermédiaire, 0,224 —
— — (grande), 0,228  —
— — grande. 0,826  —
— — grande. 0,227 —

0,228 —

0,229 —

(281 dos du nez cave.

cave.
(cave).
rectiligne,
(vexe},
Vexe.
vexe.
busqué.
busqué,

 

 

1 Les formes extrêmes : Lrès petit et irès grand, etc, (petil souligné et grand
souligné} sont indiquées dans ce code par l'impression en caractères italiques,
Chaque fois que l'échelle à sept échelons était applicable, nous Favons con-
servée pour la suile &es chifires; toutefois, dans l'énumération, nous avons
laissé de côté, pour les caractères de moindre importance, les formes limites.

 

 

 

 
mu Bu

 

 

 

0,821{ -cave sinueux.

0,281 base relevée.

 

 

 

 

02221 cave sinueux. 0,282 — relevée.

0,2231 (cave sinueux}, 0,233 — (relevée).

O,2241 rectiligne sinueux. 0,234 — horizontale.

0,2251 (vexe sinueux). 0,235 — f{abaissée),

0,261 vexe sinueux, 0,296 — abaissée.

Ô,2871 vexe sinueux. 0237 — abaissée,

0,8281 busqué sinueux.

0,2291 busqué sinueux.

0,241 hautcur du nez petite. 0,951 saillie du nez petite.

0,242  — — petite. 0,952 —  — petite.
0,243  — —  {petite), 0,253 +—  — (petite).
0244  — — intermédiaire | 0,254 —  — intermédiaire,
0,245 — — (grande). 0,855 —  -- (grande).
0,846  — — grande. 0,256 —  — grande.
O24T — — grande, 0,257 —  — grande.
0,261 largeur du nez petite. 0,271 particularités du dos du nez.
0,262 — — petite. 0,272 — du bout du nez.
0,263 — — (petite). 0,273 : — de lacloison.
ORG# —  — intermédiaire, | 0,274 —. des marines.
0,265 — — (grande), 0,275 —  delaracine.
6,266  —  — grande.

0,267 —  _— grande.

 

0,2711 dos du nezenS.
0,271% méplal du dos,
0,2713 dos mince,

0,27L4 dos large.

0,2715 dos écrasé,

0,2716 incurvé à droite.
0,8717 incurvé à gauche.
0,2718 dos du nez en selle,

0,2721 bout du nez bilohé.
0,2722 méplat du bout.
0,2723 bout effilé.

 

ORTRE  — gros.

0,2725 — pointu.

02726 — dévié à droite,
O,8387 —  — à gauche.

0,2728 ncx couperosé,

 

 

 

0,2731 cloison découverte. | 0,874 narinos empâtées.

0,273? — non apparente, 0,278 -—  dilatées.
O£T43 —  pincées.

0,8744 = récurrentes.

02751 racine du nez étroile.

ORTSZ — — large,

0,2758 — — à haul.pelite.

0,8754  — — àhaut grande.

 

 

 

 

 

 

L
— 9 —

 

0,341 bordure ou hélix de l’oreilie.
0,32 le lobe.
0,33 l'antitragus.
0,34 le pli inférieur.
0,3 Oreille. 0,35 le pli supérieur.
0,36 forme générale de oreille.
0,37 écartement du pavilion.
0,38 insertion de l'oreille.
0,39 particularités de la conque.

 

0,311i bordure originelle petite,

 

0,344 bordure originelle de l’hélif OSUR  — pelle.

8 U . 14. . .

0,342 bordure supérieure, Gas E D {pete

0,313 — postérieure, 0. 9 (45 | “

0,344 particularités de V'héfix. 3115 — — {grande}.
0,3116 — — grande.
OUT — — grande,

0,3181 supérieure petite, 0,313 postérieure petite.

0,312? — petite. 0,3182 — petite.

ü,3185 — (petite). 0,3133 — {petite}.

0,3124 — intermédiaire. | 0,3134 — intermédiaire.

0,3195 — (grande). 0,3135 — (grande).

0,3126 — grande. 0,3136 — grande.

0,3127 —. grande. 0,3187 — grande.

 

0,344 particularités darwiniennes.
0,3142 particularités de la bordure.

rieur de l'oreille.

0,31421 bordure échancrée.
0,31422 -— froissée
0,31428 — post. fendue.
0,31424 — cicat.et gelée.

0,3448 particularités du contour supé- |

6,31414 nodosité darwinienne.
0,91442 élargissement darwinien,
0,41413 saillie darwinienne.
0,3144144 tuberculé darwinien.

0,34431 contour sup. aigu.

0,31432 contour supéro-antérieur
aigu.

0,31433 contour supéro-postérieur
en équerre.

0,31434 contour supérieur bicoudé,

0,31435 contour supérieur obtus
aigu.

 

0,321 forme äu bord libre du lobe.
0,322 adhérence.

0,923 modèle de sa surface externe.
0,324 dimensions.

0,325 particularités.

 

0,841 descendant,

0,8212 descendant-équerre.
0,3213 équerre.

0,3814 intermédiaire.
0,5215 golfe.

 

 

 

 

 

 

 
 

—. 19 —

 

0,221 Jendu.

0,822? fendu.

0,38228 (fendu).
0,8224 intermédiaire.
0,3225 (sillonné).
0,8226 sillonné.
0,3827 isolé,

0,3281 traversé,
0,3282 traversé.
0,8233 (traversé).
0,8284 intermédiaire.
0,8285 (uni).

0,3236 uni.

0,8237 éminent. :

 

0,384L petit.
0,8242 peut.
0,8243 (petit).
0,8244 moyen.
0,3245 (grand).
0,2246 grané.
0,8247 grand.

0,3251 particularité de la forme du
lobe.

0,325? particularités des rides du
lobe, : |

0,3253 particularités de la peau du
lobe. |

 

0,52511 lobe fendu.

0,82512 lobe pointu.

0,52513 lobe carré.

0,82514 Iobe oblique interne.
0,32515 lobe oblique externe.
0,82516 lobe à torsion antérieure.

0,32521 lobe à fosseite.
0,32522 lobe à virgule.

0,32523 ride oblique postérieure.
0,32524 lobe à tot.

 

.0,825281 lobe poilu.

 

0,334 inclinaison de l'antitragus.
0,332 profil de l'antitragus.

0,333 renversement de l’antitragus.
0,384 volume de l'antitragus.

0,335 particularités,

0,3314 horizontale,
0,3812 horizontale.
0,3313 (horizontale),
0,3314 intermédiaire.
0,3315 (oblique).
0,5316 oblique.
0,3317 oblique.

 

G,3321 cave,

0,3822 cave.

0,3323 rectiligne.
0,3324 intermédiaire.
0,8325 (saillant).
0,3326 saillant,
0,3327 saillant.

 

0,3331 versé.
0,3332 versé.

70,3233 (versé.

0,334 intermédiaire.

” 0,3335 (droit).
: 0,3336 droit,
. 0,3337 droit.

 

 

 

 

 

 
LI —

 

0,3341 nul.

O,3342 petit.

0,3343 petit.

0,3344 intermédiaire.
0,3345 (grand).
0,5346 grand.
0,83847 grand.

0,3351 antitragus fusionné avec
lhélix.

0,3352 tragus pointu.

0,3353 tragus bifurqué. :

0,839 tragus et antitrag. poilus.

0,3353 incisurepostantitragienne.

0,3396 canal étroit,

 

 

6,841 saillie du pli inférieur.
0,542 particularités du pli inférieur.

0,3411 cave.

0,3412 cave.

0,843 (cave).

0,3414 intermédiaire,
0,3445 (vex.).

0,3416 vex.

O,3417 vez,

 

0,8421 fossette naviculaire en pointe.

 

0,354 saillie du pli supérieur.
0,352 particularités. ‘

- 0,3518 effacé.

0,851 nul,

0,8514 intermédiaire.
0,3516 accentué.
0,8517 accentué,

 

0,3521 sillons contigus,

0,3522 sillons séparés.

0,3823 pli supérieur à plusieurs
branches.

0,3524 pli supérieur joignant la!

bordure.
0,8525 hématome du pli supérieur,

0,361 forme triangul. de l'oreille.
0,362 rectangulaire,

0,363 ovale,

0,364 ronde.

 

 

 

G,471 écartement sup, de l'oreille.

0,381 insertion verticale.

 

0,372 _ post. — 0,88  — intermédiaire.
0,373 — inf. — 0,888 — ablique.
0,874 — total —

0,375 oreille collée supérieurem.

0,376 cassée à l’antitragus. |

0,391 conque repoussée.

0,392 — traversée,

0,398 -— étroite.

O,3H — large.

0,395 — basse,

0,396 — haute.

 

 

 

 

 
— 19 —

 

 

 

 

 

0,4 Les Lèvres, la Bouche, | 0,41 les lèvres.

le Menton.

0,42 la bouche.
0.43 le menton.

 

0,414 hauteur absolue dé la lèvre su-
“"périeure.

0,1 hauteur naso-labiale petite.

 

: 0,412 — — — petite.
0,442 proéminence des lèvres. O4 — … _interm.
0,443 largeur de la bordure, OAAE — : __ grand
0,414 l'épaisseur des lèvres. ? grance,
0,445 particularités des lèvres. 0,17 — — — grande.
0,4121 lèvre sup. très proéminent.| 0,4131 bordure petite,
O2 —  — proéminente.| 0,4152 — petite,
0,123 — inf. irès proéminente.| 0,4134 — intermédiaire,
O,A124 —. — _ proéminente.| 0,4136 — grande.

OST — grande.

 

0,4141 lèvres Lrès minces.

0,414 — minces.

0,143 — intermédiaires.
O,4l44 — épaisses.

0,4145 — irès épaisses.
0,4146 — lév. sup. retroussée
O,414T — Jlèv.inf pendante,

0,4151 lèvres lippues.

0,4152 lèvres gercées.

0,4153 bec-de-lièvre.

0,4154 sillon médian accentué.

 

0,£21 dimensions de la bouche.
0,422 ouverture de Ta boucha.
0,423 particularités.

0,4211 bouche petite.

0,422 — petite.

O,A218 — (petite),
O,4814 — intermédiaire.
0,425 — (grande).
0,4216 — grande.
0,4217 — grande,

 

 

 

0,4224 bouche pincée.
0,4222 —  Dhée.

 

0,4231 coins abaissés.
0,4282 coins relevés.

0,4233 bouche oblique,
0,4#34 bouche en cœur.
0,425 incisives découvertes.
0,4296 perte des incisives.

 

 

 

 
— 13 —

 

 

0,42814 coin droit abaissé.
0,42312 — gauche abaissé.

6,42932[ coin droit relevé.
0,432? — gauche relevé,

 

 

0,4233L bouche oblique à droite.
0,42332 — — à gauche,

0,42361 perte de la première inci-
sive.

0,42962 perte de la seconde inei-
sive,

0,42363 perte de la troisième inci-
sive.

0,42364 perte de la première et
seconde incisives.

0,42363 perte de Ia première el
troisième incisives,

0,42366 perte de la seconde et
troisième incisives.

 

0,434 inclinaison du menton.

0,4811 inclinaison fuyante.

 

 

 

 

0,432 hauteur du menton. 0,4312 — fuyante.
0,433 largeur du menton. 0,4314 — verticale,
0,454 forme de la saillie inférieure. 0,4316 _ saitlante.
0,435 particularités. 0,4317 _ saillante.
0,4821 hauteur petite, 0,4331 largeur petite,

0,4322 — petite. 0,4332 — petite,

0,4324 — intermédiaire. 0,4334 — intermédiaire.
0,4326 — grande. 0,4336 — grande,
O,4327 — grande. 0,4337 -— grande,
0,4341 menton plat. 0,4351 menton à fosseite.
0,4342 — à houppe. 0,482 — à fossette allongée.
0,4343  — àtrèsfortehouppe.| 0,4853 — hilobé.

 

 

0,4854 sillons sus-méntonniers.

 

 

 

 
— 14 —

 

 

0,544 profil continu.

 

 

 

ment dit.
0,62 contour général de
face.

 

0,52 — brisé.
0,51 contour général du) 0,513 — parallèle.
. profil. 0,514 —  anguleux.
0,52 contour naso-buccal.| 0,515 — arqué..
‘ 0,516 —  ondulé.
0,517 — sémi-lunaire,
0,581 prognathisme. 0,5221 orthognalhisme supérieur.
0,582 orthognathisme. 0,5222 _ inférieur.
0,523 naso-prognathe.
0,524 mâchoire inférieure proémi-
nente.
0,525 prognathisme avec mâchoire
L inférieure proéminente.
0,526 face rentrée en dedans,
0,611 crâne bas.

0,64 profil du crâne propre-

‘0,612 crâne haut.
0,613 tête en bonnet à poil,
0,614 tête en besace,

la ]0,615 occiput plat.

 

0,621 l'ensemble dn contour de la face,

0,620 particularités des parties du con-
tour,

0,628 particularités des chaîrs dela face.

0,616 occiput bombé.
6,617 bourrelet occipital.
0,618 tête en earène,
0,6211 face en pyramide,
0,6212 — en toupie.
0,613 — en losange:
0,6814 — biconcave.
0,6215 — carrée,
0,6216 — ronde.
0,6217 — rectangulaire,
0,6218 — Longue.

0,6219 asymétrie de la face.

 

0,6221 face à pariétaux écartés.
0,622 — _— rapprochés.
0,6283 — à zygomes écartés.
O,G284 — — rapprochés.
0,6225 — à mâchoires écartées.
0,6226 — — rappro-

chées,
0,6827 face à pommeltes saillantés,

 

‘0,688 —

 

0,621 face pleine.
osseuse.
0,6293 flacidité des chairs.

 

 
— 15 —

 

 

 

0,74 les sourcils.
0,72 les paupières.
0,73 giobes oculaires.
0,74 orbites.

9,711 emplacement des sourcils.
0,712 direction des sourcils.
0,713 forme des sourcils.

0,714 dimensions des sourcils.
0,745 particularités.

0,716 "nuance.

 

0,7114 rapprochés.

0,721 sourcils obliques internes.

 

 

 

 

0,25 particularités de la paupière infé- |

rieure.
0,726 particularités des deux paupières.

0,7112 écartés. 0,712  — — externes.
0, 7113 sourcils bas. |
O,T14Æ — “hauts. —-
0,7131 sourcils arqués, 0,711 sourcils courts.
0,7132 — rectilignes. 0,7142 — longs.
0,7183 —  sinueux. | O,TILR — étroits.
0,7434 rapprochement ou écarte- | 0,714 — larges.
ment nerveux des sour-
cils

0,7151 sourcils clairsemés. 0,7161 sourcils blonds clairs.
0,7152 — fournis. 0,162  — — foncés.
C,7158  — réunis. 0,163 — chätains clairs.
0,7184 — à maximum en|0,7164 — — ‘foncés.

queue. 0,7165 — roux.
0,7155 — en brosse. 0,7166  — noirs.
0,7156 — noirsetbarbeblan- | 9,7187 + blancs.

che.
‘0,741 ouverture des paupières. 0,7211 très peu ouvertes,-
0,782 inclinaison de la fente palpébrale. 0,7212 peu ouvertes.
0,723 modèle de la paupière supérieure. | Q 7214 intermédiaires.
0,724 particularités de la paupière supé- 0,7216 ouvertes. :

TISUTE. 0,7217 largement ouvertes... .

 

0,7221 à angle externe relevé.
0,7222 — ——  abaissé.

 

0,5831 paupière supérieure recou-
verte.

0,7282 paupière supérieure décou-
verte.

 

 

 
_— 56 —

 

 

0,7241 débordement entier des
paupières supérieures.

1842 paupières rentrantes,

T243 débordement externe.

44 débordement interne.

45 paupière supérieure droite
tombante.

0,7246 paupière supérieure gau-

che tombante.
0,724T yeux bridés.

3

L

SSeos

72
72

?

D

0,7851 paupière inférieure à bour-
relet.

Oo, 7252 paupière inférieure à poche.

0, 7253 à rides,

0,7284 renver-

sées.

'

 

0,7261 päupièrés chassieuses,
|0,7262 larmoyantes.
0,7263 cils très longs.

0,7264
0,7265
0,7266

courts,
abondants.
rares.

 

 

0,731 saillie du globe.
0,782 particularités du -globe,

0,7311 yeux enfoncés.
0,7912 intermédiaires.
0,7313 saillants.

 

0,7321 strabisme convergent.

0,7322 — divergent.
0,7323 — vertical,
0,7324 intraoculairée peüit.
0,7325 — grand.

0,73214 sirabisme droit converg.
0,73213 gauche
0,73214 double

 

0,732214 strabisme droit divergent.
0,73222 ‘ gauche
0,73228 double

0,788381 iris relevé,
0,73882 iris abaissé.

 

0,741 orbites basses.

O,T48 — Hautes,
0,743 —— encavées.
0,744 — pleines.

 

 

 

 

 

 
 

 

0,81 Le Cou.
0,82 Les Rides.
0,83 La Carrure.
0,84 La Ceinture.
0,85 L'Attitude.

0,86 L'Allure générale.

0,87 La Voix.

0,811 cou court,
0,842 — long.
0,813 — mince.
0,814 — large.

1-6,815-larynx- saillant,

0,816 goitre.

 

0,821 rides frontales.
0,822 — oculaires
0,823 -—— buccales.

0,8211 ride unique totalé. :
O,8212 — — médiane.
0,8213 — double.

0,8244 vides multiples.

 

0,8221 intérsourcilière verticale
médiane,

0,822? intersourciliére verticale

TT doublé.” ° CT

0,828 intersourciliére unilatérale.

0,8224 intersourcilière oblique
unilatérale.

Ô.8225 sillon horizontal de la ra-
cine du nez.

0,8226 circonflexe intersoureilier.

°0,8227 triangle intersourcilier.

0,828 rides temporales.

0,8229 rides tragiennes.

| 0,82231 intersourcilière

unilaté
rale droite.

1.0,82282 intersourcilière . unilaté-

rale gauche.

 

0,82241 intersourcilière oblique

0,82291 ride tragienne unique.

 

 

 

 

 

droite. 0,88802 — double.
0,82242 intersourcilière oblique
gauche.

0,8881 sillon naso-labial accentué.

0,8232 sillon jugal.

0,8233 sillon sus-mentonnier.

0,8234 rides verticales du cou,

0,8235 joues à fossettes,
0,8811 largeur petite.

0,834 largeur des épaules. 0,8812 — petite,

0,832 chute des épaules. 0,8314 . — . moyenne...
0,8316 — grande.
0,817 — grande.

9,8321 épaules horizontales. 0,841 ceinture petite.

0,8822 — intermédiaires. N,812 — petite.

0,8823 — tombantes. 0,843 — (petite).
0,844 -— intermédiaire,
0,845 — (grande).
0,846 — grande.
O,847 — grande.

 

 

 

 
—_ 18 —

| 0,851 le port de la tête eb l'inflexion du
con.

   

7 ‘

0,8511 tête penéhée en avant.

 

 

 

0,852 le degré de rotondité du dos. vo To, 7, Se orreree
0,853 la posture habituelle des bras et] 0,5513 inclinée à droite.
des mains et attitude. 0,8514 — — à gauche.
0,851 des voüté. 0,8531 mains sur les hanches.
0,8522 — droit. . . 0,8532 — dans les poches de
6,8523 épaules saillantes. pantalon.
O,8524 —  elacées. 0,8533 mains dans l'entournure du
gilet,
0,884 croisées sur la poitrine.
0,8535 attitude raide.
0,8536 — nonchalante.
0,8611 démarche très lente. -
0,8612 — très rapide.
| 0,8613 .— . à petits pas.
0,861 la démarche. 0,8614 — à grands pas.
Denn 1e geste. 0,865 — légère.
6,863 le regard. v" ë
0,864 mimique physionomique. 0,8616 — lourde.
O,8G1T — boiteuse,
L 08618 — déhanchée,
CT ‘0, 8619 © —  saulillante.
0,8621 gesticulation abondante. 0,8631 regard droit.
0,8622 absence de gesticulation. 0,863 ‘— oblique.
0,8633 — fixe.
0,8634 — mobile.
0,8633. — franc.
0,86356 —— fuyant.
7 : : "0,8647 7 — lent,
0,8638 — rapide.

 

0,8641 ties de la bouche.
Ô,8642 tics des yeux, _ nue
0. ,8643 clignement des yeux.

 

 

 
 
    

| 0,878. — aiguë...
0,973 — féminine,
0,874 — masculine,

0,871 voix grave,

0,875 zézaiement.
0,876 chuintement.
0,877 bégaiement.
0,878 accent étranger.

 

 

 

nat ere mars,
 

0,9 Yeux. — Cheveux. —

Barbe. — Pigmen-
tation ‘de: ia- peau.

0,94 yeux.

0,92 cheveux.

0,93 harbe

0,94 pigmentation ds dé peau,

 

0,914 auréole.
0,912 périphéria.
0,913 particularités des yeux.

Ô,9111 auréole dentelée.

 

-0,91114 auréole dentelée jaune. - |.

0 ,91112
0,91113

orange.
chätain,.

G,9112 — concentrique.

0,913 — rayonnante,

G,911441 auréole -dentelée jaune
clair.

0,911112 auréole dentelée jaune
moyen.

0,911113 auréole dentelée jaune
foncé.

 

0,812 auréole dentelée orange
clair,

0,911122 auréole dentelée orange
moyen.

0,911123 auréole dentelée orange
foncé.

0,911131 ‘auréole dentelée châtain
clair,

0,914132 auréole dentelée châtain
moyen.

C,911133 auréole denteilée châtain
foncé.

 

0,91121 auréole eoncentr, jaune.
0,91122 orange,
0,91423 châlain,

0,911211 auréole concentr. jaune
clair,

0,911212 auréole concentr. jaune
moyen,

©,911243 auréole concentr. jaune
foncé.

 

0,911221 auréole concentr. orange
clair,
0,911222 auréole concentr. orange
| moyen.
0,911223 auréole concenér. orange
. foncé,

0 ,941231 auréole concentr. châtain |
clair,
0911282 auréole concenir. châtain
moyen,
0,911883 auréole concentr. châtain
….loncé.

 

0,91131 auréole rayonnante jaune.
U,9118R crange,
0,01133 châtain.

0 ,914311 auréole rayonnante jaune
clair,
0,9 14318 auréole rayonnanle jaune
moyen.
0,911313 auréole rayonnante jaune
foncé.

 

0,911321 auréole rayonnante oran-
ge clair.
0,911822 auréole rayonnante oran-
ge Moyen,
0,915323 auréole rayonnante oran-
_ge foncé,

 

0,911834 auréole rayonnante châ-
tain clair.

0,911832 auréole rayonnante chà-
tain moyen.

0,911338 auréole rayonnante chà-
tain foncé,

 

 

 

 

 

 

 
 

0,9121 périphérie azurée,
0,928 périphérie interm, violacée,
0,9123 périphérie ardoisée.

_0,M241 périphérie azur. verdâtre.

0,91221 périph. interméd, verdât.
0,91231 périphérie ardoisée verd,

 

0,912111 périphérie äzurée _jaine
” “verdâtre. |
0,912112 périphéiie azurée orangé
verdâtre, -
û BUS périphérie azurée châtain
- . vérdâtre:

0,918811 périphérie intermédiaire

‘ jaune verdâtre,

0,912212 périphérie intermédiaire
orangé verdâtre.

0 ,912213 périphérie intermédiaire

châtain verdâtre.

 

0, 812311 périphérie ardoisée j juune |
verdâtre,

0 2312 périphér, ardoisée orangé
verdälre,

0,912313 périphérie ardoisée chä-

ain verditre.

 

 

| 0,31 œil truilé.
0,9132 yeux à secteurs.
i1,9133 yeux vairons.

. 0,9134 yeux à cercles nacrés ou:sénile ou

Particularité des yeux.

tare sur l'œil,
0,9135 yeux à zoneconcentriquegrisätre.
0,9136 yeux à pupille dilatée.
0,9437 pupille pisiforme ou excentrique.
0,9138 amputé d’un œil.
0,9139 porteur d'un œil de verre.

 

Ô,911 yeux impigmentés.
0,912 pigmentation jaune.

 

 

Classement simplifié o 13 7 a
des yeux. 0,915 _— marron en cercle.
0,916 — marron verdâtre.
0,927 — marron pur.
7 0,911 chereux droits.
0,921 nature des cheveux. 0! 8212 — - ondés.
0,922 insertion des cheveux. 0,913 — bouclés. :
0,923 l'abondante des cheveux. 0 9244 — frisés.
0,924 nuance des cheveux. 0,9215 —  crépus.
0,9216 —  laineux.
0,922 insertion cireulaire. 0,9231 cheveux abondants.
0,9223 —_— rectangulaire. ne it premns.
9: : 233 calvitie frontale.
D8RRS en pointe. 09234 — . tonsurale.
0,9235 —  pariétale.
0,9226 —  hlopécie.

 

 

 

 

 

 
 

 

 

0,9241 cheveux blonds.
0,9248 :
0,9283 .

09244

0,925

| 0,9246

'O,RAT

er

châtains.

“noirs.

TOUX,

- blonds albinos.

grisonnants.

blancs,

092411 irès blonds.
0,9418 blonds.
0,92413 blond foncé,

 

0.924291 :châtain clair.
0,92422 ehâtains.
0,92488 châtain foncé.

0,82441 roux vif.
0,92442 roux,
0,92443 roux châtain clair.

 

0,934 nature des poils de la barbe et

abondance.

. 0,932 forme de La barbe.
0,933 nuance des poils.

0,9311 poils droits,
0,9342 — ondés.
0,9343 — bouclés.
0,9314 — frisés.
0,9315 — très frisés.
0,9316 abondants.
0,9817 clairsemés.

 

0,9321 la moustache.

0,9322 les favoris.

0,932 ‘la mouche,

0,9324 barbe de bouc.

0,9335 le collier,

0,9326 la barbe entiére.

0,9327 face glabre.

10,9328 face rasée.

0,9329 joues glabres et moustaches

fortes,

0,9331 poils blonds.

0,933? — châtains.

0,9333 — noirs.

0,9334 — roux,

0,9335 — blonds albinos.
0,9336 —  grisonnants.
0,9337 — blancs.

 

0,844 coloration pigmentaire.

| 0,942

sanguine.

0,9412 coloration pigmentaire.
petite, _-

0,914 coloration pigmentaire
moyenne, |

0,9416 coloration pigmentaire :
grande.

 

 

Ô,9492 coloration sanguine pelite.

0,9424

—  moyen-

0,9486 coloration sanguine grande.

 

 

 

 
— LE —

Comme on à pu s'en convaincre par l'étude du répertoire qui
précède, celui-ci n'offre aucune difficulté d' application pratique,
Les plus grands chiffres ont six « décimales et ils se rapportent
AUX yeux. :
: On pourrait simplifier le code des yeux en ne prenant en consi-
dération que les sept classes du tableau de Bertillon. Nous
aurions alors le répertoire suivant :

6,911 yeux À à iris impigmenté.

O,9J2 — : pigmenté dé jaune.
. CUS —  —. — d'orange. -
nee (AA rene en ee de-châtaine eee
0,95 — — — de marron groupé en cerele,
0,916 .— . — — de marron rayé de verdâtre.
917 — — : — . de marron pur.

Cette classification suffit certainement pour le signalement
par télégraphe et, ainsi, le code télégraphique pour les yeux
devient excessivement simple.

" Lé’sigñalement chiffré à l'aide dü portrait parlé sera précédé
en première ligne par l'âge apparent de l'individu ; vient ensuite
sa taille. Ces deux chiffres ne se confondront pas avec les chiffres
suivants. En effet, l'âge de l'individu signalé ne dépassera jamais
un nombre à deux chiffres; on sait donc que les deux premiers
chiffres se rapportent à l'âge apparent du signalé. La taille ne
dépassera pas trois chiffres. Les trois chiffres suivants sont donc
réservés à la faille. Le signalement lui-même est télégraphié en
‘suivant rigoureusement l’ordre de la fiche du portrait parlé. Les
zéros appartenant à chaque chiffre. sont maintenus pour indiquer
la séparation des signes caractéristiques et empêchent ainsi la
confusion des différents nombres, Les virgules sont supprimées.
Chaque groupe de cinq chiffres compte au télégraphe pour un mot.

Exemple : le signalement de l’auteur de ce code se présentera
chiffré de là façon suivante, en admettant qu'il soit télégraphié
de la sûreté.de. Lausanne à cellé de Paris :..…

Service sûreté Paris. D.
ï 80 175 01512 0224 0284 0255 02732 03116 03238 08243 03325 03415
03522 04124 Ghii4a OAÂAT (5291 06214 06218 07151 O7IGL (914112 09122141
092443
ant LIL et LU Las an 1 -… Sirelé Lausanne.
Total : 29 mots (5 chiffres comptant pour 1 mot}. :
. Le même télégramme maintenant, en mots style iélégraphique,
se présente comme suit : _ :

“Service sûreté Paris.

‘ Age apparent 30 Taille 475 sinus dos nez rectiligne base horizontale
saillie grande limite particularité cloison non apparente oreille bordure
originelle grande lobe traversé limite et dimension petite limite antitragus
saillant limite pli inférieur vexe limile forme rectiligne Hmite particularité
sillons séparés orthognate supérieur face longue biconcave sourcils clair-
semés blonds clairs lèvre inférieure proéminente épaisseur grand inférieure
pendante cou long auréole jaune moyen périphérie intermédiaire verdätre
moyen cheveux blonds foncés,

| Süreté Lausanne.

Total : 75 mots.

Le coût du télégramme contenant le signalement chiffré est de
3 fr. 40 (taxe fixe : 50 centimes plus 10 centimes par 5 chiffres);
celui du télégramme en mots est de 8 francs.

Le télégramme chiffré est donc de 4 Îr. 60 meilleur marché
‘ que le télégramme ordinaire. En appliquant le code décimal que
nous venons de proposer, les différentes directions de police feront
ainsi de sérieuses économies tout en envoyant des signalements
très précis. Si l'on prend encore en considération que ce code est
absolument international, puisque les chiffres sont dans tous Îes
pays les mêmes et qu'on peut facilement traduire en toutes
langues le code français, de sorte que les mêmes chiffres signifient
partout les mêmes caractères, on se rendra compte que, par
l'acceptation du code, un sérieux pas en avant sera fait dans
l'internationalisation de la police.

Cette internationalisation, comme il a déjà été dit à maintes
reprises par des personnes autorisées, devient de plus en plus
nécessaire, En eflet, les bandes internationales : escrocs, voleurs
d'hôtels, faux joueurs, anarchistes, etc..., deviennent, avec les
facilités de transports, de plus en plus nombreuses et il est sou-
vent fort difficile, sinon impossible, pour la police d'un pays, de
les surveiller, si elle n’est pas secondée dans cette tâche par des
renseignements caps et précis provenant de la police du pays
= ph

tueux. Nous espérons que notre code télégraphique du portrait
parlé facilitera la surveillance internationale des malfaiteurs,
est évident que, pour pouvoir l'employer, il faut connaître le
portrait parlé de Bertillon, mais nous estimons que cette con-
naissance est aujourd'hui indispensable aux policiers et que les
directions de police n'ayant pas encore introduit le portrait parlé
dans leur service, suivront sous peu l'exemple de la France, de
la Suisse, de la Roumanie, etc., pays qui ont adopté depuis long-
temps cette seule méthode d'un signalement exact.

(Extrait des Archives d’Anthropologie criminelle
| N° 158. — 15 Février 1907.)

° Lyon. — Imp. A. REv, 4, rue Gentil. — 44686
