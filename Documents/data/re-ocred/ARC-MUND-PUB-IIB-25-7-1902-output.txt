EEE

/

Institut International de Bibliographie
. PUBLICATION N° 35 INDICE BIBLIOGRAPHIQUE |025.4]

 

 

CLASSIFICATION BIBLIOGRAPHIQUE

DÉCIMALE

TABLES GÉNÉRALES REFONDUES

établies en vue de la publication du

Répertoire Bibliographique Universel

Ses
es

ÉDITION FRANÇAISE
publiée avec le concours du
BUREAU BIBLIOGRAPHIQUE DE PARIS
et du
TOURING CLUB DE FRANCE

FASCLeU EE N°7
Tables de la division [70]

ee —

(TOURISME, CYCLISME, AUTOMOBILISIME)

 

 

 

 

 

INSTITUT INTERNATIONAL DE BIBLIOGRAPHIE

BRUXELLES, 1, rue du Musée, PARIS, 44, rue de Rennes
ZURICH, 39, Eidmattstrasse,

1902

 

 
 
 

 

 

 

SPORTS 791.6

 

79 Sports. Jeux et amusements.

791 Divertissements publics, Fêtes et spectacles forains,

792 Représentations théâtrales.

793 Jeux d'intérieur.

794 Jeux de calcul et de précision,

795 Jeux de combinaison et de hasard.

796 Jeuxet sports de plein air. Sports athlétiques. Tourisme
Alpinisme, Cyclisme, Automobilisme.

797 Navigation de plaisance et aérostation:

798 Equitation. Courses de chevaux. Attelages,

799 Pêche, Chasse. Tir à la cible,

F Les tables des subdivisions communes de forme, de lieu, de langue, de temps, etc.,
(tables I, IT, III, IV et V) sont applicables dans toute l'étendue de la division 79.
Lorsque des divisions spéciales n’auront pas été expressément prévues pour spéci-
fier lés particularités relatives à la pratique des différents sports et exercices ou les
divers points de vue sous lesquels le sujet peut être traité, on pourra faire usage
| aussi, dans toute l'étendue de cette division 70, des subdivisions analytiques dont le
| tableau est donné ci-après 79.0 et qui sont caractérisées par le zéro sans parenthèse,
| Les sports et amusements spéciaux à des pays déterminés, comme aussi les excur-
| sions faites en diflérentes régions, pourront être localisés par l'emploi de l'indice
géographique des lieux considérés, c’est-à-dire l'indice placé entre parenthèses, Ex.:
706.64.09 (404) Excursion en bicyclette en Suisse.
see « r=eù
79.0 Subdivisions analytiques spéciales. Con-
ditions accessoires applicables aux sports ou
f exercices.
.OI Hygiène spéciale.
.02 Nourtiture et aliments appropriés.
| .03 Vêtements spéciaux.
| .04. Equipement et outillage.
| .05 Abris.
| Er ree
| .06 Organisation générale.
| 07 Apprentissage. Instruction spéciale.
| .09 Excursions.
791 Divertissements publics. Fêtes et spectacles
| forains.
: Pour les luttes et jeux athlétiques, voir 796.8.
N TL Représentations foraines. Saltimbanques, Danseurs
de corde, Jongleurs, Bateleurs.
À .2 Concerts publics. Concours de musique et de poésie,
| Jeux floraux.
ss) Cirques. Hippodromes, Représentations équestres,
Jeux de cirque.
Pour les exercices équestres des gymnasiarques, voir 706.48.
4 Panoramas. Lanternes magiques.
5 Musées de cire. Marionnettes, Ombres chinoises.
.6 Fêtes publiques. Cortèges historiques, Défilés, Caval-
cades, [lluminations, Feux d'artifices.
|
"À

 

 

 

 

 

 
 

 

 

191.7

SEORTS

 

791.7

793

,

Jeux publics. Joutes, Mâts de Cocagne, Balançoires,
Bascules, Jeux de bagues, Montagnes russes.

Ménageries. Combats d'animaux entre eux et combats
contre les animaux, Sports d'animaux, Combats de
coqs, Combats de chats et rats, Ours lutteurs, Com-.
bats de taureaux, Courses de chiens.

Pour les exercices équestres, voir 706.48 ; pour les courses de chevaux,
Voir 708.4.

Autres divertissements.

Représentations théâtrales.

Pour les théâtres de société, voir 793, Pour la littérature théâtrale, voir sous 8
les divisions propres à la littérature de chaque langue.

Tragédies et drames.
Comédies et vaudevilles.
Farces et pantomines.
Opéras et drames lyriques.
Opéras comiques.
Opérettes et bouffonneries musicales.
Cafés-Concerts et Music-halls.
Féeries et ballets. Chorégraphie.
Pour les danses de salon, voir 703.3.

Autres pièces.

Jeux d'intérieur. Salons ou pelouses.

Théâtre de société.
Voir aussi 702 Représentations théâtrales,

Jeux préparés. Tableaux vivants. Charades. Proverbes
en action.

Danses de salon.

Pour la chorégraphie et les ballets, voir 792.8 ; pour les danses enfantines,
voir 796.13.

Danses populaires et danses nationales.

Danses de caractères. Danses françaises anciennes.
Pavane, Menuet, Gavotte.

Danses de salon pour deux personnes.
Polka, Valse, Mazurka, Redowa, Scottisch, Pas
de deux, etc. “ie

Danses de salon pour plus de deux personnes sans
accessoires.
Quadrilles, Pas de quatre, Lancier, etc.

 

 

 
 

 

y

ÿ

 

 

SRORAES 705.43

 

793.35

9
794

DO © Oo in $ &w b H

I
©
on

H

.42

.43

Danses avec accessoires.
Cotillon, etc.
Jeux d'action.
Colin-maillard, Main chaude, Furet.
Jeux de gages et d’attrappe. Jeux innocents.
Jeux de pénitence.
Jeux d'esprit et de mémoire, Rebus, Charades,
Devinettes.
Récréations scientifiques. Physique amusante. Esca-
motage.
Autres jeux d'intérieur.

Jeux de calcul et de précision avec appa-

reils spéciaux.
Pour les jeux de cartes, voir 795.4.
Jeux d'échecs. Jeu de Palamède.
Jeux de dames. Gobang, Agon.
Jeux de trictrac. Jacquet, Revertier.
Jeux de marelle.
Jeux-de solitaire.
Jeux de dominos. Matador
Jeux de billard.
Jeu de bagatelle.
Autres jeux de calcul et de précision.

Jeux de combinaison et de hasard.

Jeux de dés et d’osseléets. Jeu d’oie.

Jéux avec roues. Toton, Roulettes, Steeple:chase
ou petits chevaux.

Jeux à tirage de numéros. Loto, Biribi.

Jeux de cartes.

Jeux de cartes de combinaison. Piquet, Impériale,
Écarté, Triomphe, Mistigri, Whist, Hombre,
Reversi, Cribbage, Boston, Manille.

Jeux de cartes de hasard. Brelan, Trente et un,
Rams, Trente et Quarante, Nain Jaune, Lans-
quenet, Baccara. :

Jeux de cartes de position et d'adresse. Patiences,

Tours de cartes.
Pour la Cartomancie, voir 133.3,

 

M.

 

 

 
 

EE

 

 

 

796 SPORTS
796 Jeux et Sports de plein air et sports athlé-
tiques. Gymnastique. Tourisme. Escrime.

1 Jeux d'Enfants. Récréations de plein air.

SEL Petits jeux paisibles et de la première enfance.

2 Récréations enfantines sur le sable et la plage.

419 Rondes et danses enfantines.

.14 Jeux d’agilité. Cheval fondu, Saute-mouton, Cache-
cache. Quatre-coins, Barres.

19 Cerf-volant.

2 Jeux d’action et d'adresse avec instruments.

Pour les Jeux de balles et ballon, voir 796.8,

21 Corde à sauter, Cerceau, Escarpolette.

22 Toupies.

29 Billes.

.24 Palets, Bouchon, Tonneau, Bâtonnets.

29 Bilboquet, Diable.

.26 Volant.

27 Crocket.

.28 Boules, Cochonnet, Quilles.

.29 Autres jeux d'action.

2) Jeux de balle et ballon. Jeux de jet.

ST Balle lancée à la main sans instrument. Balle au
mur, Balle au camp, Balle indienne, Balle empoi-
sonnée, Balle cavalière.

32 Ballon lancé à la main.

33 Ballon lancé au pied. Foot-ball.

.34 Jeux de balle avec tamis, tambourin ou raquettes.
Paume, Longue paume, Lawn Tennis.

.35 Jeux de balle avec crosse ou maïllet. Base Ball, Jeu
de Mail, Cricket, Gulf, Polo.

+97 Jeux avec objets lancés. Choule, etc.

.38 Jeux de jet. Javelot, Sagaie, Boomerang.

4 Gymnastique et exercices corporels.

Pour les sports athlétiques et l'escrime, voir 798; pour les combats d’ani-
maux, voir 701.8; pour le patinage et la natation, voir 706.0; pour la
gymnastique scolaire, voir 371.73.

AI Exercices d’assouplissement et de force. Appareils de
chambre, Haltères.

.42 Marches, Courses, Echasses.

.43 Sauts et voltiges. Perches, Tremplin, Cheval de bois.

44 Barres borizontales, Barres parallèles.

.45 Portique.

 

 

 

 
2

 

 

SPORTS 706.71

 

.521

1902

5529

Trapèze et anneaux.
Acrobatie, Dislocation, Gymnasiarques.
Exercices équestres.
Pour l'équitation, voir 798.2
Exercices avec animaux. Clowneries, Exercices divers.

Tourisme. Alpinisme. Marches, Pédestrianisme,

Excursions à pied en plaine et en montagne.
On fera usage des divisions géographiques pour spécifier les localités et
des divisions analytiques de 79, qui sont rappelées ci-dessous.

Pédestrianisme. Tourisme. Marches. Excursions en

plaine.
Alpinisme, Excursions en montagne.

DIVISIONS ANALYTIQUES
-07 Hygiène spéciale.
.02 Nourriture, Boisson, Kola.
.03 Vêtements, Chaussures, Voiles, Lunettes.
.04 Outillage, Piolet, Corde, Crampon.
.05 Abris, Cabines, Refuges.

Guides, Généralités, Règlements et tarifs.
Pour les assurances des guides, voir Assurances sur la vie 368.6.

Dangers et accidents. Précautions et règles à observer.
Pour les accidents qui ne sont pas spéciaux à l’alpinisme, voir
Eboulements 551.43, Avalanches 55r.3r, Débordements 55r,57.

Pour les maladies et indispositions, voir à la Pathologie et à la
Physiologie : Mal de montagne 612.275.1 ; Troubles du cœur

g 616.12 ; Hémorragie 616.13 ; Vertiges 616.847.

Appels et signaux.

Cyclisme. Appareils de locomotion mus par l’homme.

Au point de vue sportifseulement ; pour la théorie des cycles, voir 537 : 796.6;
pour la construction, voir 629 119; pour les appareils ayec moteurs, voir
\

796.7.

Premiers appareils de vélocipédie, Célérifères, Drai-
siennes, Hobby-horse.

Monocycles.

Bicycles.

Bicyclettes.

Tandems.

Triplettes, Quadruplettes.

Appareils d'entraînement.

Autres appareils de locomotion.

Automobilisme. Appareils de locomotionavecmoteurs.

Au point de vue sportif seulement ; pour la construction des véhicules et
l’industrie de la locomotion, voir 629.11 ; pour la construction et la théorie
des moteurs, voir 621 et 536,8. À subdiviser par — comme 629.1t3.

Appareils à deux roues avec moteurs. Bicyclettes à

pétrole.

 

 

 

 
 

 

 

796.72

SPORTS

 

796.72
73
+74
7

+77

.81
.82
.83
.84.
.85
.86

.863
.865

.89

.OI
013
.915
.916

.951
.992

797

797.1

«12

.I4
.15

17

92
53

.54

Tricycles à pétrole.
Quadricycles et voiturettes à pétrole.
Tricycles, quadricycles et voiturettes Fe
Voiturettes automobiles légères pour deux ou de
personnes.
Voitures de courses.
Sports athlétiques. Sports de combat. Boxe etescrime.
Pour les combats d'animaux, voir 707.8.
Luttes à main plate
Luttes olympiques.
Boxe.
Savate, Chausson.
” Canne, Bâton.
Escrime.
Pour les règles du duel, voir 394.8, et pour la législation du duel, 343.673,
Fleuret et épée.
Sabre. [
Autres spoits athlétiques.

Exercices sur la glace, la neige et dans l'eau. Pati-

nage. Natation.

Exercices sur la glace et la neige.
Patinage sur la glace.
Patinage sur la neige. Ski.
Traîneaux.

Exercices dans l’eau en général.
Natation sans appareils.
Natation avec appareils. Nas

Navigation de plaisance et aérostation.

Pour les jeux de balle et de ballon précédemment classés sous ce même numéro,
“ voir 796.3.

Navigation de plaisance.

Au point de vuesportif seulement ; pour la navigation commerciale, voir 656.2;
pour la théorie, voir 527 ; pour la construction des navires, voir 600. :

Canotage en rivière à rame et à voile.
Navigation cotière à voile. ne Régates.
Navigation à vapeur.
Vélocipèdes aquatiques. nie et autres appa-
reils de navigation.
Aérostation.

Au point de vue sportif seulement ; pour la construction des appareils, voir
629.13.

Ballons libres.
. Ballons captifs.
Ballons dirigeables.

 
 

 

SPORTS ; 790.32

 

707.55
.56

.58
798

*22

29
.24.

re)

.62

.64
.66

799

‘I

LL
12
13

.I4
is

.16

21
.22
23

.24
29

Appareils plus lourds que l'air.
Appareils d'aviation.
Aéroplanes.

Cerfs-volants.

Equitation. Courses de chevaux. Attelages. É

Au point de vue sportif seulement ; pour l'élevage des chevaux en général, voir
636.1.

Equitation.
Voir aussi 706.48 Exercices équestres.
Dressage du cheval de selle. Harnachement.
Voir aussi 636.11.044 Dressage du cheval en général.

Equitation.
Manèges. Carrouséls.

Courses de chevaux. Champs de courses. Hippo-
dromes. Chevaux de courses. Elevage. Entraînement.
Couises d'obstacles. Steeple-chases.

Attelages.

Dressage du chéval d'attelage.
Attelage et conduite des voitures. Coatching.
Courses d’attelages. Concours hippiques.

Pêche. Chasse. Tir à la cible.

Au point de vue sportif seulement ; pour l’industrie de la chasse et l'exploitation
du gibier, voir 630.1; pour l'industrie de la pêche, voir 639.2 à 630.8.

Pêche en général.
Pêche en eau douce.
Pêche à la ligne.
Pêche au filet en rivière.
Pêches diverses.
Pêche côtière. Pêche aux crevettes, aux crabes.
Pêche en mer.
Chasse en général.
Chasse à tir.
Chasse au fusil en plaine et en battue.
Chasse au chien d'arrêt.
Chasse au marais.
Chasse à courre. Chasse au faucon. Chasse aux flam-
beaux.

Chasse aux animaux féroces ou sauvages.
Pour la chasse aux animaux à fourrures, voir 630.7.

‘te FE
Tir à la cible en général. Tir des armes à feu. Pistolet,
Fusil. Carabine.
Tirs à l'arc, à l’arbalète à la sarbacanne, etc.

 
 

 

 

 

 

 
 

 
 
