 

CONGRES MONDIAL

DES

ASSOCIATIONS INTERNATIONALES |
Deuxième session : GAND-BRUXELLES, 15-18 juin 1913
Organisé par l'Union des Associations Internationales

Office central : Bruxelles, 3bis, rue de la Régence at ace
5° Section

 

 

 

Actes du Congrès. — Documents préliminaires.

 

 

Comment devrait être organisée une
Université Internationale

Communication faite par l’Université Nouvelle de Bruxelles

[378 (w)]

A.— QUANT AUX PROFESSEURS :

Étant donné qu'une Université Internationale doit faire appel
à des professeurs venant de toutes les facultés et universités, et
que le choix qui doit en être fait, en vue d’un enseignement
déterminé et variable chaque année suivant l’état des connais-
sances humaines et l'intérêt que certaines matières présentent
d’une façon toute spéciale à chaque époque et dans chaque
branche de connaissances, il nous apparaît que le personnel
enseignant doit comprendre : 19 des professeurs appelés pour un
temps déterminé, d'une façon irrégulière, suivant les besoins de
l’enseignement, pour donner un cours ou une série de conférences
sur un sujet choisi par l'Université ; 20 un personnel fixe chargé
d'élaborer pour chaque année ou chaque semestre, le plan des
cours et conférences le plus capable d’intéresser un public
international, de coordonner ces cours et conférences, d’en fixer
le programme, d’en tracer les limites et de recruter les professeurs
les plus qualifiés pour apporter, sur ces données, un enseigne-

 
—2—

ment original, personnel dans certains cas, et le plus d'actualité
possible.

Ce personnel serait composé d’un certain nombre de Direc-
teurs d'études, d'une compétence et d'une autorité suffisantes
pour remplir ces délicates fonctions, et pour diriger en même
temps les recherches ct les travaux des étudiants de l'Université,
ou au besoin faire des cours ou des conférences complémentaires
ou préparatoires des cours et conférences faits spécialement par
les professeurs étrangers.

Ces directeurs d’études auraient à leur tête un Directeur général
chargé de maintenir la coordination des différentes directions, de
veiller à la meilleure répartition des cours et conférences, et de
surveiller l'administration de l’Université Internationale.

B.— QUANT AUX ÉTUDIANTS :

Le but d'une Université Internationale étant de s'adresser à
une élite intellectuelle désireuse de rencontrer, groupées en un
même centre, les personnalités de divers pays les plus compé-
tentes et les plus remarquables dans chaque branche de connais-
sances, on ne devrait admettre que des étudiants d'université
ayant obtenu leurs grades ou en cours d’études pour les obtenir
et inscrits dans une université quelconque.

C.—-QUANT AUX BRANCHES :

L'enseignement nous semble ne devoir comporter ni Facultés
isolées, ni Facultés groupées en université, sur le modèle des
universités actuelles ordinaires. Ce doit être un centre de hautes
études, où l’enseignement soit le plus souple possible, de façon à
s'adapter aux besoins de chaque époque. Il ne s’agit pas de faire
une université pour distribuer des diplômes à la suite d'examens,
mais un institut destiné à l’enseignement le plus élevé, le plus
personnel, le plus actuel possible, et fait par les personnalités les
plus éminentes de chaque pays dans chaque ordre de connais-
sances.

Pas de cours universitaires fixes, mais des cours et des confé-
rences sous forme de cycles, sur tous les sujet les plus intéressants
pour l'époque, en les complétant au besoin par des cours et confé-

 

 

— 3 —

rences faits par les directeurs d'études pour préparer les étudiants
à profiter dans la plus grande mesure possible des cours et confé-
rences faits par les personnalités appelées de l'étranger à l’Uni-
versité. |

D. — QUANT A LA DURÉE DES ÉTUDES :

Il va de soi qu'avec un enscignement ainsi compris, éminem-
ment variable et changeant, ct n'ayant pas pour but la prépa-
ration à des examens ou des concours quelconques, mais simple-
ment consacré par des certificats de présence délivrés aux étu-
diants par les directeurs d'études, à n'y a aucune durée d'études
à prévoir.

E. — QUANT AUX MÉTHODES :

Il résulte encore de la conception même d’une Université
internationale comme un centre de hautes études de perfection-
nement pour des jeunes gens déjà pourvus de grades universi-
taires, que les cours ne doivent pas avoir le caractère de ceux
des universités ordinaires, dont le but cst tout autre, ct que,
sans se placer systématiquement au point de vue comparatif ou
international, ils doivent surtout avoir un caractère général et
permettre de faire connaître au public scientifique intemational
les idées et les hommes les plus représentatifs à chaque époque
d'une branche donnée de connaissances.

F. — QUANT AUX LANGUES :

Il paraît bien difficile de permettre que chaque professeur
s'exprime dans sa langue nationale. Ce serait aller contre le but
même d’une Université Internationale, qui est de faire connaître
par le plus grand nombre possible d'hommes de science les idées
les .plus importantes, les plus intéressantes, dans tous les ordres
de connaissances humaines et cela par les hommes qui ont lancé
ou développé le plus ces idées. Si chaque professeur parlait dans
sa langue, il en résulterait que certains d’entre eux ne seraient
compris que par les étudiants de même nationalité qu'eux,
lesquels n’ont pas besoin de sortir de leur pays pour les entendre,
et que leur enseignement serait inutile pour tous les autres

 

 
— 4 TT

n'entendant pas leur langue. L'idéal serait évidemment que
l'enscignement fût fait dans unc seule langue, qui serait celle du
pays où siègerait l'Université internationale. Mais ce désir est
difficile à réaliser. Toutefois, les hommes cultivés connaissent en
général deux ou trois des langues suivantes : français, anglais ou
allemand. On pourrait donc admettre, en principe, que l'ensei-
gnement serait facultatif dans une de ces trois langues seulement.
Le temps et les circonstances se chargeraient de régler cette
question d’une façon plus stricte pour le plus grand avantage du
public international scientifique, dont les desiderata sauraient
bien se faire obéir.

G. — QUANT AU SIÈGE DE CETTE INSTITUTION :

Il semble naturel qu’elle soit installée dans un pays neutre
au point de vue politique, d’une part, qui ne soit pas déjà pourvu
de grandes universités et établissements d'enseignement supé-
rieur, d'autre part, et enfin, qui soit d'un accès aussi facile et
rapide que possible aux étudiants de tous les pays du monde.

La Belgique, par sa situation privilégiée à ces divers points
de vue, paraît tout indiquée pour être le siège d’une pareille

Université, qui y est du reste actuellement représentée par
l'Université Nouvelle, École internationale d'enseignement
supérieur, ct qui répond entièrement à la conception proposée
aujourd'hui par le Congrès Mondial des Associations Interna-
tionales.

ANNEXE

L'Université Nouvelle de Bruxelles.
Ecole libre et interaationale d'enseignement supérieur.

L'Université Nouvelle de Bruxelles, — Lcole libre internationale
d'Enseignement supérieur, — a été fondée à Bruxelles, en 1894, époque
depuis laquelle elle fonctionne sans interruption. — Ses locaux se trouvent
67, rue de la Concorde, à Bruxelles. — Elle comprend en ce moment : —
une Faculté des sciences sociales, économiques et financières; — une
Faculté de droit : — un Enstitut géographique ; — un Institut des Hautes
Etudes. . .

L'Institut des Hautes Etudes est l'organisme le plus original par lequel

se signale l’activité de l'Université Nouvelle, C'est par là surtout que sc
manifeste son caractère international.

Qu'il nous suffise, pour le montrer, de dire que de 1902 à 1913, c'est-
à-dire pendant la dernière décade d'enseignement, 263 professeurs résidant
à l'étranger y sont venus donner 916 conférences et que le nombre de ces
professeurs, qui n'était les premières années que d'une quinzaine, a
atteint dans les dernières, la trentaine et a même été, cette année, de 44.

Quant aux cours universitaires faits par le corps professoral belge, ils
ont comporté en 1911-1012, 1,500 deçons, et en 1912-1913, 1,570 leçons.

Ces chiffres attestent mieux que tout ce que l’on pourrait dire, l'activité
de plus en plus intense au point de vue de l'enscignement international
de l’Institut des Hautes Études de l'Université Nouvelle, et l'estime
croissant dont il jouit à l'étranger.

Il ne lui manque actuellement que des ressources matérielles suffisantes
pour lui donner l'extension que comporte une université internationale,
telle que la propose le Congrès, et pour réaliser pleinement cette haute
conception qu'elle a été, elle en est fière, la première à avoir eue, il y à
près de vingt ans, et à avoir poursuivie depuis lors dans sa réalisation.

Ii ne nous est pas possible, ici, de rappeler les conférences et séries de
conférences qui ont été organisées depuis le début de l'œuvre. Nous nous
bornerons à citer, parmi les personnalités étrangères les plus marquantes
qui ont donné leur concours à l'Université Nouvelle depuis 1894 :

MM. Élisée Reclus; — lie Recklus ; — de Roberty; — Enrico Ferri,
professeur à l'Université de Rome; — Maxime Kovalevski, membre du
Conseil d'Empire ; — Pierre Quillard ; — Max Nordau ; — Novicow ; —
Bernard Lazare; — Enriquès, professeur à l'Université de Bologne ; —
Albert Métin, agrégé à l'Université de Paris, député à la Chambre fran-
çaise ; — Niceforo, professeur aux Universités de Nagles et de Lausanne:
—Médérié Dufour, professeur à l'Université de Lille : — Hubert Lagardelle
directeur du Mouvement socialiste; — Loria, professeur à l'Université
de Turin; — Issaïcff, professeur à l'Université de SaintPétersbourg ; —
Edgard Milhaud, professeur à l'Université de Genève; — Albert Thomas,
député à La Chambre française ; — Ch. Seignobos, professeur à l'Univer-
sité de Paris ; — Sylvain Lévi, professeur au Collège de France ; — Paul
Sabatier ; — Langevin, membre du Coliège de France ; — Perrin, profes-
seur à l'Umversité de Paris; — Scipio Sighele; — René Worms; —
D. Parodi; — Bernstein, ancien député au Reichstag; — Pierre Mille;
— Kohler; — Te Dantec:; — Fontaine, directeur général du travail à
Paris; — le général Bazaine-Hayter; — Dr Auguste Forel; — Charles
Gide: Louis Havet, membre de l'Institut de France: — Buisson,
député à la Chambre française : — Dr Soilier ; -- E. Van dc Velde, archi-
tecte, à Weimar; — Ch. Diehl, membre de l'Institut de France; —
E. Bertaux, professeur à l’Université de Paris; — Salomon KReinach,

 
 

_ 6 —

membre de l’Institut de France : — Henri Marcel, directeur du Musée du
Louvre; — G. Milhaud, professeur à l'Université de Paris ; — Lalande,
professeur à l'Université de Paris; — Andler, professeur à l'Université
de Paris; — Luchaire, professeur à l'Université de Grenoble, directeur
de FInstitut français de Florence; — Augagneur, ancien ministre; —
Professeur Rathgen, directeur de l’Institut Colonial de Hambourg; —
Francis de Pressensé; — Sembat, député à la Chambre française; —
Charles Dumont, ministre; — Pierre Paris, professeur à l'Université de
Bordeaux ; — Enlart, directeur du Musée du Trocadéro ; — I. Bénédite,
conservateur du Musée du Luxembourg ; — Jessen, directeur au Musée
des Arts décoratifs, à Berlin; — Berlage, architecte, à Amsterdam; —
Réau, directeur de l'École française de Saint-Pétersbourg ;: — Mabe! Bode,
professeur à l'Université de Londres ; — Foucher, professeur à l'Université
de Paris ; — d’'Ardenne de Tizac, conservateur du Musée Czernuschi.

Les conférences de l’année universitaire qui vient de finir (1912-1913),
ont été consacrées, notamment : — aux Sciences naturelles (les méthodes
d'investigation en mathématique, astronomie, physique, chimie, bio-
logie, etc.) ; — à la Psychologie ; — à des Questions sociales et politiques
(l'évolution capitaliste et la démocratie en France) ; — et des questions
d'Art (l'art espagnol; — l'art décoratif contemporain; — les arts en
Extrême-Orient ; — le Bouddhisme dans la littérature et dans l'art); —

à la Pédotechnie {la culture de l'enfant au point de vue psycholo-
gique) etc., etc.

 
