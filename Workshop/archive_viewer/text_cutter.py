import json
import codecs
import os
import re

invalid_chars = [ '\t', '\r', '\n' ]

# removal of leading spaces, triple & double spaces
def clean_spaces( txt ):
	txt = txt.strip()
	txt = txt.replace( '   ',' ')
	txt = txt.replace( '  ', ' ')
	return txt

# enable removal of spaces between consecutive single letters
# might break the text, be carefull with it
def glue_characters(txt):
	m = re.findall( '\s(\w(?:\s\w){2,})\s', txt )
	out = txt
	for sb in m:
		needle = sb
		repl = ' {} '.format(re.sub(r'\s', '', sb))
		out = out.replace( needle, repl, 1 )
	return out

def cleanup_line( line ):
	line = glue_characters (clean_spaces( line ) )
	return line

def cut( json_path, f_target, cleanup = True ):
	
	info_js = open( json_path, 'r' )
	data = json.load(info_js)
	info_js.close()
	
	src = codecs.open( data['txt'], 'r', encoding='utf-8' )
	lines = []
	for l in src:
		lines.append( l )
	src.close()
	
	dst_path = os.path.join( f_target, data['name'] + '.txt' )	
	dst = codecs.open( dst_path, 'w', encoding='utf-8' )
	
	for r in data['blobs']:
		for y in range( r[1], r[1] + r[3] + 1 ):
			if y >= len( lines ):
				break
			line = ''
			for x in range( r[0], r[0] + r[2] + 1 ):
				if x >= len( lines[y] ):
					break
				c = lines[y][x]
				if c in invalid_chars:
					continue
				line += c
			if cleanup:
				line = cleanup_line( line )
				if len(line) > 0:
					dst.write( line + '\n' )
			else:
				dst.write( line + '\n' )
	
	dst.close()
	return True

def register_result( general_json_path, name, result_path, result_txt ):
	
	gjs = open( general_json_path, 'r' )
	general_json = json.load(gjs)
	gjs.close()
	
	for d in general_json:
		if d.has_key( 'name' ) and d['name'] == name:
			d['result'] = result_path
			d['result_txt'] = result_txt
			d['processed'] = 1
			break
	
	gjs = open( general_json_path, 'w' )
	gjs.write( json.dumps(general_json, indent=4) )
	gjs.close()
	