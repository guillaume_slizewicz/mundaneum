E  preuves        FASC.  21.
1910-05-3.
CONGRÈS  MONDIAL
DES
ASSOCIATIONS   INTERNATIONALES
B  RUXELLES  , 9-11  MAI  1910
L'Association  internationale  pour la pro-
tection légale des travailleurs  et l'Office
international  du travail.
Origines, organisation et développements,
méthodes et résultats.
Nous entendons par protection ouvrière toute action de l'Etat,
des communes et de corporations, qui tend à garantir aux
salariés des chances plus fortes de développement de leurs capa-
cités individuelles, au triple point de vue de la justice, de
l'hygiène et de la sécurité économique. Elle forme ainsi le point
de départ de la participation progressive des masses travailleuses
à la productivité croissante du système industriel ; elle prépare
non seulement les conditions qui les mettent en état d'appré-
cier les créations de la pensée et de l'art, mais elle augmente
aussi leur responsabilité en qualité de citoyens, de parents,
d'époux, de producteurs. La protection forme ainsi la pierre
— 2 —
angulaire de la réforme sociale en général. En effet, si tous les
producteurs et tous les consommateurs travaillaient à une har-
monie aussi complète que possible de tous les intérêts, aucun
souci ne leur serait plus cher que celui de conserver la capacité
productive humaine.
Mais dans aucun pays cette harmonie ne s'est établie jusqu'ici
sans l'intervention des pouvoirs publics. Partout, des mesures
spéciales concernant le régime du travail dans les mines, dans
les usines, les ateliers, les bureaux, les entreprises de transport,
sont devenues nécessaires depuis plus d'un siècle. Partout, ces
mesures de protection ont suivi de près l'essor du machinisme
industriel et des moyens de transport.
Les premières tentatives de protection légale nationale eurent
presque immédiatement leur répercussion dans le domaine
international.
Leur histoire et l'évolution de leurs influences réciproques
prouvera que la protection internationale n'a jamais été que la
conséquence naturelle de l'intervention nationale en matière de
travail.
I . — LES PREMIERS RÉFORMISTES (1816-1848).
C'est en Angleterre que l'idée de la protection ouvrière,
dans l'acceptation moderne du terme, s'est manifestée pour la
première fois. En 1802, ce pays introduisait, en effet, la pre-
mière loi protectrice des ouvriers au nom de la morale et de
l'hygiène publiques. Elle imposait des mesures spéciales qui
concernaient tout d'abord la protection des enfants contre le
surmenage, l'ignorance et la dégénération menaçante : journée
maxima de travail de douze heures pour les apprentis de
fabrique, interdiction du travail de nuit, instruction obligatoire
des enfants jusqu'à un certain âge.
Malheureusement les premières mesures de protection ouvrière
en Grand-Bretagne n'eurent qu'une existence éphémère; elles
étaient condamnées d'avance à une mort naturelle, car la dispa-
rition de l'apprentissage de la fabrique, son remplacement par
l'enfant ouvrier non qualifié rendit cette première loi illusoire.
C'est dans ces conditions que le propriétaire de l'établisse-
ment modèle de New-Lanark, Robert Owen, proclama la néces-
sité de la protection nationale des travailleurs. I l exposa ses
idées dans une réunion des fabricants de coton en 1815. Elles
aboutissaient à la demande d'une loi de protection des enfants
et des adultes; limitation de l'âge d'admission des enfants;
limitation à douze heures du travail des adultes; minimum
d'instruction primaire obligatoire pour tous les enfants, ensei-
gnement ménager pour les jeunes filles comme condition de
leur admission dans une fabrique.
Owen demande, en outre, de l'Etat l'institution d'un système
national d'éducation des caractères, l'exécution des travaux
publics, pendant les périodes de crise dans l'industrie coton-
niére; la présentation de la part des fabricants de rapports tri-
mestriels sur le nombre des ouvriers, sur les salaires et les sans-
travail ; la limitation des licences de vendre des boissons.
ER
Le 1 janvier 1816 Owen inaugurait l'école gardienne et la
Maison du peuple de sa fabrique. I l prononça à cette occasion
i
un discours dans lequel l déclara à ses ouvriers qu'il fera de la
propagande en faveur d'une loi semblable. Et après avoir
i
énuméré toutes les mesures de protection qu'il revendiquait, l
ajoute que les bienfaits d'une loi protectrice ne doivent pas être
limités à un seul pays.
En effet, six mois plus tard, le 14 juin 1816, M. George
Âugustus Lee, de Manchester, déclara à la Commission qui étu-
diait l'effet d'une loi semblable, que l'émigration des capitaux
et des ouvriers en serait la conséquence inéluctable. Ces argu-
ments menaçant le projet de loi, une action internationale
semblait s'imposer. En 1818, Owen [se rendit sur le continent
pour préparer ainsi l'adoption de la loi britannique de protec-
i
tion de l'enfance, pour laquelle l avait tant travaillé.
Owen fit la connaissance de de Sismondi, chez .M m e de Staël,
par l'intermédiaire de l'homme d'État genevois Charles Pictet
de Rpchemont, qui fui l'envoyé plénipotentiaire de la Suisse aux
Congrès de Vienne et de Paris. Owen réussit à convertir de Sis-
mondi et ses idées, et celui-ci fut le premier à critiquer dans
ses nouveaux principes de l'économie politique, la prolétarisa-
tion croissante provoquée par la révolution industrielle.
Après avoir visité les grands éducateurs de l'époque, Oberlin,
à Steinbuch (Alsace), Fellenberg, à Hofwill, et Pestalozzi, à
Yverdon (Suisse), sur l'activité desquels il exerça une grande
influence, Owen fit une propagande active pour introduire la
protection ouvrière sur le continent.
En juillet 1818, à la séance de la Société helvétique des
i
sciences naturelles à Lausane, l expose les inconvénients et les
dangers du système manufacturier. Puis, en date du 20 sep-
tembre de la même année, il adresse de Francfort-sur-Mein un
mémoire à tous les Gouvernements, dans lequel i l expose les
mesures à introduire dans tous les pays pour protéger la classe
ouvrière contre l'ignorance, la fraude et la violence. Au mois
d'octobre, il adressa un second mémoire aux Délégués de la
Sainte-Alliance, réunis en congrès à Aix-la-Cbapelle. 11 prie le
congrès de désigner une commission chargée d'examiner les
mesures de protection qu'il a introduites dans ses établissements
de New-Lanark.
Frédéric Guillaume I I de Prusse lui fit exprimer son appro-
I
bation, mais le spiritus rector de la Sainte-Alliance, Frédéric
von Gentz, s'opposa aux intentions du réformateur social.
Toutefois, des bonimes comme Lord Sbaftesbury et Oastler,
examinèrent les réformes entreprises par Owen à New-Lanark.
Us furent convaincus de la nécessité de la protection ouvrière
nationale, et les progrès que ce mouvement fit en Angleterre
réveilla, sur le continent, l'idée de la protection ouvrière natio-
nale et internationale, dont Daniel le Grand, le disciple du pas-
teur Oberlin, de Steinbach, et de Schleiermacher, de Berlin, fut
désormais l'infatigable champion.
C'était le fils du conseiller d'État bâlois Luc Le Grand, direc-
teur de la République helvétique après 1798, et comme son
— 5 —
père, l fut un grand filateur, établi dans le Steintal, en Alsace.
i
La révolution de juillet (1830) lui démontra la nécessité de
l'intervention de l'État dans les questions ouvrières, et il s'in-
spira de la loi anglaise de 1833 sur les fabriques pour faire des
démarches en faveur de l'introduction d'une loi analogue en
France.
Enhardi par la loi prussienne de 1839 sur les fabriques, i l
adressa en 1840 une lettre ouverte aux Gouvernements français,
suisse et à l'Union douanière allemande pour obtenir une
entende des Étals en matière de protection des jeunes ouvriers.
En France, l'influence de ses démarches se manifesta peu à
peu, et le rapporteur de la loi française de 1841, le baron Dupin,
mentionne la collaboration de Le Grand.
Mais cette loi prolectrice des jeunes ouvriers resta pour ainsi
dire inappliquée, car les industriels l'éludèrent en introduisant
des relais pendant la journée légale de travail. C'est alors que
Le Grand reconnut la nécessité de la prescription légale de la
journée maxima de travail pour les adultes comme pour les
l
jeunes ouvriers, afin de prévenir l'évasion de la oi.
Mais on craignait le profit que des concurrents étrangers
pouvaient tirer de journées de travail plus longues. Ce sont donc
les mêmes objections qu'Adolphe Blanqui avait déjà avancées
en 1839 et qu'il formula comme suit :
« Un seul moyen existe de l'accomplir (la journée de travail
e
normale et uniforme) n évitant ses suites désastreuses; ce serait
de la faire adopter en même temps par tous les peuples
industriels exposés à faire concurrence au dehors. Mais le
voudra-t-on? Mais le pourra-t-on? Pourquoi pas? On a bien fait
jusqu'ici des traités de puissance à puissance pour s'engager à
tuer des hommes, pourquoi n'en ferait-on pas aujourd'hui pour
leur conserver la vie et la leur rendre plus douce. » (Cours
d'économie industrielle, 2 e édition, recueilli et annoté par
M. Ad. Biaise, 1838-39, pp. 111-120.)
Cette idée de la stipulation de traités internationaux fut
confirmée par le célèbre VUlermé dans son tableau de l'état
- 6 —
physique et moral des ouvriers employés dans les manufactures
de coton, de laine et de soie, ouvrage qu'il avait entrepris par
ordre de l'Académie des sciences morales et politiques (1840,
p. 93), et par Alban de Villeneuve, l'auteur d'une économie
politique chrétienne, en décembre 1841, au cours des délibéra-
tion de la Chambre sur la loi de 1841.
Le 31 décembre 1847, Le Grand adressa son premier « Appel
respectueux aux Puissances », en faveur d'une législation inter-
nationale prescrivant la journée máxima de douze heures et la
suppression du travail de nuit des femmes, et l ajoutait que si
i
ses propositions n'étaient pas acceptées, le socialisme révolu-
tionnaire ferait son apparition.
Deux mois plus tard, pendant la révolution de février 1848,
paraissait le « Manifeste communiste » de Marx et Engels.
Le 2 mars 1848, le Gouvernement provisoire décidait que la
journée normale de travail sera limitée à dix heures à Paris et à
onze heures en province.
Cette loi fut supprimée le 13 août de la même année, après la
révolution de juin, et parmi les motifs invoqués en faveur de cette
révocation figure « l'impossibilité de conclure des traités inter-
nationaux ».
Ainsi, les tentatives de Le Grand paraissent subir un échec
définitif, en France du moins.
Son dernier espoir, l'Exposition de 18oo, ne lui apporta
aucun résultat appréciable, et l mourut le 10 mai 1859, sans
i
avoir vu la réalisation de son œuvre se dessiner.
I L — LE SOCIALISME INTERNATIONAL (1848-1871).
La révolution sociale, prédite par le pacifique réformiste
Le Grand, reprend l'idée de l'internationalisme, mais d'une
façon bien différente.
11 faut se rappeler que cette fois-ci l'appel émane d'un groupe
de réfugiés allemands qui avaient fondé à Paris, déjà en 1834,
une société secrète : la « Ligue des Bannis ».
Une scission surgit dans cette ligue en 1836, et les ouvriers-
artisans qui en faisaient partie fondèrent la « Ligue des Justes »
qui subit l'influence de Lamennais. Impliquée dans la révolte de
la « Ligue des Saisons », ses membres émigrèrent à Londres;
ils y fondèrent des associations ouvrières qui admettaient aussi
des ouvriers étrangers et devinrent ainsi des associations inter-
nationales, qui adoptèrent pour devise sur leurs cartes d'admis-
sion, imprimées en vingt langues'différentes, les paroles :
« Tous les hommes sont frères ».
Mais cette devise fut bientôt remplacée par cette autre :
« Prolétaires de tous les pays, unissez-vous », qui renferme la
confession de foi de Marx et d'Engels, proclamant que vis-à-vis
d'une bourgeoisie nationaliste, les idées de solidarité et de paix
universelles ne peuvent être réalisées que par la seule classe qui
est forcée de chercher son gagne pain sans considération de
frontières, la classe des prolétaires.
Le « Manifeste communiste », qui a été discuté pendant
dix jours au Congrès de la « Ligue des Justes », en 1847, est le
développement de cet internationalisme nouveau basé sur la
philosophie du matérialisme historique.
Les années 1848 à 1863, années de réaction politique, de
guerres et de stagnation sociale, d'une part, de spéculation et
d'affairisme, de l'autre, furent peu favorables au développement
de la législation ouvrière nationale, et complètement stériles
pour toute espèce d'idée internationale
Toutefois, sous la poussée de l'impérialisme français des
ouvriers de France demandèrent, en 1863, d'être délégués à
Londres, pour manifester avec leurs confrères anglais en faveur
de la dernière révolution polonaise.
i es svndicats ouvriers anglais remercièrent leurs confrères
à Paris dans la même année. Ils insistèrent à cette occasion sur
la nécessité de fonder une union internationale pour empêcher
l'immigration d'ouvtiers étrangers provoquée par les capita-
listes anglais pour briser la force de résistance des Trades-Uniohs
et abaisser les salaires.
- 8 —
La réponse à cette adresse, rédigée par les trades-unionistes
Odger Applegarth et le Prof ' 1 Beesly, fut la désignation d'une
délégation française et la convocation d'un congres qui tint ses
assises le 28 septembre 1804 à Saint Martins Hall, à
Londres.
25 Anglais, 1 0 Allemands, 9 Français, 9 Italiens, 2 Polonais,
2 Suisses en formaient le comité. Ce dernier constitua désor-
mais le noyau de l'Association internationale ouvrière et
déclara vouloir entreprendre par des congrès ultérieurs la
propagande pour l'émancipation de la classe ouvrière dans tous
les pays.
Elle fondait ses expériences sur le double fait que la victoire
de la loi de 10 beures en Angleterre signifiait la faillite de
l'économie ortbodoxe bourgeoise, et que le mouvement coopéra-
tif avait démontré la possibilité d'une organisation indépendante
des ouvriers, bien que l'évolution plus rapide du capitalisme \
mettait des termes et concluait à la lutte des organisations
ouvrières contre la politique agressive et nationaliste d e la
bourgeoisie.
Les congrès suivants de l'Internationale ouvrière eurent pour
mission de discuter tous les détails de ce programme. Le pre-
mier congrès, celui de Genève en 1800, proclama la réduction
internationale des beures de travail comme première étape de
l'émancipation ouvrière et la suffisance de la journée de travail
de 8 heures. I l déclara en outre que le travail d e nuit ne doit être
toléré que par exception, et l condamna le travail des femmes
i
et des enfants.
Les délégués anglais proposèrent l'organisation de grèves
immenses, et, en effet, celles qui éclatèrent en 1809, en Suisse,
à Gand, à Iserlohn et au Creusot, en 1870, furent attribuées à
l'influence de l'Internationale.
Le congrès de Lausanne, en 1807. adhéra au programme
établi par le congrès précédent, et décida l'adhésion à la Ligue
internationale de la Paix et de la Liberté.
Au Congrès de Bruxelles (1808) et de Bâle (1809) le cercle
— 9 —
de l'Internationale s'élargit par l'accession de l'Italie, de
l'Espagne, des Etats-Unis et de l'Autriche.
La Commune empêcha de tenir le congrès de 1870, qui avait
été convoqué à Paris.
Le Congrès de La Haye (1872) marqua la fin de l'Association
ouvrière, grâce aux conflits qui surgirent entre les partisans de
la méthode allemande et anglaise d'une part et la tactique des
Latins et des Slaves libertaires de l'autre.
I I I . — L'INITIATIVE OFFICIELLE (1871-1897).
Toutefois l'idée d'une action internationale en faveur des
réformes sociales ne tarda pas à être ressuscitée. La Commune
a  vait  liiii siisir aux économistes et aux hommes d'Etat éclairés
tous les dangers du laissez faire.
En 1871, le Prince de Bismark déclarait qu'il était urgent
d'entrer en pourparlers avec l'Autriche pour réaliser les réformes
sociales d'un effort commun. Ces tentatives échouèrent.
La même année, l'économiste et professeur allemand,
M. Adolphe Wagner, dans un discours qu'il prononça à Essen
sur les questions sociales, préconisa des négociations interna-
tionales en faveur de la protection légale des travailleurs.
En 1875, le théologien Henri J. W. Thiersch ressuscitait
dans son livre Sur l'État chrétien, les appels respectueux de
Le Grand, et demandait à l'Empereur Guillaume  Ier  de con-
voquer une conférence internationale aux fins de discuter les
mesures à prendre en vue de la protection légale des travail-
leurs.
Les arguments de Wagner surtout parurent très précieux à
un homme d'Etat suisse, M. le colonel Emile Frey, qui pro-
clama la nécessité de la protection internationale en 1870, en
sa qualité de président du Conseil national, dans l'espoir de
désarmer ainsi l'apposition qui se préparait contre la loi suisse
sur les fabriques, qui fut promulguée en 1877.
En 1880, le Conseil fédéral suisse s'adressa aux Puissances
— 10 —
pour en obtenir une action diplomatique, mais ses démarches
restèrent sans résultat.
Les congrès ouvrier suisse à Zurich, et ouvrier français à
Paris, en 1883, demandèrent l'intervention des Gouvernements
en matière de protection légale des travailleurs. La même année,
des discours favorables à l'interventionnisme national furent
prononcés au Parlement français, de même au Parlement alle-
mand, en 1886.
En 1886, la campagne des fabricants suisses contre la loi sur
les fabriques devient de plus en agitée. La fête centrale de la
Société du Grùtli, qui a eut lieu la même année à Granges,
réagit contre l'agitation des industriels suisses et vota les reven-
dications suivantes : journée de 10 heures; fixation à 15 ans de
la limite d'âge de l'admission de mineurs dans les fabriques ;
luttes contre les poisons industriels; création d'un OHice inter-
national du travail. Les socialistes et les catholiques suisses
s'unirent pour réaliser ce programme. En 1887 est fondé le
Secrétariat ouvrier suisse, et le D Decurtins adresse au Conseil
r
fédéral suisse son mémoire sur la protection ouvrière interna-
tionale.
Malgré l'échec de 1880, le Conseil fédéral suisse prend de
nouveau en 1889 l'initiative d'une conférence internationale
ayant pour programme l'élaboration d'un code industriel
commun, et qui devait avoir lieu dans les premiers jours de
mai 1890. à Berne.
I
Le 3 février 1890 parurent deux rescrits de Guillaume I qui
convoquait une conférence internationale pour la protection
ouvrière, à Berlin.
Peu avant cette conférence, Léon  XIII  et Guillaume I I
échangèrent des congratulations à propos de la législation
internationale du travail. L'encyclique de conditione opificum a
confirmé, sur ce point, l'attitude prise par le pape.
Le congrès de Berlin s'occupa de la réglementation du travail
dans les mines, du travail du dimanche, du travail des enfants,
des jeunes ouvriers et des femmes.
— 1 1 —
Les seuls délégués suisses, allemands et un peu les délégués
autrichiens se montrèrent sincèrement partisans d'une légère
amélioration internationale ne pouvant pas donner de résultats
positifs.
En 1891, un code industriel est promulgué en Allemagne, la
France introduit une loi protectrice des femmes en 1892, et la
Grande-Bretagne un amendement en faveur des enfants.
En 1895, le mouvement en faveur de la journée de 10 heures
i
se manifeste de nouveau en Suisse, mais l n'obtient aucun résul-
tat, parce qu'on lui oppose l'argument de la concurrence inter-
nationale. Sur ces entrefaites, le Conseil fédéral suisse tente une
nouvelle action diplomatique; les Puissances ne lui répondent
qu'en 1897 et le résultat de ses démarches est nul. Sur deux
points surtout l'initiative officielle avait échoué, i l lui manquait
l'unité de vues parmi les hommes d'Etat et la documentation
internationale. Rien de plus facile, dans ces circonstances, que
de déjouer les intentions les plus généreuses.
IV. — L'ORGANISATION INTERNATIONALE DES PARTIS ET LES PAVS FAVO-
RARI.ES A LA PROTECTION OUVRIÈRE. AGE DES TRAITÉS DU TRAVAIL
(190i et 1906).
A la suite de l'issue peu encourageante des démarches diplo-
matiques de 1896, la Fédération ouvrière suisse convoqua le
Congrès de Zurich pour le mois d'août. Cette imposante
assemblée, dans laquelle, sous la présidence de M. Henri
Scherrer, l'union de tous les partis se dessina pour obtenir une
protection ouvrière plus large, fut suivie un mois plus tard
(30 septembre 1907) d'un congrès d'économistes tenu à
Bruxelles, qui décida de fonder une association et un office
internationaux privés.
Un comité belge composé de MM. Mahain, Brants et le duc
d'Ursel fut chargé de l'élaboration des statuts de cette associa-
tion. Peu après se formèrent des comités allemands (sous la
présidence de M. de Berlepsch), autrichiens (sous la présidence
— 12 —
de M. de Philippovich) en 1899, français (sous la présidence de
M. Cauwès) en 1900.
Du 25 au 29 juillet 1900 eut lieu le Congrès de Paris, sous
la présidence de M. Millerand Ce Congrès adopta les statuts de
l'Association internationale présentés par le Comité belge. Puis
il désigna la Suisse comme siège de l'Oilice international du
travail. M. Henri Scherrer de St. Gall fut nommé président,
M. Etienne Bauer, de Bàle, secrétaire général de l'Association
et directeur de l'Oilice. Ce dernier inaugura son activité le
I ' e mai 1910.
I l convient de mentionner aussi que les congrès interna-
tionaux d'hygiène et de démographie et, en général, les méde-
cins éclairés, ont beaucoup contribué à l'entente internationale
désirable au double point de vue de la santé publique et de la
conservation de la force productrice de la classe ouvrière.
Le mouvement international en faveur des traités ouvriers
a donc trouvé, après tant de difficultés, des partisans et des
amis dans tous les rangs, sauf parmi les ebampions de plus en
plus rares de l'économie politique orthodoxe.
Ce résultat correspond complètement aux idées cl aux
influences auxquelles le mouvement doit son origine. Ce furent
d'abord les réformistes, de Owenà Le Grand, qui le préconisaient
comme solution à opposer à l'argument de la concurrence
internationale. Par la réduction des heures de travail, ils enten-
daient rétablir le milieu familial — et ceci constitue le côté
conservateur de leurs réformes — et de faire pénétrer l'éduca-
tion et les idées d'association et d'organisation dans les masses
ouvrières.
La seconde source à laquelle l'Association internationale doit
son origine est d'essence syndicaliste. Son but principal était de
combattre l'immigration du travail mal rétribué, favorisée par
des capitalistes, et de relever universellement les conditions du
travail. Elle vise à assurer un minimum de revenus et de travail
aux nations avancées en cherchant à étendre les mêmes condi-
tions aux autres nations.
— 1 3 -
Sa troisième source, qui est née de l'idée chrétienne de la
fraternité humaine, c'est la conception socialiste de la solidarité
universelle de la classe ouvrière contre les guerres commerciales
des classes dirigeantes. Son objet est de lier les nations par des
traités et d'assurer la paix.
La quatrième source doit son origine à la lutte moderne
contre l'avilissement de la race par les poisons industriels et des
influences semblables. Elle a pour objectif d'assurer le minimum
d'hygiène nationale, d'orienter et de stimuler l'esprit d'invention
vers le progrès hygiénique.
Comme on le voit, les tendances en apparence les plus con-
ti adictoires convergent, dans le domaine international, vers le
même but, et se proposent de faire participer les classes
ouvrières aux manifestations caractéristiques de la civilisation et
de répandre ses bienfaits d'une façon plus rapide.
Car la conservation et l'économie des énergies humaines sont
la condition préliminaire indispensable, qu'il s'agisse de l'édifi-
cation, par l'action collective, d'un système social plus compli-
qué et mieux réglé que le nôtre, ou simplement de la satisfac-
tion des exigences croissantes de la vie moderne.
