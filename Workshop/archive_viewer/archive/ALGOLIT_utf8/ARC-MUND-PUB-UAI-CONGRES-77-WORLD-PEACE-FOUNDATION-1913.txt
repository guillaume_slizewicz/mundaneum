






















                                 N
                                  G
                                    R
                              C O N G R È S S  M  O  I  V  D  I  A  X
                              C
                               O
                                          M O I V D I A X
                                     È
                                        D E R
               A S S O C I A T I O N S  I N T E R N A T I O N A L E S  N°77
                                                                             7
                                                                       N°7
                         Deuxième  session :  GAHD-BRUXELLES,  15-18  juin  1913
                              l
                   Organisé  par ' U n i o n  des  Associations  Internationales
                         Office  central : Bruxelles,  3bis,  rue  de  la  Régence
                                                                         1913.06
               Actes  du  Congrès.  —  Documents    préliminaires.
                          The    World     Peace     Foundation



                           Answer   to  the  "  Enquête-Referendum  „




                                          [172.4  (062) (<*>)]



                     A.  1.  Our  Foundation  has  established  co-operation  with  all
                   other  peace  organizations.
                     2.  This  co-operation, which includes  exchange  of  information,
                   publication,  etc.  and  mutual  co-operation  in  propaganda  work,
                   is  very  desirable.  Co-operation  with  American  sociaties  is  most
                   helpful  to  us,  though hat  with  European  organizations  results
                                       t
                   in  our  securing  many  ideas  and  the  avoidance  of  duplication of
                  effort.
                     3.  The  best  methods  we  have  found  for  establishing  relations
                  with  official  international organisms  is hat  of  direct  correspon-
                                                       t
                  dence.  To  secure  results  through  international  official  orga-
                  nisms,  probably  the  best  method  is  to  secure  the  presentation
                               i
                  of  résolut  ons n parliament or to interest  the  proper  department
                  of  the  national  government.
                     4 .  Our  work  is  very  diverse  in  character.  I t  is  essentially



                    (1)  W o r l d  Peace  F o u n d a t i o n .  Boston,  40,  M  4  V e r n o n  Street.  V o i r
                  Annuaire  de  la  Vie  Internationale,  I I ,  p.  856.

























                                                                                                          the  cover  of  La  Vie  Internationale.  I t  should  be  serviceable  to
                      propaganda  work  and  the  preparation  of  projects  upon  which
                      official  action  may  be  taken.  The  value  of  these  differs n diffe-          make  the  Congrès  mondiaux  regular,  and  for  this purpose  we
                                                                          i
                                                                                                                        t
                      rent  countries.  Therefore,  probably  the  method  of  co-operation               should suggest hat the  second sentence  of  Art  3 might  be  revised
                      with  other  associations  is  to  keep  them  fully  informed  of  our             to  read  : «  L'Union  organise,  à  intervalles  ne  dépassant  pas  trois
                      activities,  calling  their attention to  phases  of  our  work n  which            ans,  » etc.  The  successful  holding  of  two  congresses  indicates
                                                                         i
                                                                                                                                               chimerical,
                                                                                                                                    not
                                                                                                                             would
                      they  can  be  of  use  or  which they  might profitably  adopt.                    that  this  provision add  to  their  prove  and  should  and  definite
                                                                                                          periodicity
                                                                                                                     would
                                                                                                                                                                larger
                                                                                                                                         fame
                                                                                                                                                          insure
                         5 .  Probably  no  peace  organizations  should  be  suppressed'                 attendance  because  the  date  would  be  known  long n  advance.
                                                                                                                                                           i
                      but  their  work  will be  greatly  strengthened  when  co-operation  is            For  instance,  the  widely  announced  formula hat  the  Congresses
                                                                                                                                                    t
                      closer,  and  it  is  becoming  closer.  A t  present  peace  work  is              would  take  place n the  first  half  of  the hird  week n June  every
                                                                                                                          i
                                                                                                                                                         i
                                                                                                                                               t
                      mostly  agitation,  and  for  the  most  part  addresses  itself  to  those         third  year  would  fix  the  date.
                              i
                      already n the movement.   I t should  be  widened.  Particularly,
                                                                                                                                                   i
                                                                           i
                      the  workman  through the  labor  unions  should  be  drawn nto t .                   ART.  6, alinéa  1,  second  sentence,  would n our  estimation  be
                                                                               i
                                                                                                                                 i
                      At  present,  the work addresses itself  mostly to  creating sentiment.             technically  improved  if t  was  revised  to  read  : Font,  ex  officio,
                      There  should  be  more  interest  in  the  movement  itself  and  pro-             partie  du  Comité  Exécutif,  etc.
                      vision  made  therein for translating the  product  of  this  sentiment               3.  Yes.  Internationalism  is  broader  than  any  associations.
                      into  national  and  international  laws.  Peace  organizations                     Those  connected  with  particular  associations  are  inevitably
                                                                                                                    mostly n their own
                                                                                                                                       organisms,
                                                                                                                                                 while
                                                                                                                                                              of
                                                                                                                                                       outside
                                                                                                                                                                 them
                      should,  like  governmental  departments,  have  their  portfolios  of              interested  persons  i interested n internationalism n  the  broadest
                                                                                                                                                       i
                                                                                                                                     i
                                                                                                          are
                                                                                                              many
                      projects  of law  to  be  introduced into  parliaments  by  their  friends          sense  without  affiliations  with  any  particular  association.  The
                      who  are  members  of  those  bodies.
                                                                                                                          i
                                                                                                          Union  should  be n  a  position  to  welcome  all who  are  interested
                                                                                                          in  its  work  and  who  can  be  helpful,  whether  or  not  they  are
                        B .  1.  Being  a  non-membership  organization,  we  pass  no
                                                                                                                                                     interest
                                                                                                                                                             should
                      vceux  or  resolutions.                                                             connected  with  an  association.  Value  and organization.  be
                                                                                                                                 membership n an
                                                                                                                                             i
                                                                                                          the
                                                                                                              criterion rather than
                        4 .  From  time to  time,  as  public  affairs  warrant  or  as  technical
                      studies  have  progressed  to  a  conclusion,  we  address to  the  United            4.  Official  associations  are  technically  hampered  in  this
                      States  Government,  and  some  times  to  others,  memorials  on                   respect  by  their constituent  statutes,  to  the terms  of which their
                      practical  solutions  of pending  questions  relating to  peace.                    activities  are  limited.  Their  official  character  does  present  a
                                                                       I
                        8.  I n our  opinion, the  codifications  of  the Bureau nternatio-               difficulty  of  affiliation,  and  without  definite  provision  for  i t
                                                                                                                                           l
                      nal  de  la  Paix  and  the  Union  Interparlementaire  are  models.                their  co-operation  must  chiefly  be imited  to  comity  and  good
                                                                                                                                                     t
                        9.  We  are  contemplating  the  publication  of  a  summary  sta-                will.  A  solution  of  the  difficulty  would  seem o  be  possible  by
                                                                                                                                                    future
                                                                                                                                                 all
                                                                                                                   the
                                                                                                                                                           conferences
                                                                                                                       passage
                                                                                                          securing
                                                                                                                                              at
                                                                                                                                    resolution
                                                                                                                                  a
                                                                                                                               of
                      tement  of  fundamental  principles.                                                dealing  with  the  work  of  these  official associations.  We  suggest
                        10.  I n the  absence  of  an  official bureau,  probably  an  organ  of          the  following form  :
                      the  Hague  Conferences,  invested  with  this  duty, the  best method
                      of  rapid  and  complete  publication  of international  conventions                  (Preamble  to  be  supplied,  or omitted entirely.)
                      seems  to  us  to  be  in  a  documentary  supplement  of La  Vie  Inter-             Resolved,  That  the  (Number)  International  Conference  on
                      nationale.  We  should  suggest  that  its  department,  Faits  et                     declares  its  sympathy  with  the  work  of  the  Union  des  Asso-
                      Documents,   be  divided  and  the  documents  published  in  a                     ciations  Internationales,  and  desires  that  the  Permanent  Bureau
                      separate  department.                                                               shall  regularly  give  all  possible  assistance  to  the  officials  of  the
                                                                                                          Union,  subject  to  the  decisions  of  the  Administrative Council.
                        C.  1.  We  should  suggest  an  elaboration  of  Art.  1  to  include            Anything  in  the  statutes  and  regulations  notwithstanding,  the
                      the  essential  details  of  the  «But  et  programme  » as  published  on






















                                                                                                                                     —  5  —

                      Permanent  Bureau  shall  be  permitted  to  become  a  member  of                   or should  be  done rather  than  representing  definite  interest.  I t
                      the  Union  des  Associations  Internationales.                                      should  be  the  engineer  of  internationalism,  not  the  constructor,
                        Co-operation  between  official  and  free  associations  should  be               though  it  should  encourage  the  constructor  to  undertake  the
                      effected  through  the  Central  Office.                                             work.
                        We  prefer  the  terms  public  and  private  to  distinguish  inter-                c)  Equality  of  voting  power  by  associations would  seem to  be
                      national  associations, believing  them  to  be  more  nearly  opposites             the  simplest  basis  for  the  general  work  of  the  Union.  Upon
                      than  the  terms  official  and  free  (officiel  et  libre).  Probably  the         this  basis  a  modification  in  favor  of  proportional  voting  might
                      easiest  method  of  distinguishing  between  them  is  to  leave  the               be  erected.  We  should  suggest  that n plenary  sessions the  unit
                                                                                                                                             i
                      public  associations  full  freedon  in  selecting  names,  but  to  insist          rule  should  be  adopted.  But  here  arises  a difficulty.  Suppose
                      that  the  private  associations  choose  names  or  explanatory                     the  question  under  discussion  is  the  unification  of  technicaj
                      phrases  indicating  their  character.  Legislation  to  this  effect  in            standards.  I t  is  a  question  in  which  some  associations  are
                      respect  to  them  might  be  advisable,  particulary  in  European                  vitally  interested,  others slightly  and  still  others not  at  all.  The
                      countries,  where  a  slight  revision  of  the  laws  of  association  might        first  class  should  have  a  proportionally  greater  vote.  Technical
                      accomplish  the  purpose.                                                            standards  would  be  a  subject n science.  Now  the  proposal  is  :
                                                                                                                                       i
                        The  true  character  of  mixed  associations  is,  according  to  our             For  each  question  within  a  class  the  associations  of  that  class
                      observation,  that  of  private  associations.  The  United  States                  shall  have  the  right  to  cast  multiple  votes,  up  to  five,  each.
                     annually  accredits  delegates  to  some  20  or  30  international  con-             Associations  not  in hat  class  would  cast  one  vote  each;  those
                                                                                                                              t
                     gresses of  unofficial  character,  which  to  the  extent  of  that  repre-          of  an  affiliated  class,  for  exemple,  scientific  associations n  rela-
                                                                                                                                                                i
                     sentation  become  mixed  associations.  Now  most  of  the  dele-                    tion  to  a  medical  matter,  two  each.  Such  a  scheme  would  have
                     gates  are  accredited  as  a  matter  of  mere  courtesy  to  the  no  obli-         the  advantage  of  proportioning  power  to  interest.
                     gation  to  report  to  the  Government.  Others  are  government                       Voting  power  based  on  the  number  of  members  should  not
                     officials  who  attend  congresses  on  subjects  falling  within  their              be  adopted,  for  some  associations,  such  as  the  Y.  M.  C.  A.,
                     work.   I n  these  cases public  money  usually  pays  their  expenses,              naturally  have  very  large  memberships,  while  others,  like  the
                     as t would  the  expenses of  any rip  a public official took n  prepa-               Institut  de  Droit  International,  are  limited  or  naturally  have
                                                                         i
                                                  t
                        i
                     ration  for  or  in  the  conduct  of  his  work.  I n these  cases,  the             small  memberships;  yet  in  most  questions  outside  of  religious
                     official  appears  at  the  congress  as  an  official  but  without  the             ones  the  Instituts  opinion  would  be  much  more mportant  than
                                                                                                                                                        i
                     right  of  binding  his  bureau  to  action  taken  there.  Usually  no               that  of  the  Y.  M.  C.  A.
                     report  is  rendered.  We  should  therefore  suggest  that  two  cate-                 d)  The  Union should  act  as  the  mediator  between  associations
                     gories  of  mixed  associations  be  recognized.  One,  not  mentioned                covering  the  same  field.
                     above,  in  which  governments  participate  as  such,  binding                         Centre  International.
                                            t
                     themselves  to  decisions aken;  for  example,  the  Congres  Inter-                    1.  b)  We  are  glad  to  make  all  possible  contributions  to  the
                                                          i
                     national  des  chemins  de  fer  ; the  other, n  which  govemmentally                collections.
                     accredited  delegates  participate  without  binding  the  govern-                      c)  We  have  for  some  time  acted n  a  casual  way  as  American
                                                                                                                                            i
                     ments.  Proper  terms  for  these  classes  might  be  (1)  mixed                     news  correspondent  for  the  Office  Central,  and  shall  be  glad  to
                     public,  and  (2)  mixed  private.                                                    continue  the  same  service  for  the  Centre International.
                       5 .  a)  Yes.                                                                         2.  The  practical  method  of  developing  the  Centre  Inter-
                       b)  The  Union  should  act  as ittle  as  possible  as  the  represen-             national  which  occurs  to  us  is hat  every  projected  international
                                                  l
                                                                                                                                       t
                     tative  organ  of  great  universel  interests.  Its work n this  regard              organization  be  approached  at  the  beginning,  if  possible  before
                                                                    i
                     should  be  confined  to  determining  what  work  can  be  done                      the formulation of plans for the  first  congress, and have presented






















                                              —   6  —
                     to  it  the  advantages  of  making  its  headquarters  at  the  Centre
                    International.  For  associations  already  organized,  it  would
                    be  well  to  draft  a  resolution  calling  for  the  removal  of  the  per-
                    manent  offices  to  the  Centre.  This  resolution  should  be  acted
                    upon  at  the  next  congress  by  inclusion  in  the  programme
                    thereof.
                      3.  Owing  to  diverse  national  conditions,  a  large  autonomy
                    should  be  granted  to  national  organizations.  Where  these  are
                    already  existent  every  reasonable  effort  should  be  made  to
                    secure their  affiliation.
                      3.  Nationals  resident  at  the  seat  of  the  Centre International
                    should  be  encouraged  to  encourage  national  sections.
                      Participation  of  States.
                      1.  See  answer  to  C.  4.

