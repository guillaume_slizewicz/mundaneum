




















                                             .
                                N
                                              M
                                  G
                                           O
                             C O N G R È S S  M O . M ) I A L L
                             C
                                          M
                               O
                                    R
                                     È
                                                  A
                                                )
                                                 I
                                       I>E8
               A S S O C I A T I O N S  I N T E R N A T I O N A L E S  N  ° 5 7
                                                                       N°57
                        Deuxième  session  :  GAND-BRUXELLES,  15-18  juin  1913
                  Organisé  par  l ' U n i o n  des  Associations  Internationales
                         Office  central  : Bruxelles,  3bis,  rue  de  la  Régence
                                                                       3 e  Section
                         Gongrès.
                                       Documents
                                   —
                                                    préliminaires.
              A  c t e s  du  Gongrès .  —  D  o c u m e n t s  préliminaires .
              Actes
                     d
                       u
                       Fixation    des  Systèmes     Internationaux
                                      d'Unités    légales
                                           R A P P O R T
                                               D E
                                 M .  F E R N A N D  C E L L E R I E R  ^
                       Directeur  d u  L a b o r a t o i r e d'Essais  d u  Conservatoire  N a t i o n a l
                                     des  A r t s  et  Métiers,  Paris
                                              [531.7]

                            I.  —  C L A S S I F I C A T I O N  D E S  UNITÉS

                    Le  Congrès  Mondial  des  Associations  Internationales,  réuni
                  à  Bruxelles  en  1910,  avait  voté,  à  l'unanimité,  les  considérants
                  relatifs  à  l'emploi  international  du  Système  métrique  pour  les
                  usages de  la mesure dans  tous les  domaines de  la Science,  de  l'In-
                  dustrie  et  du  Commerce,  et  avait  émis  le  vœu  que  les  Associa-
                  tions Internationales,  dans  leur  domaine  d'action, se  préoccupent
                  d'assurer cette  unification en l'appuyant sur le Système  métrique.
                    Déjà,  des  propositions et  des  études  ont  été  faites  sur  ce  sujet
                  et  le  Congrès  Mondial  est  en  mesure  d'espérer  que  son  vœu  sera
                  réalisé  prochainement  par  plusieurs  États.
                    A  ce  sujet,  i l convient de  signaler,  qu'actuellement  en  France,
                  le  Ministère  du  Commerce  et  de  l'Industrie  s'occupe  de  fixer,
                  par  voie  législative,  les  diverses  unités,  de  manière  à  apporter
                  les  modifications  ou  extensions  nécessaires  à  la  législation  des























                                                —  2  —

                     Poids  et  Mesures,  et  posséder  une  base  uniforme et d'un  carac-                 ques  en  unités  mécaniques,  unité  d'échelle  de  température,  unités
                     tère  international dans  les  transactions  où  ces  unités  intervien-               électriques,  unités  optiques.
                     nent.                                                                                    Chacun  de  ces  groupes  comportant, lui-même, des  unités  clas-
                                                                       a
                       I l  est  donc  bien  probable  que  la  France,  qui  le  7 vril  1795              sées  suivant  les  divisions  de  chacun  d'eux.
                     créa  le  Système  Métrique  décimal  possédera  prochainement  un
                     Système  légal  d'unités  métriques  d'un  caractère  international.
                       Pour  faciliter  cette  réalisation,  i l  paraît  nécessaire  d'établir                          I I .  —  UNITÉS  MÉCANIQUES
                     une  distinction  entre  les  mesures  plus  particulièrement  du                        Divers  systèmes  pratiques ont  été  proposés.  Ils dérivent  d'ail-
                     domaine  scientifique  —  et pour lesquelles  le  maximum de  précision                leurs  du  système  absolu  C.  G . S.,  universellement  adopté,  mais
                     est  nécessaire,  —  et  les  mesures  destinées  aux  transactions  indus-            dont  les  unités  ne  sont  pas,  en  général,  de  l'ordre des  grandeurs
                     trielles  et  commerciales.
                                                                                                            usuelles.
                       Pour  les  premières,  en  raison  des  modifications  qu'entraînent                   Parmi  ces  systèmes,  celui  des  mécaniciens,  Système  M. K.  S.,
                     parfois  assez  rapidement  les  résultats  plus  précis  de  la  science,             paraît  se  rapprocher  le  plus  des  besoins  de  la  Mécanique.  I l pré-
                     il  paraît  inutile  de  les  faire  toutes  adopter  dans  des  lois qui,  par        sente  aussi  l'avantage  de  pouvoir  réaliser  matériellement  l'unité
                     leur  nature  même,  si  elles sont  lentes  à  établir,  sont  parfois  plus          de  force  qui  n'est  autre  que  le  poids  d'une  masse  de  un  kilo-
                     lentes  à  modifier.  On  risquerait  donc,  en  rendant  légales  trop                gramme.
                     hâtivement,  certaines  unités  scientifiques,  sur  lesquelles  l'accord                L'Union  des  Associations  Internationales  a  déjà  eu  l'occasion
                     n'est  d'ailleurs  pas  toujours  parfait,  de  rendre  rapidement                     de  signaler  l'intérêt  de  ce  système,  par  la  publication,  dans
                     désuètes,  certaines  unités  abandonnées  par  le  Progrès.
                                                                                                            La  Vie  Internationale,  du  «système  de  mesures  et l'organisation
                       I l  semble  bien, par  conséquent,  que, pour  les  mesures  scienti-               internationale  du  Système  métrique  »,  par  M.  C H . - E D .  G U I L -
                     fiques  proprement  dites,  le  rôle  du  Congrès  Mondial  soit  de  faire            LAUME.  Dans  une  étude  des  Unités,  M.  D E  B A I L L E H A C H E  signale
                     connaître,  autant  que  faire  se  peut,  à  toutes  les  Associations                également  les  avantages  du  Système  M. K.  S.
                     Internationales  intéressées,  l'état  dans  lequel  se  trouve  chaque                  Toutefois,  au  point  de  vue  international,  le  Système  M. K.  S.
                     question,  au  fur  et  à  mesure  que  des  propositions nouvelles  sont              présente  un inconvénient  qui tient  à la variation de  la  pesanteur
                     faites.                                                                                d'un  point  à  un  autre  du  glohe  et  même  en  un  même  point,  sui-
                       Par  contre,  l'utilité  de  faire  adopter  dans  les  divers  pays  des            vant  l'altitude  et  les  conditions locales  ;  en  sorte  que  l'unité  de
                     mesures  légales  internationales  pour  la  facilité  des  diverses                   force  est  différente  aux  divers  points  de a  terre. Or, l se  trouve
                                                                                                                                                             i
                                                                                                                                                 l
                     transactions  entre  les  peuples,  doit  décider  le  Congrès,  non  seu-             que  la  variation  de  l'accélération  de  la  pesanteur  aux  divers
                     lement  à  renseigner  les  Associations  Internationales  intéressées                 points  du  globe  est  relativement  faible  (au  maximum  1/200 e
                     sur  l'état  des  questions,  mais  encore  à  attirer  l'attention  de  ces           environ  de  sa  valeur).  Si  donc,  on  considère  seulement  l'emploi
                     groupements  sur  les  moyens  qui paraissent  les  plus propres  pour                 pratique  d'unités  internationales, l sera  avantageux  de  convenir
                                                                                                                                           i
                     préparer  et  assurer  l'unification  légale  des  mesures.                            d'une  accélération  internationale de  la  pesanteur,  ou  s'il  est  re-
                                                                l
                       I l  y  a  lieu  tout  d'abord,  de.  s'entendre  sur a  classification  à           connu  nécessaire,  d'une  accélération  par  « zone  sphérique  »  rap-
                     donner  aux  unités  suivant  le  rang  qu'elles  occupent  dans  la                   pelant,  pour  les  parallèles  géographiques,  ce  qui  a  été  convenu
                     succession  des  unités  dérivées. Les principaux  termes  à  accepter                 pour  les  « fuseaux  horaires  ».
                     paraissent  être  unités  absolues,  fondamentales,  dérivées  primaires,                I l  ressort  des  décisions  des  divers  Congrès  scientifiques  ou
                     dérivées  secondaires,  etc.
                                                                                                            techniques  que  des  réglementations  légales  internationales  peu-
                       En  second  lieu,  i l  est  nécessaire  de  classer  les  unités  suivant           vent  être  prises  dans  un avenir  très  prochain, par  les  Etats,  pour
                     les,  divisions  adoptées  pour  les  mesures  scientifiques  et  techni-              les  principales  unités  mécaniques  de  :  longueur,  masse,  temps,























                                                 —  4  -                                                                           —  5  —

                       force,  pression,  énergie  ou  travail,  puissance  et  leurs  unités               Ainsi  donc,  l'échelle  thermodynamique  est  une  échelle  théo-
                       dérivées.                                                                          rique  qui  peut  être  considérée  comme  pouvant  servir  de  base
                                                                                                         précise  pour  les  mesures  de  températures  ;  l'échelle  du  ther-
                          I I I .  —  UNITÉ  D'ÉCHELLE   D E  TEMPÉRATURE                                momètre   normal  à  hydrogène,  peut  lui être  pratiquement  subs-
                                                                                                          titué,  avec  une  précision  toute  scientifique,  entre  — 2 5  0  et
                         Dans,les  divers  milieux  scientifiques,  on  est  d'accord  pour               +  100 ,  et  avec  une  approximation largement  suffisante  pour  les
                                                                                                               0
                       reconnaître  que  l'échelle  de  température,  qui  présente  tous  les            besoins  de  la  pratique  pour  les  températures  basses  et  pour  les
                       avantages  de  constance  et  de  précision  indispensables  pour servir           températures  élevées  jusqu'à  1000 .
                                                                                                                                         0
                       de  base  aux  déterminations  de  température,  est  l'échelle  absolue             Malheureusement,  l'emploi du  thermomètre  à hydrogène  sou-
                       thermodynamique,  définie  par  les  indications  que  fournirait  un             lève  des  difficultés  considérables  pour  les  applications  courantes.
                       thermomètre  à  gaz  dit  « parfait »  sous  pression  constante  (ou             C'est  pourquoi, afin  que  cette  définition  ait  une  portée  pratique,
                       sous volume constant) i ) .                                                       il  est  indispensable  de  s'assurer  qu'il  existe  des  thermomètres
                                            (
                         Si l'on construisait un  thermomètre  avec  un  tel  gaz,  les  varia-          auxiliaires,  commodes  d'emploi et  suffisamment  précis,  étalonnés
                       tions  centésimales  observées  définiraient  les  températures  en                d'après  cette  échelle  normale.
                       fixant  deux  points  de  cette  échelle,  par  l'état  du  thermomètre              Pour  les  températures  ordinaires,  les  remarquables  travaux
                       dans  la  glace  fondante  (o°)  et  dans  la  vapeur  d'eau  bouillante          du  Bureau  International  des  Poids  et  Mesures  ont  montré  que  le
                       sous  la  pression  atmosphérique  normale  (ioo°).                               thermomètre  à  mercure  peut  devenir  un instrument de  précision
                         Mais,  un  tel  thermomètre  est  matériellement  irréalisable,  et             de premier ordre, à la condition  d'être  convenablement construit,
                       l'échelle  thermodynamique  est  une  échelle  théorique  à  laquelle             soigneusement  étudié  et  employé  suivant  certaines  règles.
                       toutefois on pourra toujours avec certitude se  reporter:                            Pour  les  basses  températures,  i l  résulte  notamment  des  tra-
                         Les  remarquables  travaux  exécutés  dans  ces  dernières  années              vaux  de  Kammerlingh  Onnes  et  Crommelin  en  1906,  sur  les
                       en  thermométrie,  particulièrement  en  France,  au  Bureau                      couples  thermoélectriques  fer-constantan  de  —253  0  à  o°  et  sur
                       international  des  Poids  et  Mesures,  ont  conduit  les  savants  des          le  thermomètre  à  résistance  d'or  de  — 220  0  à  o°,  ainsi  que  des
                       divers  pays  à  adopter  comme  échelle  pratique  l'échelle  centési-           travaux  de  Meilink  (1904),  sur  les  thermomètres  à  résistance  de
                       male  du  thermomètre  à hydrogène  sous  volume  constant  dite  échelle         platine,  de  —210  0  à  o°,  que  ces  divers  instruments  fournissent
                       normale,  proposée  par  le  Comité  International  des  Poids  et                des  indications de  l'ordre  du  degré,  précision  suffisante  pour  les
                       Mesures  en  1887.                                                                besoins industriels u  «  Froid  ».
                                                                                                                           d
                         Aujourd'hui,  grâce  aux  études  de  haute  précision  de  M. Daniel             Enfin,  pour  les  températures  élevées  jusqu'à  1000 0  environ,
                       Berthelot  on  connaît  les  écarts  entre  les  indications du thermo-           les  nombreux  travaux  exécutés  dans  ces  dernières  années  en
                       mètre  à  hydrogène  et  les  températures  absolues  depuis—240  0               particulier sur les  couples thermoélectriques, montrent également
                       juqu'à  +iooo°.C  'est  ainsi qu'entre o°  et  ioo°,  l'écart  maximum            qu'il  est  possible  d'utiliser pratiquement  à  ces  températures,  des
                       n'atteint  pas  o°,ooo6  ; pour  les  températures  supérieures  à  ioo°          appareils  d'une  précision  de  l'ordre  du  degré,  c'est-à-dire  suffi-
                       jusqu'à  1000 ,  les  écarts  sont  minimes,  à  cette  dernière  tempé-          sants pour les  besoins industriels et  commerciaux  où l'on emploie
                                   0
                       rature,  ils  atteingnent  seulement  o°,05  ;  pour  les  basses  tempé-         ces  températures.
                       ratures  jusqu'à  —240 ,  les  écarts  atteignent  o°,i8.                           Dans  tout  cet  intervalle entre  — 2 4 0  0  à  +1000 ,  des  thermo-
                                           0
                                                                                                                                                        0
                                                                                                         mètres  auxiliaires,  appropriés  à  Ja  température  et  aux  expé-
                                                                                                         riences,  peuvent  donc  contribuer, avec  une  suffisante  approxima-
                         (1)  Rappelons  que  les  gaz  réels  se  rapprochent  des  gaz  dits  «  par-  tion,  à  rendre  pratique  l'adoption de  l'échelle  normale.
                       faits»  c'est-à-dire  q u i  obéiraient  aux  lois  de  M a r i o t t e ,  de  Gay-Lussac,  En  ce  qui  concerne  les  températures  supérieures  à  1000 , 0  on
                       lorsqu'on  d i m i n u e  suffisamment  leur  pression.























                                                 —  6  —                                                                               —   7  —
                                                                                                                                                        d
                                                                                                                              1
                       ne  peut  songer  au  thermomètre  à  hydrogène,  mais  les  derniers                 lieu  à  Londres  en 908,  confirmait le  principe u  choix  des  deux
                                                                                                                                  l
                       progrès  de  la  Pyrométrie  permettent  d'entrevoir  prochainement                   Unités  matérielles  de 'ohm  et  de  l'ampère.  Dans  ces  dernières
                       des  accords  internationaux d'échelles  atteignant  de  très  hautes                 années,  on  a  entrepris  dans  diverses  nations  des  recherches  en
                       température,  lesquelles  seraient,  d'ailleurs,  connues  en  fonction               vue  de  matérialiser  un  élément  de  pile  dont  la  force  électo-
                       de  l'échelle  thermodynamique.                                                       motrice  soit  une  fonction  précise  du  volt  et,  i l  a  ainsi  paru,
                         En   résumé  l'unité  théorique  d'intervalle  de  température                      à  certains  groupements  scientifiques,  que  le  volt  pouvait
                       peut  être  définie  par  les  divers  Etats  en  se  basant  sur  l'échelle          être  représenté  par  0* g3 0  de  la  force  électromotrice  à  2 0  0  d'un
                       absolue  thermodynamique.                                                             élément  Weston  au  sulfate  de cadmium.
                         Pour  les  applications  industrielles  et  commerciales  entre                       Les  uns  ont  alors  préconisé  de  remplacer  comme  unité  fonda-
                       — 2 4 0  O  à  + 1 0 0 0  0  : l'unité  d'intervalle  de  température  ou  degré  cen-  mentale  l'ampère  international,  par  le  volt  ainsi  défini,  et  de
                                                                                                             déduire  l'ampère  de  la  loi  d'Ohm,  les  autres,  admettant  que
                       tigrade  est  la  variation  de  température  qui  produirait  l  e  _  ^ j
                                                                                                             la  loi  d'Ohm  est  suffisamment  remplie  pour  les  besions  delà
                       de  la  variation de  pression  de  l'hydrogène  sous volume  constant                Technique,  ont  proposé  de  considérer  les  trois  Unités,  ainsi
                       prise  sous  la  pression  manométrique  initiale  de  1  mètre  de                   matérialisées,  comme  toutes  trois  fondamentales.
                       mercure  entre  les  points  fondamentaux  o°  glace  fondante  et                      L'accord  est  donc  loin  d'être parfait.
                       ioo°  vapeur  d'eau  en  ébullition  sous  la  pression  atmosphérique                  Quel  que  soit  l'avantage  de  posséder  un  étalon  matériel,  fonc-
                       normale.                                                                              tion  de  volt,  i l  paraît  cependant  nécessaire  de  faire  remar-
                         La  série  de  ces  intervalles  constitue  l'échelle  normale  de 'hy-             quer  que  l'élément  Weston  est  relativement délicat  de  confection
                                                                              l
                       drogène,  qui  se  confond  sensiblement  avec  l'échelle  thermody-                  et  d'emploi,  qu'il  n'a  pas  encore  reçu  la  sanction  d'une  très
                       namique.
                                                                                                             longue  expérience  permettant,  en  particulier,  de  conclure  qu'il
                                                                                                             sera  possible  pour  les  époques  qui  suivront  de  le  reproduire
                                   I V .  —  UNITÉS  ÉLECTRIQUES
                                                                                                             toujours  identique  à  lui-même.  D'ailleurs,  i l  paraît  bien  pro-
                         Les  unités  électriques  du système  pratique it « Système  inter-                 bable  que  les  exigences,  qu'entraînent  les  rapides  progrès  des
                                                                d
                       national  », préconisé  en  1893  par  le  troisième  Congrès  internatio-            mesures  électriques, ui feront préférer,  peut-être  dans  un  avenir
                                                                                                                                l
                       nal  d'Électricité,  réuni  à  Chicago,  servent  de  base  actuellement              très  prochain, n  autre  étalon  plus  précis  et  encore mieux  défini.
                                                                                                                          u
                       dans  les  divers  pays  à la  fixation  d'Unités  électriques  légales.                Enfin,  si 'on  admet  le  principe  de  considérer  le  volt  comme
                                                                                                                        l
                         Ce  Congrès  avait  défini,  ainsi  qu'on  sait,  deux  Unités  princi-             troisième  Unité  fondamentale  définie  en  fonction  d'un  étalon
                       pales  ou  fondamentales  :  l'ohm  international  pour  la  résistance               matériel, l n'y  a  pas  de  raison pour  ne  pas  également  admettre
                                                                                                                      i
                       électrique  et  l'ampère  international  pour l'intensité  de  courant.               comme   unités  fondamentales  l'unité  de  capacité,  l'unité  de
                         Etaient  considérés  comme  Unités  dérivées  :  le  volt,  le  coulomb,            self-induction,  gar  exemple,  qui peuvent  être  matérialisées.
                       le  farad,  le  joule,  le  watt,  etc.                                                 Si 'on  se  place  au  point  de  vue  de  l'élaboration  des lois et  de
                                                                                                                  l
                         Les deux unités fondamentales  choisies étaient  représentées  en                   leur  application  dans  les  divers  pays  qui  est  en  fin  de  compte
                       fonction  d'un étalon  matériel  : l'ohm  par  une  colonne  de  mercure              un  des  buts  que  doit  poursuivre  le  Congrès  Mondial,  ne  risque-
                       de  section  uniforme, ayant  une  masse  et  une  longueur  détermi-                 t-on  pas  alors  en  introduisant  dans  le  texte  d'une  loi  autant
                       nées,  l'ampère  par  un  certain  dépôt  d'argent  provoqué  par  un                 d'unités  fondamentales,  dont  le  degré  de  précision  est  suscep-
                       courant  invariable traversant  une  solution de  nitrate  d'argent.                  tible  de  s'améliorer  avec  le  progrès,  de  conserver  comme  légales
                         Le  volt  unité  dérivée  d'un  usage  courant,  se  déduit  alors  par             pendant  de  trop  nombreuses  années, en  raison  de  la  lenteur  que
                      l'application  rigoureuse  de la loi d'Ohm.                                            nécessite  généralement  les  modifications  de  lois,  des  Unités  qui
                         La  Conférence  internationale  des  Unités  Electriques  qui  eut                  seraient  tombées  en  désuétude?























                                              —  8  —                                                                                 —  9  —

                      La  même  observation  s'applique  d'ailleurs  à  la  représentation                  que,  malgré  tout  leur  intérêt  pour  les  études  électriques,  cer-
                    de  l'ampère.  Tout  le  monde  est  d'accord,  que  l'ampère  peut  être               taines  de ces  unités,  envisagées  par  le  Congrès, doivent être  réser-
                    déduit  avec  suffisamment  de  précision  pour  les  besoins  de  la pra-              vées  momentanément  tout  au moins.
                    tique  de  la loi de  Joule,  c'est-à-dire  en fonction du  joule du  Sys-                Tel  est  le  cas,  par  exemple,  pour la  conductance,  l'inductance,
                    tème  K.  M.  S.  défini  comme  unité  d'énergie  aux  unités  des  gran-              le  flux,  la  perméabilité  et  la  réluctance,  dont  les  emplois  sont
                    deurs  mécaniques.                                                                      encore  limités  à  des  cas  particuliers intéressant  des  « spécialistes
                      Si  donc,  on  peut  considérer  les  idées  émises par  quelques-uns,                avertis  ».
                    de  créer  soit  deux,  soit  trois  unités  fondamentales  comme                         I l  peut  en  être  de  même  pour  la  capacité  dont  les  emplois
                    réalisables,  par  contre,  pour  l'établissement  des  dispositions                    courants  sont  encore  limités.
                   légales  internationales,  i l  paraît  préférable  de  n'en  adopter                      D'ailleurs,  si  l'on  adopte  le  principe  que  les  Unités  dérivées
                    qu'une  seule.                                                                          peuvent  être  ultérieurement  définies  par  un  Règlement  d'admi-
                                 l
                      A  cet  égard, 'ohm  est  défini  en  fonction d'un  étalon  matériel                 nistration  publique, l  paraîtrait  actuellement  suffisant  pour  ne
                                                                                                                                i
                   avec toute la précision  désirable.                                                      pas  créer  légalement  un  trop  grand  nombre  d'unités  pratiques,
                      D'ailleurs,  pour  concilier  cette  manière  de  voir  de l'adoption                 non  encore toutes  bien connues du public,  de  se  limiter,  dans  un
                   de l'Ohm comme seule Unité fondamentale avec celle qui préconise                         premier  règlement  aux  grandeurs  électriques  les  plus  courantes.
                   soit  2, soit  3 unités  fondamentales, l suffit  d'ajouter  à la suite  de                 I l  semble  que  l'on pourrait  se imiter  à  définir  les  unités  :  de
                                                    i
                                                                                                                                           l
                   la  définition  de  l'ampère  et  de  celle  du  volt,  une  indication  spé-            quantité  d'électricité  (Coulomb)  et  de  résistivité  (Ohm-centi-
                   cifiant  que  : «  dans les  transactions commerciales  et industrielles,                mètre),  et  éventuellement  de  capacité.
                   »  cette  unité  est  représentée  en  fonction  de  l'étalon  matériel                     Les  deux  premières  de  ces  grandeurs  électriques interviennent
                   »  choisi  ».                                                                             en  effet,  d'une  façon  de  plus  en  plus  courante,  dans  les  transac-
                                                                                                            tions  commerciales  et  industrielles  pour  la  consommation  par
                     Dans  ces  conditions,  les  unités  électriques  légales  seraient
                   classées  de  la  manière  suivante  :                                                   les  services  de  l'État,  les  groupements  ou  les  particuliers,  de
                                                                                                            l'énergie  électrique,  ou dans la connaissance  des  qualités  des  con-
                     A.  —  Texte  de  loi.  —  Unité  fondamentale  : l'ohm,  et  sa  repré-               ducteurs,  pour  les  installations électriques,  pour  les  réseaux  télé-
                   sentation  matérielle, défini  au  même  titre  que  les  unités  méca-                  phoniques,  etc.
                   niques  fondamentales.                                                                      E n  ce  qui  concerne  l'énergie  et  la  puissance,  leurs  définitions
                     B.  —  Règlement  annexe  à  la  loi.  —  (Décret,  Règlement                          rentrent  tout  naturellement dans  celles  adoptées  pour les  mêmes
               Í   d'administration publique, etc.)                                                          unités  dérivées  des  grandeurs  mécaniques,  puisque  le  Système
                     i °  Unités  dérivés  principales  :  l'ampère  (déduit  de  la  loi  de               des  Unités  Electriques  se  relie  à  celui  des  Unités  Mécaniques  par
                   Joule),  le  volt  (déduit  de  la loi d'Ohm).                                           la  loi de  Joule.
                     En  outre,  les  indications de  leur  représentation  pour les trans-
                   actions  commerciales  et  industrielles  en  fonction  de  l'étalon                                V.  —  UNITÉS   PHOTOMÉTRIQUES
                   matériel  choisi ;
                                                                                                               Des  observations  analogues  à  celles  présentées  pour  les  unités
                     2°  Unités  dérivées  secondaires,  c'est-à-dire  celles  qui dérivent,                 électriques,  sont  également  à  formuler  pour  faciliter  l'unifica-
                   soit  par  leur  définition,  soit  par  les  équations,  exprimant  leurs                tion  des  Unités  légales  de  photométrie.
                   propriétés,  des  unités  ci-dessus  et  qui n'ont  pas  encore  été  maté-                 Sous  ces  réserves,  plutôt  de  forme  que  de  fond, l est  utile  que
                                                                                                                                                          i
                   riellement  réalisées.
                                                                                                            le  Congrès  Mondial  signale  aux  Associations  intéressées,  les  déci-
                     A  ce  sujet,  et  en  se  plaçant  au  point  de  vue de  l'application  de            sions  communes  prises  en  1909  après  de  remarquables  études  par
                   la  loi  pour  les  transactions  commerciales  et  industrielles,  i l semble
                                                                                                             le  Bureau  of  Standards  de Washington,  le  Laboratoire Central























                                                                                                                                      —  11  —
                                                —   10  —
                                                                                                            1908,  par  la  Conférence  internationale  des  Unités  électriques  de
                       d'Électricité  de  Paris,  le  National  Physical  Laboratory  anglais,
                       pour  comparer  leurs  étalons  respectifs  de  lumière  à  l'étalon  alle-          Londres.-
                       mand  Hefner  et  à  l'étalon  Violle, lequel  semble  d'ailleurs devoir               B.  —  Par  un  texte  annexe  à  la  loi  (Règlement  d'Administration
                       être  pris  comme  étalon  de  l'imité  fondamentale.                                publique.  Décret,  etc.)  :
                                                                                                              i°  Unités  dérivées  primaires,  l'ampère,  déduit  de  la  loi  de
                                                                                                            Joule  ; le volt,  déduit  de la  loi  d'Ohm.
                                         V I .  —  C O N C L U S I O N S                                      En  outre,  une  indication  complémentaire  spécifiant  que  «  dans

                         Conformément  à la  décision  du  Congres  Mondial  des  Associations              les  transactions  industrielles  et commerciales,  chacune  de  ces  Unités
                       Internationales  réuni  à  Bruxelles,  en  1910,  j'ai  l'honneur  de  sou-          est  représentée  en  fonction  d'un  étalon  matériel  déterminé  ».
                       mettre  au  Congrès  de  1913  que  la  Commission  instituée  en  vue                 2  0  Unités  dérivées  secondaires  :  le cotdomb  ;  l'ohm-centimètre.
                       de  transmettre  les  vœux  des  Congres  à  toutes  les  Associations                 6 .  —  Utilité  d'une  unification  légale  des  mesures  photométri-
                       Internationales  intéressées,  fasse  connaître  à  ces  Associations  les           ques  en  se  basant  sur  les  résultats  obtenus  dans  ces dernières  années
                       vœux  ci-après,  destinés  à  faciliter  la  fixation  de  systèmes  interna-        par  les  grands  laboratoires  des  diverses  nations.
                       tionaux  d'unités  légales  dans  les  diverses  nations  :
                         1.  —  Utilité  d'une  classification  des  Unités  en  :
                                      Unités  fondamentales  ;
                                       —    dérivées  -primaires  ;
                                       —      —    secondaires,  et:.
                         2.  —  Utilité  d'une  classification  scientifique  d'après  les  bases
                       du  Système  métrique  décimal  en :
                                     Unités  mécaniques  ;
                                     Unité  de  température  ;
                                     Unités  électriques  ;
                                       —    photométriques.

                        3.  —  Utilité  d'une  entente  internationale  pour  l'adoption  soit
                      d'un  seul  nombre,  soit  d'un  nombre  par  « zone  » pour  Vaccélération
                      de la  pesanteur.
                        4.  —  Opportunité  de  définir  l'unité  d'intervalle de  température
                      pour  les  besoins  des  transactions  commerciales  et  industrielles
                      de—240 0  à  -}-iooo ,  par  l'échelle  centésimale  du  thermomètre  à
                                        0
                      hydrogène,  dite  «  échelle  normale ».
                        5.  —  En  vue  de  tenir  compte  de  la  durée  de  fixité des  lois  des
                      divers  pays,  il  est  recommandé  d'adopter  pour  les  Unités  élec-
                      triques  :
                        A.  —  Par  un  texte de loi,  au  même  titre  que  les  unités  fondamen-
                      tales  mécaniques  :
                        Unité  fondamentale  de  résistance  électrique  : 'ohm  adopté  en
                                                                  l

