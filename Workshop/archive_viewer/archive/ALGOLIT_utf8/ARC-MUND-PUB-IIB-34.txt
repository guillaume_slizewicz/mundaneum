-

ï¿½EGLES
P .. ,._..-

............ "'I"ï¿½Â·------ï¿½

... Â·- .. Â·Â·' ..

-.:ï¿½Â·

POUR LES

DEVELOPPEM'ENTS
"""'ï¿½':t_I"'T>I(ï¿½""'4:jï¿½,...,IItJ><Â·""''''''.W''''''Â·OUIlI'''';'_!XMJIIW.1IIil''''iï¿½_''.'''I'';'H''''ï¿½.IIï¿½lIIï¿½'i1ï¿½:<'.r!iï¿½'''''''''''ï¿½''''''I''''Â·I'''''
,

....... ï¿½-{....

,

A

APP0RTER ï¿½ LA

CLASSIFICATION DECIMALE
..

-

Office International de

Bibliographie

REGLES
POUR LES

DEVELOPPEMENTS
A

APPOR'l'ER A LA

CLASSIFICATION DEcIMALE

''''_'__'

HJ_ï¿½IQ4loCOIil_:_'

--

BRlfXELLES
IMPRIMERIE

VEï¿½VE

FERDINAND

26-28, RUE DES

1896

MINIMES

LARCIER

,_

Les tables de la Classitication Decimals ont
besoin, en plusieurs de
parties, d'etre developpees et enrichies de divisions nouvelles.
Ce travail est commence sous la direction de l'Office

leurs

international
collaboration de plusieurs
groupes scientifiques et qui centralise pour les consacrer definitiveÂ­
ment, boutes les propositions modificatives qui lui sont faites.
C'est pour ces collahorateuns
<Iue nous avons reuni ici
de

Bibliographie qui

fait

appel

a la

quelques
quelques conseils sur la maniere de proceder au choix
nouvellles divisions, afin de maintenir entre routes Ies
parties

regles
des

a

et

de la Classification decimale
a les examiner avec soin.

une

indispensable

unite, N0US invitons

Les divisions nouvelles ne doivent etre creees
qu'en parfaite conÂ­
naissance de cause Aussi
imports-t-il de relire ce qui a ete publie
sur la Classification Decimals dans le
Bulletin de l'Institut, et dans
la preface de 1\'L Dewey a l'edition americaine.
Aucun expose theorique

valant des exemples concrets, nous
renvoyons a!lx tables amplifiees
de la Classification Decimale
qui ont ete puhliees pour la Philo sophie
et Ia Sociologie. On en verifiers
dans les diverses biblioÂ­
ne

graphies deja
philosophica,

-

I'application
Bibliographia sociologica,
BibZiographia astronomica.

parues

:

-

Bibliographia

Avant de localiser

un sujet nouveau il faut se
reporter a I'index
de la Classification Decimale et
y verifier si Ie sujet n'a
pas etc deja indexe. L'index en effet etahht les concordances entre
to utes .les
parties de la classification.

alphabetique

.

Pour

5e

ya lieu de
male telle

conformer.aux decisions de la Conference de
Bruxelles, il
ne modifier aucun des nombres
de la Classification DeciÂ­

qu'elle existe actuellement, afin de maintenir la parfaite
passes et les classements futurs.

concordance entre les classements

-6-

Une classification bibfiographique
strictement

scientifique

en ce

se

differerrcie d'une classification

qu'elle poursuit

mettre des notices

UFl

dans

but tout a fait praÂ­
un ord.J!e tel 6J1!l'il

hibhographiques
tique
so it toujours facile de les retrouver la OU on les a placees, tant pour
celui qui a fait le travail que pour t01!l:t autre qui doit le corrsalter.
Des lors,: sans etre viciee dans son principe, une classification biblioÂ­
graphique peut s'ecarter de l'ordre stricteraent scientifique. En outre,
s'il est vrai qu'une telle classification doit servir a un repertoire
ideologique, il faut cependant tenir compte, dans une certainernesuJ!e,'
que ce ne sont pas les idees elles-memes qu'jl faut classer, mais les
notices des livres et des articles de revues qui traitent de ces idees. Il
est telle idee parfaitement distincte de toute autre et qui devrait avoir
une place propre dans une classification theorique, mais a qui il est
inutile de donner nne place a part dans une olassification hibliograÂ­
phique, attendu que l'on n'a rien ou presque rien ecrit sur elle. En
d'autres termes, Ia classification hibliographique .n'est pas une pure
:

classification des sciences, mais bien cette classification dans
ports avec les notices hibliographiques (1).

Pour etaiblir

compte des
cations :

une

regles

ses

rapÂ­

classification complete,' en precede en tenant
qui sont propres a toutes les classifi-

suivantes
"

aï¿½ Densmbrement complet des objets

fJï¿½Â· iExamen

des caracteres

c) Choix d'an de

ees

a

classifier;

que presentent ces objets:
caraeteres eomme base de Ia olassification;

epeciflques

suhord,ilila,tiol!l des autres caracteres a celui-ei.
classes et sous-elasses en prpcedant du
simple au compose.
Eï¿½s difflcultes derivent de la rrruttiplicise does caracteres (;Fmi mems
@l!ljet et consequemment de Ia multiplicite des classifications posÂ­
'sibles.Pour decider du caractere pris comma base de la classification,

d1 Repartiti.on

des

objets

ge:qe'li1:l1 au paJ!ticuli@J! et

Â·il y

'1.

a
-

lieu de tenir

Les

objets

(1) Voir ce qui a
Bibli,og'l'aphie, p. 86.

en

dy

compte des observations suivantes
de connaissances sont,

ele di] a

ce

ou

:

bien des etres materiels

suilel da'ns Ie Bul'letin de l'InstÂ£tut international
.

de

-7-

appartenant au monde physique, tels des mineraux, des plantes, des
appareiis scientiflques, des langues ecrites, etc., etc., ou bien des
etres intellectuels, des idees, des concepts. Ces deux especes d'objets
peuvent etre envisages 8, deux points de vue. Au premier point de vue
on les envisage comme complete en eux-rnemes, comme autonomes,
.comme une totalite, en tant qu'une unite concrete. Au second point
de vue, on les envisage dans leurs relations avec les autres objets ou
comme parties d'une unite abstraite.
Bien qu'une classification se mette toujours au point de vue abstrait
et envisage les objets dans leurs relations les uns avec les autres, il
faut cependant tenir compte de ce fait que les deux points de vue
'

s'entrecroisent constamment.

Exemple. Les plantes sont envisagees au point de vue morpholoÂ­
gique, physiologique, economique, geographique ; un pays est enviÂ­
sage au point de vue de son climat, de sa geographic, de son admiÂ­
nistration; la biologie etudie les memes phenomenes dans toute la
serie des etres, les plantes, les animaux, l'homme.
Pour etre complete" une classification doit done denombrer if la
fois les objets et les points de vue et choisir comme fondement de la
classification, selon les
2.

cas, la serie des

uns 011

la serie des autres.

Les

sciences toutefois ont un developpement traditionnel
qui ne permet pas des distinctions aussi nettes. 11 faut en tenir
compte dans [a Bibliographie comme dans I'Enseignement, car c'est
sur un tel developpement que sont fondees les
specialites professionÂ­
-

nelles

3.

qui influent

si fort

sur

la constitution des sciences elles-rnemes.

La classification doit etre

-

envisages

dans

son

ensemble

en

merne temps que dans ses parties, car elle doit servir au repertoire
bibliographique universal. S'il s'agissait d'etahlir une classification
pour chaque hranche isolee sans tenir compte de cet ensemble, la

olassification de la
cadres

non

Chimie, par exemple, devrait com prendre dans ses
seulement la chimie pure, mais toutes les sciences auxiÂ­

liaires et annexes, la chimie appliquee, les principes generaux de la
physique, La chimie physiologique, etc. Au contraire, le repertoire

bibliographique

est

un, et

ses

diverses

parties

appui, Autant que faire se pourra, il n'y
l'exemple qu'une seule rubrique, Chimie
fois

aux

phique,

chimistes et
pour les

aura

preterit un mutuel
done, pour continuer
se

physiologique,

une

servant a Ia

seule Chimie

physiologistes,
photographes et pour les chimistes.
aux

photograÂ­

/

-'-8

a la Classification Decimale, il
deux' idees: rendre Ie systeme le plus
mnemonique possible et pour cela faire servir les memes nombres
ou parties de nombre a exprimer les memes idees; etablir
une

Dans les

developpements apportes

Iaut tenir compte

de

CgS

correspondance symetrique
en

entre les suhdivisions des diverses

reprerrant les subdivisions d'une branche

quelles de divisions a
Quant au premier point,

telles

une

autre branche,

on

en

hreuses dans la classification
9.

est indexee

formel 09

ï¿½l!lant

: at!

et

trouvera

en

des

iflJarties

les faisant servir

applications

existante.Ainsi, l'Histoire

comme

nomÂ­

science

L'histoire d'une science est marquee par l'indice
meme nomhre est attache ici La meme idee.

second

point, on vern par example que Ies developpeÂ­
philologie sont conformes a ceux de la litterature, En
botanique et en zoologie, les divisions du 081 et du 691- sont symeÂ­
triques, En medecine, il y a concordance presque ahsolue entre
I'anatomie, la physiologic, I'hygiene et la pathologie. Ex.
au

ments de la

61

Medecine_

611

Anatomic.

611.8

Anatomic dill

612
.

612.8
613
(H3.8

systeme

lPhysiol@gie.
Physiologie

dill

Hygiene.
Hygiene

systeme

du

nerveax.

systems

nerveux.

nerveux.
'

etc.

Dims eet ordre

d'idees,

il ya lieu de remarquer

:

L-

Presque TIO'l!lJtes les sciences precedent al;ldouIld'lJ.t!i par la
co'mparee : c'est Ie cas notamment l'lol!lJr la Biologic, la
Pfuilologie, le Droit. Ces sciences presentent doac des divisions fenÂ­
methode

damentales

a

communes

langue, par exemple,
phie, une phonetique.

a

preÂ£erence eette

base
sans

une

grammaire,

Dims

la

comparative

'l!l!ne

parties.

syntaxe,

une

Toute

Iexigra-

classification, il faut choisir 0.13
et

disposer

les cadres de la classifiÂ­

que
points de vue comÂ­
medification; de subdivisions a chaoun des

oatioa de tello maniere

pares servent,

to utes et a chaeune de leurs

les subdivisions des

eiements concrets. On trouvera

un

exemple
.1

dans les divisicns de la

.

-9dont 41 constitue la Philologre comparee. Toutes les
divisions au 410 reviennent, avec partiellement les memes indices,
dans les divisions de la philologie de chaque langue speciale.

Philologie (4),

etablit des developpements symetriques et concorÂ­
observer
qu'il est telle hranche qui doit etre oonsideree
dants, il faut
la
de
le
comme
matiere, de preference it telle autre qui doit
siege

2.

-

Lorsqu'on

lui demeurer subordonnee quant aux developpements. Exemple :
L'admin istration interieure d'un pays est divisee d'apres les objets de
cette administration au 351.7. Mais il existe une administration interÂ­

qui elargit tous les [ours son domaine. L'hygiene puhlique,
vagabondage, la monnaie, etc., peuvent, en effet,
etre l'objet de dispositions nationales et parallelement de disposiÂ­
tions internationales C'est pourquoi toutes les divisions du 351.7 ont
ete reprises comme divisions du 341.27, objet de l'administration
nationale

la mendicite et le

internationals. Plusieurs de CeS divisions, dans l'etat actuel du droit
international', seront sans util ite faute d'objet, mais on les laisse sans

emploi. D'autre part, on ne creera pas au 341.27 de divisions qui
n'existent pas deja au 351.7, ce dernier nombre etant considere
comme le siege principal de la matiere.
Autres exemples a com parer
351. 77 et 614; 351,83 et 331.

:'

341.2, 347

et 351. 7; 347.7 et

347;

'

Une consequence de cette regle de symetrie et de parallelismo
qu'il importe d'otablir des denombrements comple ts, alors meme
que tel1e au telle rubrique est inemployee. II suffit qu'une division
sait necessaire a quelques-unes des parties pour l'etahlir theoriqueÂ­
meat au profit de tOI1S, quitte a n'en point faire usage. Ainsi Ies diviÂ­
sions de la zoologic physiologique seront faites, une fois pour toutes,
pour tous les etres sans S8 preoccuper du point de savoir si telle ou
telle fonction existe chez tous ou chez quelques-uns. Recommencer
une subdivision speciale pour chaque espece serait eompliquer sans
necessite. De meme il n'existe qu'une serie de division pour Ia PaleonÂ­
tologie (56) d'une part, et la Botanique (D8) et la Zoologie (59)
3.

-

est

d'autre part.
4.

-

Une

preoccupation identique

doit exister

pour

l'avenir. 11 est

bien des divisions inutiles pour la bibliographie de la science d'auÂ­
deviendront utiles pour la science -de demain.
jcurd'hui, mais

qtli

,

,

Bationnellement, virtuellement divisions existent des maintenant.
II faut donc, ou bien les indiquer, ou bien construire la classification
de maniere qu'elles y trouvent aisement place dans l'avenir.

10

v
.

Voici mainrenant

quelques regles particulieres

.

.

faut introdaire dans la classification

A) Lorsqu'il
velle, on y precede

une

matiere

nouÂ­

dizaine'vestee disponibte,
B) Lorsqu'il n'existe plus de division disponible, OID. examine avec
quelle rubrique deja classes le sujet nouveau presents le plus d'anaÂ­
logie. On donne aloes a. la rubrique existante un sens plus general,
commun a l'idee
primitive et a l'idee nouvelle. On cree ensuite des
subdivisions decimales a cette rubrique.
Exemple : L'indice 341.2 etait attrihue aux traites intemationaux.
Comme une place manquait aux etudes detaillees relatives a I'adrniÂ­
'nistcation internationalo, ce nombre classificareur a rec;u la signiflÂ­
cation collective de :
Etats, terr.itoires, relations internationales,
â‚¬lID.

utilisant une

Â«

traites,

administrations internationalss

Â».

Et les divisions suivantes

opt eM creees.

341.21. Etats ou. personnes du droit international.
341.22. Territoire ou choses du droit iIlternationaI.
341.23. Droits et devoirs essentiels et reciproques des Etats.
341.24. Traites qui completent ou nwdifieflt ces deoits et
devoirs

ces

.

.

iJ}e cette maniere les tra.ites proprement dits

ne

sent

qu'une partie du 341.2.
[Autre exemple a compaeer : 332) Paupensme.]
C) Lorsqu'une matiere a plus de neï¿½lf divisions et qu'il
sible de reunir dans nne signification collective deux

plus

n'est pas posÂ·
ou

d'entr'elles, 'on
division

avec

plusieurs

elasse toutes les matieres restantes S01!lS la neuvieme
le titre Â« autres matieres Â»,
Consulter le Bulletin,
ï¿½

.,

p.

devenus

'

.

89.

J)) Lorsque
une

ees

enumeration

branches,

on:

divisions sont
et

nombreuses, qu'eltes

classification

constituent

branches tt sousÂ­
peut aussi- abaadonner la division 'decima'le en 10
non

TItle

em

pour prendre la division centesimale en 100. Cette division fournit
alors 80 rubriques nouvelles et non pas 100, car les 01, 02 a 09 et les
20, 30, 40,., 210 doiveat etre abandonnes afin fie ne pas creer de con"
fusion avec les indices formels exprimant les
ConsulÂ­
generalites.
'ter Ie eiev.eloppement de l'indice 119 dans les tables de 1:1
Philosophie
-

in Bibliographiq

.

p.hilosophica.

-11-

E) Si 1'(iHil appeecie ql!l'un sujet serait mieux classe a 1:me autre plaee,
ri
n'eIililpeche i;le Ia lud lihmner. eu creant un nouveau nombre,
ne
On
sUi['lprime pas I'ancien indice qui demeure sans qu'on en fasse
en

a l'antre et cette
usage. l\'Iais em etahlit une reference d'un nomhre
bilocation donne plus de souplesse a tout le sysMme. Eviter cepenÂ­
dant un trop grand nombre de places pour un meme sujet.

"VI
Peur arriver a faire des denombrements
ou de l'autre des mani eres suivantes.

complets,

on

pro cede de

l'une

a) On opere experim entalement en eherchant a Glasser des
biblsographiques recueillies d'avance.
0) Oro.' compile les tables methcdiques des traites geneï¿½aux.

Nches

c) OID. releve toms les mots, soit des tables analytiques de tels
traites, s0it des eID.cY010pMies techID.iques, soit d'autres bi1Jliograpihies.
tes articles de revues fraitant toujours de sujets plus speciaux que
livres, il est utile de compiler les tables genï¿½rales des revues techÂ­

les

niques.

"VII
mn des

pcincipaex Im?rites de la classification decimals est sa simÂ­
i11ilï¿½@rte de lui conserver ce caractere, surtout au point de
des comibinaisons d'indices et de l'usage partiel des tables deciÂ­

pï¿½i,ci;te.
vue

,[l

.

males.
Les combinaisons

d'indices,

teIles

qu'elles

ont ete

exposees

dans la
.

Bote

.

les

de la: Classification

sont celles de l'indice

regles
Decimate,
geographique, de l'indice historique, des indices formels et des indices
determinants (Voir Bulletin, p. 90).
L'application reflechie de ces indices est suseeptible de. -donnen a la
classification decimate une extension 'Nouvelle saID.S creel' ibeaucoup
d'inï¿½lices neuveaux. Pour ae citer OIu'un exemple, routes les
appl,iJ;aÂ­
tions sciemtifiâ‚¬[tles de Ia phot(!)graphï¿½e seront classees au nombre 778,
deterlninah:le :plar ;!;0US les inlilices specifiqueÂ». J\.iI;lsi : application de
la photographis aRX astres, 778; D2, etc.
L'adjonction de ces indices complementaires doit etre faite, autant
sur

r

,

12

-

-

possible, au radical pur du nombre classificateur.
simple et plus mnemoniqna, Exemple :
que

Finances

publiques
Id.

II faut eviter de

dire,

Finances

plus

336

en

car ce

C'est

France.

serait

336(44).

compliquer

sans

publiques

necessits

:

336

Id.

dans les divers pays

Id.

en

France.

336.1

336.1(44)

Budget

336.2.

VIII
Revenant

triques

sur

ce

nous

que

avons

dit des

developpernents symeÂ­

et

paralleles (voy. IV), insistons sur l'importance de choisir,
parmi les rubriques possibles, celles qui sont susceptibles des plus
larges developpements ulterieurs et qui permettent Ie plus facilement
to utes les combinaisons
.

On

en

trouve

.

plusieurs exemples dans

la classification' decimals

actue lie .

01

Bibliographie.
Bibliographie par pays.
016: Bibliographie sur des sujets speciaux,
Exemple : 016 : 1)2 Bibliographie de l'astronomie.

011)

IX
D'une maniere

combinaison de

generals,
ruhriques,

en

creant

il faut

se

une

rubrique

nouvelle

ou

une

representor mentalement quelle

place
.

exacte occupera, dans un repertoire, une fiche
bihliographique
indexee conformement a l'indice cree. C'est ainsi,
par exemplo, qu'il
n'est pas indifferent d'indexer la litterature en
comrne base

les nationalitss et dans

chaque

prenant
nationalite les genres, dans

chaque

-13

gerue les epoques

nalite,

les

ou en

-

epoques, puis

Litterature

XVIIIe

XIXÂ·

Theatre,

comme

les genres. So it

francaise

Poesie,

prenant

-

base, dans chaque natioÂ­

:

:

siecle,

siecle.

XVIIIe

siecle,

XIXÂ· siecle.

Eloquence,

XVIIIÂ· siecle.

XIXe

siecle.

ou

Litterature francaise ;

siecle, Poesie,
Theatre,
Eloquenee.
siecle, Poesie,
Theatre,
Eloquence.

XVIII"

XIXÂ·

Dans le

premier

epoque.
eeuvres

poesie francaise sera rennie,
division par genre primera celle

cas, .toute la

merne que tout Ie theatre et la

de

par
Dans le second cas, l'idee dominante est le groupement des
d'une meme epoque,

Dans

chaque

doit decider

donnee,

cas

quelle

particulier,

lors de la confection des

idee doit devenir

principale

et

tables,

laquelle

on

suborÂ­

