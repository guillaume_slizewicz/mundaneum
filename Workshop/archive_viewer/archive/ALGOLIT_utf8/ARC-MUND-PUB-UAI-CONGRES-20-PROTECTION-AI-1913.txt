
























                              C O I V G t t K S  M O N D I A L
                                        D E S
               A S S O C I A T I O N S  I N T E R N A T I O N A L E S  N°20
                                      G
                          Deuxième  session  : AHD-BRUXELLES,  15-13  juin  1913
                   Organisé  par  l'Union  des  Associations Internationales
                         Office  central  :  Bruxellei,  3bis,  rue  de  la  Régence  2  e  Section
               Actes  du  Congrès.  —  Documents    préliminaires.






                     La   Protection     du    Nom    et   de   l'Emblème
                           des   Associations      Internationales


                                           R A P P O R T

                                            rRKSBNTÉ  P A R
                                    M.  le  D  r  T E L L  P E R R I N
                          Avocat à  La Chaux-de-Fonds,  professeur  de  droit public
                                 à l'Université  de  Neuchâtel  (Suisse),

                   au  nom  de  l'Union  Internationale  des  Amies  de  la  Jeune  Fille


                                             [347-774]



                     L'enquête  ouverte  par  l'Union  des  Associations  Internatio-
                   nales  sur  les  questions  à  l'ordre u jour  du  Congrès,  s'étend  au
                                                 d
                   « Régime  juridique  des  Associations  internationales  »,  La  t r o i -
                   sième  question  posée  vise  les  « difficultés  spéciales  rencontrées
                   par  les  Associations  en  ce  qui  concerne  la  détermination  du
                   statut  juridique  ou  par  suite  dii  défaut  de  personnification
                   civile  ».
                                                I

                     De  récents  événements  touchant  l'Union  internationale  dés
                   Amies  de  la  Jeune  Fille  ont  mis  en  lumière  d'une  façon  très
                   caractéristique  les  lacunes  du  régime  juridique  auquel  les  Asso-
                   ciations  sont  actuellement  soumises.  Voici  les  faits  :
                     L'Union  internationale des  Amies  de  la  Jeune  Fille  (U.  I .  A.
                   J.  F.)  a  été  fondée  à  Genève  en  1877.  Son  siège  est  à  Neuchâtel,























                                                                                                                                      —  3  —

                                                                                                            de  la  Jeune  Fille  ».  Les  protégées  de  l'association,  toutes  les
                   en  Suisse.  Elle  a  pour  but  « de  former  un  réseau  de  protection                jeunes  filles  auxquelles  elle  offre  son  assistance,  la  désignent
                                                            l
                   autour  de  toute jeune  fille  appelée  à  quitter a  maison  paternelle                presqu'invariablement  sous  ce  seul  nom.
                   pour  chercher  ailleurs son  gagne-pain,  et  autant  que  possible,  de                  Cela  suffit  pour  que 'on soit  fondé  à  affirmer  que 'Union, qui
                                                                                                                                                             l
                                                                                                                                 l
                   toute  jeune  fille  isolée  ou  mal entourée,  quelles  que  puissent  être             la  première  a  usé  de  cette  désignation  comme  nom,  y  a  acquis
                   sa  nationalité,  sa  religion  et  ses  occupations  ». ,                               un  droit  exclusif  et  peut  dès  lors  s'opposer  à  toute usurpation
                     L ' U .  I .  A  .  J .  F.  étend  son  activité  à  toutes  les  parties  du         de  ces  mots  par  des  tiers.
                   monde.  Elle  est  secondée  par  des  Comités  nationaux  constitués                      L'adjonction  faite  par  l'Association  française  (Société  auto-
                   dans  les  Etats  suivants  :  Pays-Bas,  1882  ;  France,  1884  ;  Alle-               nome),  ne  saurait  suffire  à  empêcher  les  confusions  de  se  pro-
                   magne,  1885  ; Grande-Bretagne,  1886  ; Suisse,  1886  ; Italie,  1896  ;              duire,  dès  l'instant  où  les  mots  d'Amies  de  la  Jeune  Fille  sont
                   Danemark,   1911  ; Belgique,  1912.                                                     maintenus  et  forment  aussi  la partie caractéristique  de  la raison.
                      Les  autres  pays  relèvent  directement du  Comité  international.                   I l  va  de  soi  que  la  variante  d'« Amies  des  Jeunes  Filles  »  est  si
                      A u  cours  de l'année  1912,  l ' U . .  A. J .F. fut avisée  de la forma-           insignifiante  qu'elle  passera  inaperçue  de  la  plupart.
                                                  I
                   tion  d'une  association  dans  une  ville  française,  qui  prétendait                    Cette  argumentation  a  été  portée  à  la  connaissance  de  l'asso-
                   se  nommer  Association  des  Amies  des  Jeunes  Filles,  Société  auto-                ciation  intéressée  ; mais  celle-ci  n'a  pas  voulu renoncer  à  se  parer
                   nome.                                                                                    du nom d'Amies  des  Jeunes Filles.  I l paraît  vraisemblable  qu'elle
                      Cette  dernière,  bien  que  poursuivant,  paraît-il,  un  but  ana-                  spécule  sur  l'insuffisance  de  la  protection  légale  accordée  dans
                                      I
                   logue  à celui de ' U . .  A.  J . F.,  n'a  aucun lien  avec  elle.                     l'état  actuel  du  droit  au  nom  des  associations  non  commerciales
                                  l
                      L ' U .  I .  A.  J . F.  a  envisagé  que  la  raison  choisie  par  cette  nou-     et  plus  encore  sur  le  fait  que  l ' U .  I .  A.  J .  F.  n'a  pas  jusqu'ici
                                                                                                                                                          l
                   velle  société  portait  atteinte  à  ses  droits,  en  se  fondant sur  les             accompli  en  France  les  formalités  exigées  par. la oi pour  sa  re-
                    considérations  suivantes  :                                                            connaissance.
                      La  loi  n'a  pas  imposé, aux  Associations  poursuivant  un  but                      Voici  en  quels  termes  le  Conseil  juridique  de  l ' U .  I . A  J .  F.,
                    non-économique,  des  règles  strictes  pour  la  formation  de  leur                  s'est  exprimé  sur  ce  point  dans  une  consultation récente.
                    raison sociale,  comme  elle l'a fait  dans  bien des  pays, par  exemple               -  «  L'Union internationale des  Amies  de  la  Jeune  Fille  a  un  but
                    pour  les  sociétés  en nom collectif ou  en commandite. Le principe                    purement  philanthropique.  A  ce itre,  elle  ne  saurait  en principe
                                                                                                                                          t
                    du  libre  choix  a,été  admis.  I l est  donc  impossible ici, lorsqu'on  se          redouter  l'action  d'oeuvres  similaires ;  elle  doit  plutôt  se  réjouir
                    trouve  en  présence  de  deux  raisons  représentant  certaines  simi-                de  ce  que  son nitiative  ne  demeure  pas  isolée  et  trouve  des  imi-
                                                                                                                         i
                    litudes,  d'objecter  que  la oi ne  permettait pas  le  choix  d'autres               tateurs.  Dans  son  champ  d'action,  une  société  rivale  est  un
                                            l
                    raisons.
                                                                                                            auxiliaire  et  non  un concurrent.
                      La  question  de  savoir  si  deux  raisons  sociales  peuvent  coexis-                 »  S'il  y  a  donc  une  différence  essentielle  entre  des  associations
                    ter  se  ramène  à  savoir  si  la  confusion  entr'elles  est  possible.  A           de  ce genre  et  les sociétés commerciales, l faut reconnaître  d'autre
                                                                                                                                                i
                    cet  égard,  i l  saute  aux  yeux  que  toutes  les  parties  de  la  raison          part  des  analogies  très réelles entre toutes deux  en  ce  qui  concerne
                    ne  sont  pas  d'égale  importance.  Dans  chaque  cas  particulier,                   la  protection  due  à  leurs  noms  respectifs.  Toutes  sont  des  per-
                    il  faut  examiner  quels  sont  les  mots  caractéristiques  qui  ont  la             sonnes  juridiques, dont  le  patrimoine comprend  le  nom.
                    prépondérance  et mpriment  à  la  raison  un  cachet  d'originalité.                     »  La  jurisprudence moderne  de  tous  les  États  européens  tend
                                     i
                      E n  l'espèce,  c'est  incontestablement l'expression  d'« Amies  de                 à  protéger  le  nom  des  personnes  juridiques aussi  bien  que  celui
                    la  Jeune  Fille  »  qui l'emporte sur les  autres  éléments  de la  raison ;          des  personnes  physiques,  mais  les  dispositions légales  ne sont  pas
                    c'est  elle  qui  sert  essentiellement  à  identifier  l'association.  Cela            les  mêmes  selon  qu'il  s'agit  de  sociétés  commerciales  ou  non-
                    est  si rai,  que  le public connaît  et  désigne  couramment  l'œuvre                  commerciales.
                         v
                    de  l'association  et  l'association  elle-même  sous le nom  d'« Amies























                                              —  4  —                                                                                —  5  —

                      »  D'une  manière  générale  on  peut  dire  que  les  législateurs                 que  leur  opinion  soit  prédominante  en  France.  Weiss,  par  exem-
                    se  sont  occupés  de  la  protection  du  nom  commercial  beaucoup                  ple,  dans  son  Traité  de  droit  international  privé,  s'exprime
                    plus  que  de  celle  de  la  raison  de  sociétés  non-commerciales.  Le             comme   suit  :  (Tome  I I ,  p.  455)  « Nous  croyons  que  la  personne
                    droit  international  marque  la  même  prédilection.  C'est  ainsi                   »  fictive  créée  hors  de  France  n'existe  chez  nous  qu'en  vertu  de
                    notamment  que  la  Convention  de  Paris,  du  20  mars  1883,  pour                 » a  reconnaissance  expresse  ou  tacite  qui ui  est  donnée  par  le
                                                                                                            l
                                                                                                                                                   l
                    la  protection  de  la  propriété  industrielle organise  un  service  de             »  législateur  français,  et  qu'en  dehors  de  cette  reconnaissance,
                    réciprocité  entre  tous  les  États  contractants.  Cette  convention                »  elle  n'a  en  principe  aucun  droit  à  faire  valoir  devant  nos  tri-
                    stipule  notamment  à  son  article  2  que  « les  sujets  ou  citoyens              »  bunaux.  La oi  qui ui  a  donné  la  vie  n'a  aucune  autorité  au
                                                                                                                        l
                                                                                                                               l
                    de  chacun  des  Etats  contractants  jouiront,  dans  tous  les  autres              »  delà  des  limites  du  pays  qu'elle  régit.  Encore  que  la  personne
                             l
                    États  de 'Union,  en  ce  qui  concerne...  les  marques  de  fabrique               »  morale  ne  soit  souvent  qu'un  groupe  d'individus  réunis  pour
                    ou  de  commerce  et  le  nom  commercial,  des  avantages  que  les  lois            »  poursuivre  un  but  commun,  elle  ne  se  confond  pas  avec  ces
                    respectives  accordent  actuellement  ou  accorderont  par  la  suite                 )» individus ; elle  a  des  droits  et  des  obligations  qui ui sont  pro-
                                                                                                                                                          l
                    aux  nationaux.  En  conséquence,  ils  auront  la  même  protection                  »  près  ;  et  sa  capacité  collective  est  l'œuvre  de a oi.  L'intérêt
                                                                                                                                                          l
                                                                                                                                                        l
                    que  ceux-ci  et  le même  recours  légal  contre  toute atteinte  portée             »  auquel  cette  loi s'est proposée  de pourvoir par la création  d'une
                    à leurs droits... »                                                                   »  personne  morale  est  un intérêt  purement national, et l ne  peut
                                                                                                                                                             i
                      «  L'article  8  de  la  même  convention  statue  que  le  nom  com-               »  en  être  autrement, car  elle n'a  pas  qualité  pour  parler  au  nom
                    mercial  sera protégé  dans  tous  les  pays de 'Union  sans obligation               »  des  intérêts  du  monde  entier. Une  fiction  universelle,  créée  par
                                                          l
                   de  dépôt.                                                                             »  la  volonté  d'un  législateur  local,  est  une  impossibilité  juri-
                      »  IFs'agit  de  savoir  si la  raison  de  l'Association internationale            »  dique. »
                    est  un  « nom  commercial  ».                                                          »  A  noter que la volonté  des  organes de  l'Association  d'étendre
                      »  I l paraît  impossible  de l'admettre, car la caractéristique  de la             son  œuvre  dans  tous  les  États  sans rester  confinée  à la  Suisse  est
                    société  commerciale  est  la poursuite d'un but  de  lucre.                          sans  influence  ici,  car  l'association  comme  personne  morale  est
                      »  On  pourrait  peut-être  se  demander  si,  dans  la  mesura  où                 une création non de  volontés individuelles mais  de la  loi.
                   l'Union  internationale  édite  des  brochures,  affiches,  etc.,  elle                  »  I l serait  donc  nécessaire,  pour  obtenir  une  protection  effec-
                   n'accomplit  pas  des  actes  commerciaux.  Mais  ces  imprimés  sont                  tive  en  France  et  revendiquer  devant  les  tribunaux  l'exclusivité
                   distribués  gratuitement ;  c'est  un  moyen  de  propagande  d'où                     de la raison de 'Union  internationale des  Amies  de la  Jeune  Fille,
                                                                                                                        l
                   tout  calcul  intéressé  est  exempt  ;  enfin,  c'est  là  un  côté  très             de  satisfaire  aux  obligations  imposées  aux  associations  pour leur
                   secondaire  de  l'activité  déployée  par  l'Association.  Le  but  phi-               reconnaissance.  D'après  le  droit  français,  spécialement  d'après
                   lanthropique  restera  toujours  au  premier  plan.                                    la loi sur le contrat d'association  du  I  E R  juillet  1901,  deux  moyens
                     »  I l  n'existe  aucune  convention  internationale  prévoyant  la                  s'offrent  : la  déclaration  préalable  à la  Préfecture  du  département
                   protection  du  droit  à  la  raison non  commerciale.  Cela  s'explique               où  l'Association  a  son  siège,  ou a  reconnaissance  d'utilité  pu-
                                                                                                                                         l
                                                                                                                                        l
                   parce  que  les  conflits relatifs au  nom  d'associations  sans  but  éco-            blique  par  décret  à  solliciter de 'Administration.
                   nomique  sont  rarissimes.                                                               »  Tant  que  l'une  ou  l'autre  de  ces  formalités  n'aura  pas  été
                     »  Les  auteurs  de  droit  international  privé  ne  sont  pas  d'accord            accomplie,  l'association  sera  considérée  comme  clandestine  ;  elle
                   sur  la  protection qiie 'on  doit  accorder  aux  personnes  morales                  ne  sera  pas  personne  morale  et  ne  pourra  par  conséquent  ester
                                        l
                   à  l'étranger.  Certains  enseignent  que  celles-ci  peuvent  pro-                    en  justice.
                                                                                                                      l
                   longer  leur  activité  et  exercer  leurs  droits sur  le  territoire  d'au-            »  Une fois 'Union  internationale reconnue  en France,  sa  raison
                   tres  Etats,  sans  avoir  été  reconnues  ou  autorisées  par  eux.                   sera  selon  toute  vraisemblance  protégée  dans  ce  pays  en  vertu
                   D'autres  refusent  leur  adhésion  à  ce  système  et  i l  semble  bien              des  mêmes  principes  qui  sont  appliqués  en  Suisse.























                                               —   6  —
                                                                                                                                     —  7  —
                        »  I l y  a  cependant  encore  un  écueil.  Si 'Union internationale
                                                             l
                                                                                                                                                             l
                      n'acquérait  la  personnalité  juridique  en  France  que  postérieure-              durer  autant  qu'elle  ; le  protéger  quelque  temps,  puis ui  couper
                      ment  à  la  constitution  d'une  association  indépendante  d'elle,                 ensuite  la  protection  légale,  serait  absurde  ;  cela  équivaudrait
                                                                                                            l
                      reconnue  dans  ce  pays,  dont  la  raison  comprendrait  les  mots                 à imiter  la  protection du nom  à  une  certaine  durée.
                      « d'Amies  de  la  Jeune  Fille  », on pourrait  se  demander  comment                 S'il  s'agissait  de  questions  commerciales,  l'emblème  serait
                                                            I
                      le  conflit  serait  résolu  par  les  tribunaux. l paraîtrait  assez con-           protégé  par  les  principes  régissant  la  concurrence  déloyale.
                      forme  aux  tendances  nationalistes  de  la  jurisprudence  française,              Faute  de  but  de  lucre, nous  sommes désarmés  !
                                                                                                                  I
                      d'admettre  que  la priorité  d'usage de la raison sociale  à  l'étranger              L'U. .  A.  J.  F.,  par  exemple,  utilise  comme  emblème  l'étoile
                      doit  s'effacer  devant  la  priorité  d'usage  en  France,  cette  dernière         à  sept  rayons.  Les  agentes  sont  munies  de  cet  insigne  qui  sert
                      étant  seule  prise  en  considération. »                                            à  les  faire  reconnaître  de  celles  qui  désirent  recourir  à  leurs  ser-
                                                                                                           vices.  Si  des  tiers poursuivant  des  visées  immorales  se  parent  de
                                                            d
                        Cet  exposé  démontre  que la protection u nom des  associations                   cet  emblème,  on  voit  d'emblée  les  terribles mécomptes  qui  peu-
                      est  dans  le  domaine  international  des  plus  précaire.  Faute  d'ob-            vent  en  résulter  pour  les  victimes.  Néanmoins,  cet  emblème  est
                      server  dans  certains  Etats  les  conditions  de  forme,  parfois  coû-            dénué  de protection légale. Tous  les  moyens  tentés pour l'obtenir,
                      teuses ou difficilement réalisables,  exigées  pour la  reconnaissance               tels  que  dépôt  comme  marque  de  fabrique, adjonction à la raison
                      d'une  association,  celle-ci  ne  peut  revendiquer  le  droit  exclusif            des  mots  « nion  avec l'étoile  à sept  rayons  » sont  insuffisants,  de
                                                                                                                     U
                      à  son nom.  E t  si une  autre  association  usurpe  sa  raison  en  ayant          l'aveu  des  juristes,  toujours  parce  que  les  lois  dont  nous  cher-
                      soin  de  se  conformer  aux  conditions  de  forme  requises  pour  la              chons  à  obtenir  le  bénéfice  ne  visent  pas  les  associations  non
                      reconnaissance  des  associations,  i l  est  à  craindre  que  la  spolia-          commerciales.
                      trice ne triomphe et ne parvienne même à interdire dans  ces  mêmes                    Quelque  utile  que  soit  le  port  d'un  signe  distinctif  par  les
                      États  l'usage  de  la  raison  à  celle  qui  en  est  en  fait  le  véritable      agents  de ' U .  I . A.  J .  F.,  on peut  se  demander  s'il  ne  serait  pas
                                                                                                                    l
                      ayant-droit.                                                                         préférable  d'y  renoncer,  à  cause  des  abus  possibles,  contre
                        L'intérêt  primordial  à  obtenir  une  protection  effective  de  la              lesquels  nous  sommes  sans moyens  d'action.
                                                           l
                                                                         l
                      raison  est  incontestable.  Dans  le  cas  de ' U . .  A.  J .  F., a  confu-
                                                               I
                                                     l
                      sion  que  crée  la  contrefaçon  ou 'imitation  de  la  raison  entraî-
                      nerait  les  plus  funestes  conséquences  si  elle  était  exploitée  dans                                      I I I
                      un  but  peu  honorable.                                                               L'exposé  qui précède  permet  de  conclure  à la nécessité  de nou-
                                                                                                           velles  normes juridiques.
                                                   I I                                                       I l  est  impossible  d'aborder  ici le  problème  de  la  situation uri-
                                                                                                                                                                   j
                        Une  question  liée  à  celle  du  nom  est  celle  de  la  protection  de         dique  des  associations  et  de  leurs  emblèmes  dans  chaque  pays
                      l'emblème  choisi  par  une  association.                                            de  lege  lata  et  surtout  de  lege  ferenda.  Chaque  Etat  doit  adapter
                        Actuellement,  ces  emblèmes  ne  sont  pas  protégés.  Ils  ne  peu-              à  son système  légal  les  règles  nouvelles  nécessitées  par  l'évolution
                      vent  se  réclamer  ni  des  règles  concernant  la  propriété artistique,           de  ces  organismes  trop  méconnus  jusqu'ici.
                      ni  de  celles  sur  les  marques,  n i de  celles  sur  le nom. a  seule  pro-        Tout  ce  que  l'on peut  dire pour  rester  dans  le  cadre  de  généra-
                                                                      L
                      tection  possible  serait  éventuellement  celle  des  dessins  et  mo-              lités  que  comporte  le  présent  rapport  c'est  qu'une  analogie  de
                      delés  industriels ;  mais  cette  protection  limitée  à  quelques                  fait  existe  d'une  part  entre  les  raisons  des  sociétés  commerciales
                      années  (15  ans  en  général),  est  insuffisante  et  ne  répond  pas  au          et  celles  des  associations,  d'autre  part  entre  les  marques  de
                      but.  L'emblème  qui  symbolise  une  association  et  rappelle  à  cer-             fabrique  et  de  commerce  et  les  emblèmes.  Cette  ressemblance
                      tains  égards  l'enseigne  d'un  établissement,  doit  normalement                   de  fait  permettrait de  résoudre juridiquement  la question de  pro-
                                                                                                           tection  légale  par  des  moyens  analogues  à  ceux  employés  depuis
                                                                                                           longtemps  dans  le domaine commercial.

























                                                  —  8  —
                                                                                                                                      —   9  —
                         A u  point  de  vue  national,  i l 'y  a  pas  de  difficulté  particulière
                                                    n
                       à  vaincre, pourvu que  le législateur  consente  à entreprendre  cette                 L'avant-projet  de  convention  internationale  présenté  au
                       tâche.  I l  suffirait  d'élaborer  une  loi  assimilant  les  associations          Congrès  et  publié  dans  la  revue  La  Vie  Internationale,  t. , I  p.  496
                                                                                                                                                       i
                       aux  sociétés  commerciales  au  point  de  vue  de  la  protection  du              et  suivantes,  est  très  intéressant  à  cet  égard  ; l suggère  une solu-
                       nom et de traiter les emblèmes comme les  marques,  ou  éventuelle-                   tion  qui,  dans  ses  grandes  lignes,  pourrait  être  adoptée.
                       ment  comme  les  enseignes.  I l appartient  à  chaque  pays  de  trou-
                       ver  une  solution correspondant  à  ses  institutions  juridiques  déjà
                       existantes.  Cette  question  ne  se  rattache  qu'indirectement  au                                        CONCLUSIONS
                       programme'  du  Congrès.
                         Dans  le  domaine  international  par  contre,  i l  nous  appartient                 Pour  tenir  compte  des  considérations  émises  au  cours  de  ce
                       de  formuler  des  propositions précises.  La  question  est  très  diffé-           rapport,  nous  proposons  d'apporter  à  cet  avant-projet  les  modi-
                       rente  selon  que  la  création  du  droit  international précède  ou                 fications  suivantes,  en  y  ajoutant  :
                       suit  celle  du  droit  national.                                                      Ad  ART.  I V ,  2  e  alinéa.  —  Le  nom  de  ces  Associations  interna-
                         Si  au  moment  de  l'apparition  de  règles  internationales  la                  tionales  sera  protégé  dans  tous  les  Etats  contractants  qui  s'engagent
                       situation  a  déjà  reçu  une  solution satisfaisante  au  sein  des  Etats          à  leur  appliquer  les  règles  de  leur  droit  national,  ou,  à  défaut  de
                       intéressés,  le  problème  est  simplifié.  L'infrastructure  existant               dispositions  légales  sur  la  matière,  à  leur  faire  application  par
                       déjà,  le principe formulé  par l'article 8 de  la Convention  de  Paris             analogie  des  dispositions  légales  réglant  la  protection  du  nom  com-
                       de  1883  relativement  au  nom  commercial  suffit.  On  pourrait,                  mercial,  notamment  en  ce  qui  concerne  les  sanctions  civiles.
                       par  exemple,  libeller  la  disposition suivante  :                                   3  e  alinéa. —  L'emblème  régulièrement  enregistré  sera  protégé  dans
                                                                                                            tous  les  Etats  contractants  qui  y  appliqueront  leur  droit  national,
                         «  Le  nom  des  associations  protégées  dans  leur  pays  d'origine
                       sera  protégé  dans  tous  les  pays  de 'Union,  sans  obligation  de               ou  à  défaut  de  dispositions  légales  sur  la  matière  appliqueront  par
                                                         l
                       dépôt.  I l  en  sera  de  même  de  l'emblème  symbolisant  une asso-               analogie  les  règles  et  sanctions  civiles  prévues  pour  la  protection
                       ciation.  Pour  les  associations  internationales le  pays  où  elles ont           des  marques  de  fabrique  et  de  commerce.  Le  droit  à  l'emblème  s'ac-
                       leur  siège  central  est  considéré  comme  pays  d'origine.  »                     quiert  par  Venregistrement  au  bureau  international,  conformément
                                                                                                            aux  prescriptions  de  l'article  V.
                         On  pourrait  aussi,  pour  les  emblèmes,  créer  un  office  central
                                                                                                                           0
                       de  dépôt,  analogue  à  celui  qui existe pour l'enregistrement inter-                Ad  ART.  V,  4 .  —  L'emblème  adopté,  qui  sera  inscrit  dans  un
                       national  des  marques  en  vertu  de  l'Arrangement  de  Madrid  du                 registre  ad  hoc  et  dont  un  certificat  d'enregistrement  sera  délivré
                       14  avril  1891.                                                                     à  Vassociation  titulaire.
                         Mais  si  le  droit  international  ne  peut  se  superposer  à  un  droit
                       national  parce  que  ce  dernier  est  encore  inexistant,  l'organisa-
                       tion  d'une  protection  efficace  se  complique ;  elle  est  à  créer  de
                       toutes  pièces.
                         Cette  hypothèse  ne  saurait  être  négligée,  car l est  fort  possible
                                                                  i
                       que  dans  certains  pays,  l'évolution  nationale  soit  plus  lente  que
                       le  développement  international.  Pareil phénomène  n'est  pas  sans
                       précédent.  C'est  ainsi,  par  exemple,  que  la  Suisse  adhéra  à  la
                       Convention  de  Paris  de  1883,  où  naquit 'Union  pour  la  protec-
                                                             l
                       tion  de  la  propriété  industrielle,  avec  Bureau  à  Berne,  alors
                       qu'elle ne possédait  encore  aucune oi sur les  brevets  d'invention.
                                                      l

