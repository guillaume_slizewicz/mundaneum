import bpy

#bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

tmpl_name = 'template_txt'
txt_name = 'generated_txt'
txt_content = 'Some text with break\nnnbb line2'


template_obj = bpy.data.objects[tmpl_name]

previous_objs = []
for o in bpy.data.objects:
    previous_objs.append( o.name )

print( previous_objs )

def new_obj():
    out = None
    for o in bpy.data.objects:
        found = False
        for po in previous_objs:
            if o.name == po:
                found = True
        if not found:
            print( 'not found: ' +o.name )
            out = o
            previous_objs.append( o.name )
    return out

def generate_txt( txt ):
    
    bpy.ops.object.select_all(action='DESELECT')
    template_obj.select = True
    
    bpy.ops.object.duplicate()
    
    obj = new_obj()
    obj.location = (0,0,0)
    obj.name = txt_name
    obj.data.name = txt_name
    obj.data.body = txt

generate_txt( txt_content )