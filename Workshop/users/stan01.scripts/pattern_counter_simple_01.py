# from pattern.fr import parsetree
from pattern.en import parsetree
from collections import Counter
import re


def nonStopWord (word):
	stopWords = ['the', 'on', '.', 'a']

	return word not in stopWords





#def count (key, counter, filter=None):
	#if filter(key):
		#if key not in counter:
       		#counter[key] = 1
    	#else:
      		#counter[key] += 1

counter = Counter()

with open('input/cat.txt', 'r') as h:
# Open a file
  raw_text = h.read()
  # Read a file, store it in raw text
  parsed_text = parsetree(raw_text, lemmata=True)
  # Process the text, turn it into a list of sentences,
  # these are in fact lists of words
  
  for sentence in parsed_text.sentences:
    for word in sentence.words:
      if word.string not in counter:
        counter[word.string] = 1
      else:
        counter[word.string] += 1

print('*********')
print('3 Most Common:')
print(counter.most_common(3), '\n')
print(counter)

