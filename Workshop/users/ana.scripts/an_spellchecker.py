from spellchecker import SpellChecker

spell = SpellChecker( language ='fr')



spell['morning']  # True
# 'morning' in spell  # True

# # find those words from a list of words that are found in the dictionary
# spell.known(['morning', 'hapenning'])  # {'morning'}

# # find those words from a list of words that are not found in the dictionary
# spell.unknown(['morning', 'hapenning'])  # {'hapenning'}

# misspelled = spell.unknown(['morning', 'hapenning'])  # {'hapenning'}
# for word in misspelled:
#     spell.correction(word)  # 'happening'

# for word in misspelled:
#     spell.correction(word)  # {'penning', 'happening', 'henning'}