#!/usr/bin/env/ python

# This script creates a sorted frequency dictionary with stopwords
# The output is printed in a txt-file and in a Logbook in Context

 #    Copyright (C) 2016 Constant, Algolit, An Mertens

 #    This program is free software: you can redistribute it and/or modify
 #    it under the terms of the GNU General Public License as published by
 #    the Free Software Foundation, either version 3 of the License, or
 #    (at your option) any later version.

 #    This program is distributed in the hope that it will be useful,
 #    but WITHOUT ANY WARRANTY; without even the implied warranty of
 #    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #    GNU General Public License for more details: <http://www.gnu.org/licenses/>.



from __future__ import division
from collections import Counter
import string
import nltk



'''
This script creates a frequency dictionary of words used in a text filtering out stopwords
'''

# VARIABLES


# textfiles
source = open('../input/frankenstein.txt', 'rt')
#destination = open('whywastheseastormy_freq.txt', 'wt')

# other options
freqwords = ["the", "a", "to", "of", "in", 'is', "with", "on", "for", "at", "from", "about",\
 "are", "an", "up", "out", "have", "be", "this", "one", "says", "as", "all", "just", "was", "so", "there", "not", "by",\
 "into", "been", "dont", "has", "over", "doesnt", "did", "had", "would", "could", "didnt"]
relationals = ["she", "you", "i", "he", "we", "her", "his", "it", "its", "their", "me", "our", 'they', "us", "my",\
"your", "theyre", 'them', "youre", "him", "were", "these"]
subphrases = ["and", "that", "but", "like", "what", "if", "then","theres", "or", "which", "who", "while", "where", "when",\
"thats", "how", "because"]

# chosen option, removed "she", "you", "i", "he", "we"
stopwords = ["the", "a", "to", "of", "in", 'is', "with", "on", "for", "at", "from", "about",\
 "are", "an", "up", "out", "have", "be", "this", "one", "says", "as", "all", "just", "was", "so",\
 "her", "his", "it", "its", "their", "me", "our",\
 "and", "that", "but", "like", "what", "if", "then", "there", "they", "us", "my", "your", "theres", "theyre", "or", "not",\
 "which", "by", "who", "them", "into", "while", "been", "dont", "where", "youre", "has", "when", "over", "him", "were", "doesnt",\
 "did", "thats", "how", "had", "these", "would", "could", "because", "didnt"]


## FUNCTIONS

# PREPROCESSING TEXT FILE
## remove caps + breaks + punctuation
def remove_punct(f):
	tokens = (' '.join(line.replace('\n', '') for line in f)).lower()
	for c in string.punctuation:
		tokens= tokens.replace(c,"")
		tokens = tokens.strip()
		#print("tokens", type(tokens))
	return tokens

# remove stopwords
def remove_stopwords(tokens):
	tokens = tokens.split(" ")
	words =[]
	for token in tokens:
		if token not in stopwords:
			words.append(token)
	return words

## create frequency dictionary 
def freq_dict(words):
	frequency_d = {}
	# tokens = tokens.split(" ")
	for word in words:
		try:
			frequency_d[word] += 1
		except KeyError:
			frequency_d[word] = 1
	return frequency_d

## sort words by frequency (import module)
def sort_dict(frequency_d):
	c=Counter(frequency_d)
	frequency = c.most_common()
	return frequency

def most_common(frequency_d, amount=10):
	c=Counter(frequency_d)
	return c.most_common(amount)

# write words in text file 
def write_to_file(frequency, destination):
	for key, value in frequency:
		destination.write(("{} : {} \n".format(key, value)))
	destination.close()




## SCRIPT


# execute functions
tokens = remove_punct(source)
print("remove punct:", tokens)

words = remove_stopwords(tokens)
print("remove stopwords:", words)

frequency_d = freq_dict(words)
#print("frequency_d:", frequency_d)

#frequency = sort_dict(frequency_d)
#print("sorted_freq_d:", frequency)

#write_to_file(frequency, destination)

print(most_common(frequency_d, 10))

source.close()

#destination.close()


