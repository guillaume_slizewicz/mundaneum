E  XAMEN 
DU
 PROJETDELA 

Société Royale de Londres concernant le Catalogue
International des Sciences
Le Bulletin a tenu ses lecteurs régulièrement au courant des projets
bibliographiques de la Société Royale de Londres Ainsi qu'on a pu
le lire dans le compte rendu, la Conférence de 1898 a confié, à un
comité international, le soin d'établir un nouveau projet d'organisa-

tion du catalogue, tenant compte de toutes les observations sollicitées
des comités locaux. Sur un grand nombre de points, en effet, la
Observations présentées a u point de  vue  de l a méthode
Conférence avait dû se déclarer insuffisamment instruite, et l'avant-
bibliographique
projet du comité de la Société Royale n'avait pas paru pouvoir être
par le Comité de Direction de l'Institut International de Bibliographie
adopté sans plus ample examen. Les observations publiées ici forment
une contribution à l'examen critique de cet avant-projet.

I. — REMARQUES PRÉLIMINAIRES.
Dans les notes qui vont suivre, on a exposé une série d'observations
nouvelles ou déjà formulées ailleurs, sur le projet rédigé par le comité
de la Société royale, pour l'élaboration du Catalogue international des
sciences. Ces observations sont de portée et de valeur inégales. En les
produisant sous une forme méthodique, on a cherché à mettre un peu
d'ordre dans la discussion d'une matière aussi complexe et aussi
étendue.
Caractères généraux du projet de la Société Royale. — La Société
Royale a cru devoir procéder comme si elle était en présence d'une

tabula rasa, sur laquelle l dépendait exclusivement de la force de ses
(i)V. Bulletin, I, p. 5o, 158, programme, , I p. 107-112, 182, 188, compte rendu,
I, 277; Conférence, 1896, Acta, I , p.  281292  ; Conférence, 1898, Résumé, I I , raisonnements d'y inscrire tout ce qu'elle voulait. Aucune préoccu-
I
p. 159-164.
pation de rattacher le travail nouveau aux travaux anciens, d'uti-