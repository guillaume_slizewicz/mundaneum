from . import settings

def debug (text):
  if settings.DEBUG:
    print("Debug : {}".format(text))

def log (text):
  print("Log   : {}".format(text))

def error (text):
  print("Error : {}".format(text))