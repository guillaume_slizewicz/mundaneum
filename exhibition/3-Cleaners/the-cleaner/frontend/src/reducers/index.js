import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import application from './application';
import document from './document';
import lines from './lines';
// Import reducers

export default function createRootReducer(history: {}) {
  const routerReducer = connectRouter(history);
  // Perhaps remove this last call / fat arrow

  return connectRouter(history)(
    combineReducers({
      router: routerReducer,
      application,
      document,
      lines
      /* , other reducers */ 
    })
  );
}
