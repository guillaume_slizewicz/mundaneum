import { simpleUpdate } from '../utils';
import { DOCUMENT_LOAD_SUCCESS } from '../actions/document';

const initialState = '';

export default function document (state = initialState, action) {
  switch (action.type) {
    case DOCUMENT_LOAD_SUCCESS:
      return action.document;

    default:
      return state;
  }
}