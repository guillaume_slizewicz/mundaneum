import {
  documentGet as apiDocumentGet,
  documentPost as apiDocumentPost
} from '../services/api';

export const DOCUMENT_LOAD = "DOCUMENT_LOAD";
export const DOCUMENT_LOAD_FAIL = "DOCUMENT_LOAD_FAIL";
export const DOCUMENT_LOAD_SUCCESS = "DOCUMENT_LOAD_SUCCESS";

export const DOCUMENT_STORE = "DOCUMENT_STORE";
export const DOCUMENT_STORE_FAIL = "DOCUMENT_STORE_FAIL";
export const DOCUMENT_STORE_SUCCESS = "DOCUMENT_STORE_SUCCESS";


export const documentLoad = () => (dispatch, _) => {
  dispatch({ type: DOCUMENT_LOAD });
  apiDocumentGet()
    .then((data) => {console.log(data); dispatch(documentLoadSuccess(data)) })
    .catch(() => dispatch(documentLoadFail()))
} 

export const documentLoadFail = () => ({ type: DOCUMENT_LOAD_FAIL });

export const documentLoadSuccess = ({ document, lines }) => ({
  type: DOCUMENT_LOAD_SUCCESS,
  document, lines
});

export const documentStore = () => (dispatch, getState) => {
  const { document, lines } = getState();
  dispatch({ type: DOCUMENT_STORE });
  apiDocumentPost(document, lines.lines)
    .then(() => dispatch(documentStoreSuccess()))
    .catch(() => dispatch(documentStoreFail()))
};

export const documentStoreFail = () => ({
  type: DOCUMENT_STORE_FAIL,
});

export const documentStoreSuccess = () => ({
  type: DOCUMENT_STORE_SUCCESS,
});