// const API_URL = "http://localhost:5000/the-cleaner/api/";
const API_URL = "/the-cleaner/api/";

export const api_url = (tail) => {
  return `${API_URL}${tail}`;
}

const get = (url) => {
  const promise = new Promise((resolve, reject) => {
    fetch(api_url(url), {
      method: "GET"
    }).then((response) => {
      if (response.ok) {
          response.json()
          .then(data => resolve(data))
          .catch(reject);
      }
      else {
        reject();
      }
    }).catch(reject);
  });

  return promise;
}


const _post = (url, data) => {
  const promise = new Promise((resolve, reject) => {
    const postData = new FormData();

    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        if (Array.isArray(data[key])) {
          data[key].forEach((v) => postData.append(key, v));
        } else {
          postData.append(key, data[key]);
        }
      }
    }

    fetch(api_url(url), {
      method: "POST",
      body: postData
    }).then(resolve).catch(reject);
  });

  return promise;
}

// TODO generalize API call function
const post = (url, data) => {
  const promise = new Promise((resolve, reject) => {
    _post(url, data).then((response) => {
      if (response.ok) {
        response.json()
          .then((data) => resolve(data))
          .catch(reject);
      } else {
        reject();
      }
    }).catch(reject);
  });

  return promise;
}

const external_post = (url, data) => {
  const promise = new Promise((resolve, reject) => {
    let postData = new FormData();

    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        postData.append(key, data[key]);
      }
    }

    fetch(url, {
      method: "POST",
      body: postData
    }).then((response) => {
      if (response.ok) {
        response.json()
          .then((data) => resolve(data))
          .catch(reject);
      } else {
        reject();
      }
    }).catch(reject);
  });

  return promise;
}


export const documentGet = () => get('document')

export const documentPost = (document, lines) => post('update_document', { document, lines: JSON.stringify(lines) })

export const linePost = (document, line, sentence, clean ) => post('update_line', { document, line, sentence, clean })

export const poemGet = () => get('get_poem')

export const print = (text) => external_post('http://cleaner.local/print', { text: text })

export default { documentGet, documentPost, linePost, poemGet, print }