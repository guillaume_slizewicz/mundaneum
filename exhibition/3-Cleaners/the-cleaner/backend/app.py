from flask import Flask
from flask import Flask, jsonify, request
from classification_utils import db, clean_states
import json
import random

baseurl = '/the-cleaner/api'

app = Flask(__name__)

@app.route("{}/document".format(baseurl), methods=["GET"])
def get_document ():
  conn = db.connect()
  rows = db.get_document(conn)

  # print(rows[0])

  if rows:
    return jsonify({
      'document': rows[0]['document'],
      'lines': [{
        'document': r['document'],
        'line': r['line'],
        'sentence': r['sentence'],
        'clean': r['clean'],
        'clean_annotation': r['clean_annotation'],
        'checked': r['checked'],
      } for r in rows]
    })
  else:
    return jsonify("Error")

@app.route("{}/update_sentence".format(baseurl), methods=["POST"])
def update_sentence():
  conn = db.connect()
  document = request.form['document']
  line = request.form['line']
  sentence = request.form['sentence']
  clean = request.form['clean']

  if db.update_sentence(conn, document, line, sentence, clean):
    return jsonify("Done")
  else:
    return jsonify("Error")

@app.route("{}/update_document".format(baseurl), methods=["POST"])
def update_document():
  conn = db.connect()

  document = request.form['document']
  lines = json.loads(request.form['lines'])

  # print(document, lines)
  # return jsonify("?")
  if db.update_document(conn, document, lines):
    return jsonify("Done")
  else:
    return jsonify("Error")

def get_start_line (conn):
  sentence = db.get_poem_sentence(conn)

  if sentence['sentence']:
    return sentence
  else:
    return get_start_line(conn)


@app.route("{}/get_poem".format(baseurl), methods=["GET"])
def get_poem():
  conn = db.connect()
  sentences = []
  target = random.randint(2,4) * 3
  start = get_start_line(conn)
  
  while len(sentences) < target:
    sentence = db.get_sentence_at_line(conn, start['line'])
    
    if sentence['clean_annotation'] != clean_states.EMPTY:
      sentences.append(sentence['sentence'])

  return jsonify({ 'text': '\n'.join(sentences) })

@app.after_request
def add_headers(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers',
                        'Content-Type,Authorization')

  return response