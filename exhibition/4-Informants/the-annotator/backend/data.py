import glob
import os.path
import settings
import random
import re
import csv

documents = glob.glob(os.path.join(settings.datadir, '*.txt'))

def pick_sentence_from_tree (tree):
  sentence = ''

  while not sentence:
    sentence = random.choice(tree).string

  return sentence

def pick_sentence():
  path = random.choice(documents)

  with open(path, 'r') as h:
    text = re.sub(r'\n(\S)', ' \\1', re.sub(r'-\n(\S)', '\\1', h.read(), flags=re.M), flags=re.M)
    # print(text)
    tree = parsetree(text, tokenize = False, tags = False, chunks = False)

    return (0, pick_sentence_from_tree(tree))


def transform ():
  