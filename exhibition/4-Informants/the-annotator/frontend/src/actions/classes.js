import { classesGet } from '../services/api';

export const CLASSES_LOAD = 'CLASSES_LOAD';
export const CLASSES_LOAD_FAIL = "CLASSES_LOAD_FAIL";
export const CLASSES_LOAD_SUCCESS = "CLASSES_LOAD_SUCCESS";

export const classesLoad = () => (dispatch, getState) => {
  dispatch({ type: CLASSES_LOAD })
  classesGet()
    .then(classes => dispatch(classesLoadSucces(classes)))
    .catch(() => dispatch(classesLoadFail()))
}

export const classesLoadFail = () => ({
  type: CLASSES_LOAD_FAIL,
});

export const classesLoadSucces = (classes) => ({
  type: CLASSES_LOAD_SUCCESS,
  classes: classes
})