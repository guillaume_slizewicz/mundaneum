import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Annotator from './containers/Annotator';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Route exact path="/" component={ Annotator } />
        </Router>
      </div>
    );
  }
}

export default App;
