import React from 'react';

import { t } from '../utils'

export const SentenceActions = ({
  sentenceClassSelected,
  sentenceLoading,
  sentenceLoad,
  sentencePost,
  sentenceMarkDirty
}) => (
  <section class="sentence--actions">
    <button onClick={ sentenceMarkDirty } disabled={ sentenceLoading }>{ t('mark_dirty_button') }</button>
    <button onClick={ sentenceLoad } disabled={ sentenceLoading }>{ t('sentence_load_new_button') }</button>
    { (sentenceClassSelected !== null) ? <button onClick={ sentencePost } disabled={ sentenceLoading }>{ t('sentence_submit_button') }</button> : '' }
  </section>
)

export default SentenceActions;