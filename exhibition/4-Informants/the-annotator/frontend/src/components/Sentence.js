import React from 'react';

export const Sentence = ({
  sentence,
  before,
  after
}) => (
  <section className="sentence">
    { before.map(sentence => <p className="sentence--window">{ sentence }</p>) }
    <p>{ sentence }</p>
    { after.map(sentence => <p className="sentence--window">{ sentence }</p>) }
  </section>
);

export default Sentence;