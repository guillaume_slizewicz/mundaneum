export const messages = {
  'classes_load': 'Loading annotation classes',
  'classes_load_fail': 'Could not retreive classes',
  'sentence_load': 'Loading a new sentence to annotate',
  'sentence_load_fail': 'Could not load a new sentence',
  'sentence_load_new_button': 'Load another sentence',
  'sentence_submit_button': 'Submit Annotation',
  'mark_dirty_button': 'This sentence should be cleaned',
  'sentence_mark_dirty': 'Scheduling sentence to be cleaned',
  'sentence_mark_dirty_fail': 'Unable to send feedback to the server',
  'sentence_mark_dirty_success': 'Feedback sent to the server',
  'sentence_post': 'Sending annotation',
  'sentence_post_fail': 'Unable to send annotation',
  'sentence_post_success': 'Sent annotation to the server',
  'select_class_explanation': 'Select a class for this sentence'
}

export default messages;