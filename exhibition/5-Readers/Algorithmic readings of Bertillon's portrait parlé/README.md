# Algorithmic readings of Bertillon's portrait parlé

This work consists of 2 parts

Writing with Otlet:  is a character generator that uses the spoken portrait code as its database. Random numbers are generated and translated into a set of features. By creating unique instances, the algorithm reveals the richness of the description that is possible with the portrait code while at the same time embodying its nuances.
Data-workers-algorithmic-readings-portrait-parle-installation-view-2.jpg

An interpretation of Bertillon's spoken portrait: draws a parallel between Bertillon systems and current ones. A webcam linked to a facial recognition algorithm captures the beholder's face and translates it into numbers on a canvas, displaying it alongside Bertillon's labelled faces. 

## Installation note
add to /home/pi/.bashrc
```
if [ $(tty) == /dev/tty1 ]; then 
  bash /home/pi/Documents/mundaneum/exhibition/growing_a_tree/growing_a_tree.sh
fi
```

# Authors
Guillaume Slizewicz 

# License
Copyright (C) Guillaume Slizewicz, Brussels, 2019
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details: <http://www.gnu.org/licenses/>.

