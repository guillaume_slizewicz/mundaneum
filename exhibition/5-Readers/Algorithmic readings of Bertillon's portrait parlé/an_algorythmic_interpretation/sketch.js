//Sketch made for Algolit's data worker exhibition by Guillaume Slizewicz.
//Based on https://kylemcdonald.github.io/cv-examples/

var capture;
var tracker
var w = 640,
    h = 480;
var xoff = 0.0;
var yoff = 0.0;


function setup() {
    capture = createCapture({
        audio: false,
        video: {
            width: w,
            height: h
        }
    }, function() {
        console.log('capture ready.')
    });
    capture.elt.setAttribute('playsinline', '');
    createCanvas(w, h);
    capture.size(w, h);
    capture.hide();


    tracker = new clm.tracker();
    tracker.init();
    tracker.start(capture.elt);
}

function draw() {
	background(255, 255, 255, 100);
	//image(capture, 0, 0, w, h);

    
  	xoff = xoff + 0.01;
  	yoff = yoff + 0.02;
	positions = tracker.getCurrentPosition();
    parameters = tracker.getCurrentParameters();
		
  
	strokeWeight(1);
  
    push();
    for (var i=0; i<positions.length; i++) {
    fill(255,100,100);
    text(i, positions[i][0], positions[i][1]);
    }
    pop(); 

  

    stroke(map(noise(xoff), 0, 1, 0, 255), map(noise(yoff), 0, 1, 0, 255), map(noise((xoff+xoff)/2), 0, 1, 0, 255), 110);

}
