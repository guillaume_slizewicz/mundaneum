########  Writing with Otlet script  #######
########  By Guillaume Slizewicz     #######
########  Written for Data Workers   #######
########  An Exhibition by Algolit   #######
########  At the Mundaneum in Mons   #######

#modules
import random
import json
import requests
import RPi.GPIO as GPIO
import time
# import escpos
from escpos import printer
# from escpos import magicencode



# hardware printer

thermalprinter = printer.Serial("/dev/ttyUSB0", 19200)
thermalprinter.set(flip="FALSE")
# thermalprinter.codepage = 'PC850'
# thermalprinter.charcode("UTF-8")

#hardware button
GPIO.setmode(GPIO.BCM) 
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP,bouncetime=500)
prev_input = 0

#dataset
with open("/home/pi/mundaneum/data_tree_6th.json", "r") as read_file:
    data = json.load(read_file)

#################################################################################
######### function pour générer des parties du corps à partir du dataset ########
#################################################################################

def generate_feature(range_part,option=0):
	generated_list=[]
	random_key= random.randint(range_part[0],range_part[1])
	random_key_1= str(random_key)[:1]
	random_key_2= str(random_key)[1:2]
	random_key_3= str(random_key)[2:3]
	description_to_print="error to define"
	# thermalprinter.text(data[random_face_part_1]["data"])
	face_part=(data[random_key_1][random_key_2]["data"][0])
	face_description=(data[random_key_1][random_key_2][random_key_3]["data"][0])
	if random_key > 1000:
		random_key_4= str(random_key)[3:4]
		face_description_2=(data[random_key_1][random_key_2][random_key_3][random_key_4]["data"][0])
		if option==1:
			description_to_print= face_description_2
			# print(face_description_2, end=' ', flush=True)
		else:
			description_to_print= face_description+":"+face_description_2
			# print(face_description,':',face_description_2, end='', flush=True)
	else:
		description_to_print= face_part+":"+face_description
		# print(face_part,':',face_description, end='', flush=True)

	if len(description_to_print)>45: #for thermal printer capacity
		thermalprinter.text(description_to_print[46:])
		thermalprinter.text("\n")
		thermalprinter.text(description_to_print[:46])
	else:
		thermalprinter.text(description_to_print)
	thermalprinter.text('\n')

	# 	random_key_4= str(random_key)[3:4]
	# 	face_description_2=(data[random_key_1][random_key_2][random_key_3][random_key_4]["data"][0])
	# 	if option==1:
	# 		thermalprinter.text(face_description_2)
	# 	else:
	# 		thermalprinter.text(face_description)
	# 		thermalprinter.text(':')
	# 		thermalprinter.text(face_description_2)
	# else:
	# 	thermalprinter.text(face_part)
	# 	thermalprinter.text(':')
	# 	thermalprinter.text(face_description)
	# thermalprinter.text('\n')


###############################################
########   Button Callback definition  ########
###############################################


def button_callback(channel):
		time.sleep(0.2)

		random_gender= random.randint(0,1)
		if random_gender == 1:
			gender=" Homme"
		else:
			gender=" Femme"
		age= random.randint(6,104)


		######## Génération des différentes parties #######

		generate_feature(cou)
		if (random.randint(0,100) >90):
			generate_feature(chair)
			# thermalprinter.text('\n')
		if (random.randint(0,100) >96):
			generate_feature(lèvres)
			# thermalprinter.text('\n')
		if (random.randint(0,100) >60):
			generate_feature(bout_du_nez)
			# thermalprinter.text('\n')
		if (random.randint(0,50) >40):
			generate_feature(posture)
			# thermalprinter.text('\n')
		if (random.randint(0,16) >10):
			generate_feature(mimique)
			# thermalprinter.text('\n')
		if (random.randint(0,20) >15):
			generate_feature(geste)
			# thermalprinter.text('\n')
		if (random_gender==1 and age>14):
		 	generate_feature(barbe)
		 	# thermalprinter.text('\n')
		generate_feature(pigment)
		# thermalprinter.text('\n')
		if (random.randint(0,20) >15):
			generate_feature(abondance_cheveux)
			# thermalprinter.text('\n')
		generate_feature(nature_cheveux)
		# thermalprinter.text('\n')
		generate_feature(couleur_cheveux)
		# thermalprinter.text('\n')
		generate_feature(regard)
		# thermalprinter.text('\n')
		generate_feature(demarche)
		# thermalprinter.text('\n')
		generate_feature(voix)
		# thermalprinter.text('\n')
		if (random.randint(0,9) >8):
			generate_feature(yeux_part,1)
			# thermalprinter.text('\n')
		generate_feature(yeux)
		# thermalprinter.text('\n')



		######## AGE et GENRE #######


		thermalprinter.text (" {} ans" .format(age))
		# thermalprinter.text('\n')


		thermalprinter.text (gender)
		# thermalprinter.text('\n')

		thermalprinter.text ("Personnage généré:")


		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.cut('PART')
		#input("Appuyer sur le bouton pour générer un personnage...")
		#userInput = input("Appuyer sur le bouton pour générer un personnage ou appuyez sur q pour quitter...")
		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.text('...')
		thermalprinter.text('\n')

		thermalprinter.text("Appuyer sur le bouton pour générer un personnage")
		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.text('////////////////////////////////////////////////••••••••••••••••••••••••••••••••••••••••••••••••¿¿¿¿¿¿¿¿¿      WRITING WITH OTLET     ¿¿¿¿¿¿¿¿¿¿••••••••••••••••••••••••••••••••••••••••••••••••////////////////////////////////////////////////')
		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.text('\n')
		thermalprinter.text('--')
		thermalprinter.text('\n')
		thermalprinter.text('\n')

		time.sleep(0.2)



###############################################
######## Range des différentes parties ########
###############################################

front=[117,147]
nez=[211,266]
yeux_part=[9131,9139]
yeux=[911,917]
demarche=[8611,8619]
regard=[8631,8638]
voix=[871,878]
couleur_cheveux=[9241,9245]
nature_cheveux=[9211,9216]
abondance_cheveux=[9231,9235]
pigment=[9411,9413]
barbe=[9321,9328]
mimique=[8641,8643]
geste=[8621,8622]
posture= [8531,8536]
coloration_sanguine=[9421,9423]
bout_du_nez=[2721,2728]
lèvres=[4151,4154]
chair=[6231,6233]
cou=[811,816]

###############################################
########   Code pour la génération 	###########
###############################################
while True:
	GPIO.add_event_detect(18,GPIO.BOTH,callback=button_callback) # Setup event on pin 18 rising edge



GPIO.cleanup()
