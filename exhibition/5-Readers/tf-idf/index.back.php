<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>TF-IDF</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<main>

		<div class="explanations-content">
			<h1>Hi ! I am the TF-IDF reader </h1>
			<p class="explanations explanation1">I mean… I am the Term Frequency–Inverse Document Frequency</p>
			<p class="explanations explanation2">I mean…  I am a numerical statistic and I am intended to reflect how important a word is to a document in a collection or corpus</p>
			<p class="explanations explanation3">I am really useful for indexing websites</p>
			<p class="explanations instructions">Maybe, my explanations are not clear, so… to understand how I work, choose a text below, I'm going to analyse it</p>
			<!-- ↓ -->
		</div>
			
		
		<form action="#analyse" method="post" class='explanations'>
			<!-- <form action="" method="post" class='explanations'> -->
			<div class="custom-select">
				<select name="text">
				<?php
				session_start();
				$directory = 'text';
				$files = scandir($directory);
				foreach ($files as $key => $value):
				  if($value!="." && $value!=".."):?>
				  	<option value="<?php echo $value?>"><?php echo $value?></option><br>
				  <?php endif; 
				endforeach; ?>
				</select>
			</div>
			<input type="submit" name="submit" text="Analyze" value="Analyze">
		</form>



			<?php
			if(isset($_POST['submit'])){
				$selected_val = $_POST['text'];  // Storing Selected Value In Variable
				// echo '<h1>'.$selected_val.'</h1>';
				
				// // Display Text Content
				// $textContent = file_get_contents($directory.'/'.$selected_val);
				// echo '<div class="original-text">';
				// 	echo '<h2>Original Text</h2>';
				// 	echo '<p>'.$textContent.'</p>';
				// echo '</div">';

				// // Display Text Split by Content
				// $outputSentences = shell_exec('python3 split-by-sentences.py ' . $selected_val);
				// echo '<div class="split-by-sentences">';
				// 	echo '<h2>Text Split by sentences</h2>';
				// 	echo $outputSentences;
				// echo '</div">';

				// // Display Sentences Split by Words
				// $outputWords = shell_exec('python3 split-by-words.py ' . $selected_val);
				// echo '<div class="split-by-words">';
				// 	echo '<h2>Text Split by words</h2>';
				// 	echo $outputWords;
				// echo '</div">';

				// // Display remove punctuation process
				// $outputPunctuation = shell_exec('python3 remove-punctuation.py ' . $selected_val);
				// echo '<div class="lowercase">';
				// 	echo '<h2>Lowercase Text</h2>';
				// 	echo $outputPunctuation;
				// echo '</div">';

				// Display TF formula and results
				// $outputTF = shell_exec('python3 tf.py ' . $selected_val);
				// echo '<div class="tf">';
				// 	echo '<h2>Term Frequency (TF)</h2>';
				// 	echo '<p><math>TF = Number of times the word appears in a sentence / the total number of words in that sentence</math></p>';
				// 	echo $outputTF;
				// echo '</div">';

				// // Display IDF formula and results
				// $outputIDF = shell_exec('python3 idf.py ' . $selected_val);
				// echo '<div class="idf">';
				// 	echo '<h2>Inverse Data Frequency (IDF)</h2>';
				// 	echo '<p>IDF = log(total number of sentences / nombre of documents containing the word</p>';
				// 	echo $outputIDF;
				// echo '</div">';

				// // Display TF-IDF formula and results
				// $outputTFIDF = shell_exec('python3 tf-idf-html.py ' . $selected_val);
				// echo '<div class="tf-idf">';
				// 	echo '<h2>Term Frequency - Inverse Data Frequency (TF-IDF)</h2>';
				// 	echo '<p>TF-IDF = TF x IDF</p>';
				// 	echo $outputTFIDF;
				// echo '</div">';

				// Display the most relevant words for each texts
				$output = shell_exec('python3 tf-idf-relevant-words.py ' . $selected_val);
				echo '<div class="text-content">';
				echo '<div class="inprogress">
					<p>--------------------------</p>
					<h4 class="text-title">'.$selected_val.'</h4>
					<h3>Analysis in progress</h3>
					<p>--------------------------</p>
					</div>';
					echo '<h2 class="step-title">This is the text you have sent to me.<br> Let\'s Go !</h2>';
					echo '<div class="formula"></div>';
					echo $output;
				echo '</div">';

			} // end if of submit
			?>
			<div class="again">I can do it again</div>
		</main>
	<script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>

</body>
</html>