De l’enquête sur les faits et de leur examen général on peut dégager l’esquisse suivante d’une Organisation universelle :

1° L’organisation couvrira le champ entier des matières de connaissances et d’activité, ainsi que l’ensemble des formes et des fonctions de la documentation.

2° L’organisation implique la mise en œuvre des principes de coopération, coordination, concentration et spécialisation du travail, répartition des tâches entre organismes existants ou création d’organes nouveaux aux fins d’assurer des tâches anciennes. L’organisation se réalisera par concentration verticale, horizontale, longitudinale.

3° Les Offices de documentation seront multipliés de manière à répondre aux besoins constants. Ils seront spécialisés et couvriront chacun la partie du domaine général qu’il sera déterminé de commun accord.

4° La Répartition se fera selon les trois bases combinées a) de la matière (répartition verticale) (sujet ou science) ; b) du lieu (répartition horizontale) ; c) de l’espèce de fonction ou opération documentaire (répartition longitudinale.) [Publication, bibliothèque, bibliographie, archives, encyclopédie ou muséographie ; locaux régionaux, nationaux ou internationaux ; généraux ou spéciaux] ; la solution complète du problème mondial comporte : cent matières, soixante pays, six formes de documentation, sous les deux modalités, production ou utilisation, soit un bloc ou réseau de 72,000 alvéoles. Au centre, au siège de l’Office mondial, seront rassemblées les collections générales ainsi que les services centraux d’échanges et de prêts, placés sous un régime de propriété commune et de gestion coopérative.

5° Afin de rationaliser leurs activités et de les rendre plus efficientes, il sera procédé graduellement à une refonte des organismes documentaires ou de leurs activités par voie de fusion, séparation, concentration ou décentralisation.
