Il y a quelques mois, résumant ses impressions sur la situation
du Congo et l’avenir auquel peut prétendre ce pays, un de nos
compatriotes voyageurs émettait cette idée:

«Il faudrait, disait-il, que l’on envoie en Europe un certain nombre
de jeunes Congolais, afin d’y recevoir une éducation complète et
spéciale. Avec le goût du travail, le vif sentiment de sa nécessité
devrait leur être inculqué. Du doigt, pour ainsi dire, ils devraient
toucher les bienfaits de notre civilisation, de façon à en conserver
des traces indélébiles. On les renverrait ensuite là-bas, et à leur
tour ils deviendraient les initiateurs de leurs compatriotes. Auprès
de ceux-ci ils sauront toujours mieux remplir ce rôle que quiconque
d’entre nous».

Quand on réfléchit sur les principes mêmes de l’éducation on comprend
toute la vérité pratique de ces paroles.

L’histoire et les faits contemporains sont là du reste pour les
appuyer de leur autorité.
