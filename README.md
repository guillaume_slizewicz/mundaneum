# algolit @ mundaneum


This repository hosts the code for the artworks and working sessions presented during the algolit exhibition data workers, held in the Mundaneum in Mons in October, March and April 2019.

Data Workers is an **exhibition of algoliterary works**, of stories told from an ‘algorithmic storyteller point of view’. The exhibition was created by members of Algolit, a group from Brussels involved in artistic research on algorithms and literature. Every month they gather to experiment with F/LOSS code and texts. Some works are by students of Arts² and external participants to the workshop on machine learning and text organized by Algolit in October 2018 at the Mundaneum. 

You will find in [exhibition](/exhibition) the code for the pieces presented in the Mundaneum.

The section [documents](/documents) gather the original documents that were used for the exhibition and workshop, they come from the digitised collection of the mundane

The section [workshop](/workshop)  gather all the scripts and documents developed during the workshop.



-------------------------
![poster_exhibition](/poster_web.png)

**Data Workers** was created by Algolit. 

**Works by**: Cristina Cochior, Gijs de Heij, Sarah Garcin, An Mertens, Javier Lloret, Louise Dekeuleneer, Florian Van de Weyer, Laetitia Trozzi, Rémi Forte, Guillaume Slizewicz, Michael Murtaugh, Manetta Berends, Mia Melvær. 

**Co-produced by**: [Arts²](http://blog.artsaucarre.be/artsnumeriques/), [Constant](http://constantvzw.org/) and [Mundaneum](http://expositions.mundaneum.org/en/expositions/data-workers). 

**With the support of**: [Wallonia-Brussels Federation/Digital Arts](http://www.arts-numeriques.culture.be/), [Passa Porta](https://www.passaporta.be/en), UGent, [DHuF - Digital Humanities Flanders](https://www.uantwerpen.be/en/faculties/faculty-of-arts/research-and-valoris/research-axes/digital-humanities/) and [Distributed Proofreaders Project](https://www.pgdp.net/c/). 

**Thanks to**: Mike Kestemont, Michel Cleempoel, Donatella Portoghese, François Zajéga, Raphaèle Cornille, Vincent Desfromont, Kris Rutten, Anne-Laure Buisson, David Stampfli. 